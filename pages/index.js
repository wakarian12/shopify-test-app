//Import Polaris elements, Resource Picker and React
import { Page, Card, ResourceList, ResourceItem, Avatar, TextStyle, Button } from "@shopify/polaris";
import { ResourcePicker } from "@shopify/app-bridge-react";
import React, { Component } from 'react';

class Index extends Component {
    state = { open: false, resourceListArr: [], deleteButtonShown: false, resourceIndex: 0 }//set state

    render() { 
        return ( 
        //Page styling using Polaris Page element, Text an Button
        <Page                  
            fullWidth
            title="Product Selection"
            primaryAction={{
                content: 'Select product',
                onAction: () => this.setState({open: true})
            }}
            >
                <ResourcePicker  /*Resource Picker, change state on clothong and handling if items selected*/
                    resourceType="Product"
                    open={this.state.open}
                    onCancel={ () => this.setState({open: false})}
                    onSelection={(resources) => this.handleSelection(resources)} 
                /> 
                <div>
                    {// Display Clearing Button if any item is selected
                    this.state.deleteButtonShown ? 
                        <div style={{marginBottom: '15px'}}>
                            <Button destructive 
                            onClick={() => this.handleClear()}>Clear all</Button>
                        </div> 
                    : null} 
                </div>
                <Card>
                <ResourceList /*Adding Resource List wich consists of ResourceItems */
                    resourceName={{singular: 'product', plural: 'product'}}
                    items={ this.state.resourceListArr }
                    renderItem={(item) => {
                    const {resourceIndex, id,  name, avatarSource, price, variant} = item; //Setting Item
                    return (
                        <ResourceItem
                        id={id}
                        media= {<Avatar customer size="medium" name={name} source={avatarSource} />}
                        accessibilityLabel={`View details for ${name}`}
                        >
                        <h2>
                            <TextStyle variation="strong">{name}</TextStyle>
                        </h2>
                        <div>{price}</div>
                        <div style={{position:'absolute', right:'20px', marginTop: '-40px'}}>
                            <Button destructive resourceIndex={resourceIndex}
                            onClick={(ind) => this.handleDelete(item.resourceIndex)}>Delete</Button>
                        </div>
                        </ResourceItem>
                    );
                    }}
                />
                </Card>
        </Page> 
        );
    }

    //Handling Deletion of element from Resource List
    handleDelete = (ind) => {
        var pos = this.state.resourceListArr.map(function(e) { return e.resourceIndex; }).indexOf(ind); //Detecting position of item by it`s 'resourceIndex' value
        this.state.resourceListArr.splice(pos, 1); //Delete item from products array
        this.setState({ resourceListArr:  this.state.resourceListArr }); //Updating state
        //In case if all objects removed, hide 'Clear All' button
        if (this.state.resourceListArr.length == 0) {
            this.setState({deleteButtonShown: false});
        }
    }

    //Handling 'Clear All' button action
    handleClear = () => {
        this.setState({ resourceListArr: [] }); //Remove all items from product`s array
        this.setState({deleteButtonShown: false});//Hide 'Clear All' button
        this.setState({ resourceIndex: 0 });//Set 'ResourceIndex' counter to 0
    }

    //Handling items selection from resource picker
    handleSelection = (resources) => {
        //mapping new arrays with needed products values
        const titleFromResources = resources.selection.map((product) => product.title);
        const idFromResources = resources.selection.map((product) => product.id);
        const priceFromResources = resources.selection.map((product) => product.variants.map(function(e) { return e.price; }));
        const variantFromResources = resources.selection.map((product) => product.variants.map(function(e) { return e.title; }));
        const imgFromResources = resources.selection.map((product) => product.images[0].originalSrc);
        //Show 'Clear All' button
        this.setState({deleteButtonShown: true});
        //Close Resource Picker
        this.setState({open: false});
        //Adding products to product`s array
        for (let x = 0; x < priceFromResources.length; x++) {
            //if product don`t have variants
            if (priceFromResources[x].length == 1){
                 //Making new item with needed values, and concating it to array
                let joined = this.state.resourceListArr.concat({resourceIndex: this.state.resourceIndex, id: idFromResources[x],  name:titleFromResources[x], avatarSource:imgFromResources[x], price:priceFromResources[x]});
                //Updating the state
                this.setState({ resourceListArr: joined });
                this.setState({ resourceIndex: this.state.resourceIndex + 1 });
            }
            else {
                for (let y = 0; y < priceFromResources[x].length; y++) {
                     //Making new item with needed values, and concating it to array
                    let newJoined = this.state.resourceListArr.concat({resourceIndex: this.state.resourceIndex, id: idFromResources[x],  name:titleFromResources[x], avatarSource:imgFromResources[x], price:priceFromResources[x][y]});
                    //Updating the state
                    this.setState({ resourceListArr: newJoined });
                    this.setState({ resourceIndex: this.state.resourceIndex + 1 });
                }
            }
        }
    }

}
  
  export default Index;