module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./pages/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "react/jsx-runtime");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _shopify_polaris__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @shopify/polaris */ "@shopify/polaris");
/* harmony import */ var _shopify_polaris__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_shopify_polaris__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @shopify/app-bridge-react */ "@shopify/app-bridge-react");
/* harmony import */ var _shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);



function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//Import Polaris elements, Resource Picker and React




class Index extends react__WEBPACK_IMPORTED_MODULE_3__["Component"] {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {
      open: false,
      resourceListArr: [],
      deleteButtonShown: false,
      resourceIndex: 0
    });

    _defineProperty(this, "handleDelete", ind => {
      var pos = this.state.resourceListArr.map(function (e) {
        return e.resourceIndex;
      }).indexOf(ind); //Detecting position of item by it`s 'resourceIndex' value

      this.state.resourceListArr.splice(pos, 1); //Delete item from products array

      this.setState({
        resourceListArr: this.state.resourceListArr
      }); //Updating state
      //In case if all objects removed, hide 'Clear All' button

      if (this.state.resourceListArr.length == 0) {
        this.setState({
          deleteButtonShown: false
        });
      }
    });

    _defineProperty(this, "handleClear", () => {
      this.setState({
        resourceListArr: []
      }); //Remove all items from product`s array

      this.setState({
        deleteButtonShown: false
      }); //Hide 'Clear All' button

      this.setState({
        resourceIndex: 0
      }); //Set 'ResourceIndex' counter to 0
    });

    _defineProperty(this, "handleSelection", resources => {
      //mapping new arrays with needed products values
      const titleFromResources = resources.selection.map(product => product.title);
      const idFromResources = resources.selection.map(product => product.id);
      const priceFromResources = resources.selection.map(product => product.variants.map(function (e) {
        return e.price;
      }));
      const variantFromResources = resources.selection.map(product => product.variants.map(function (e) {
        return e.title;
      }));
      const imgFromResources = resources.selection.map(product => product.images[0].originalSrc); //Show 'Clear All' button

      this.setState({
        deleteButtonShown: true
      }); //Close Resource Picker

      this.setState({
        open: false
      }); //Adding products to product`s array

      for (let x = 0; x < priceFromResources.length; x++) {
        //if product don`t have variants
        if (priceFromResources[x].length == 1) {
          //Making new item with needed values, and concating it to array
          let joined = this.state.resourceListArr.concat({
            resourceIndex: this.state.resourceIndex,
            id: idFromResources[x],
            name: titleFromResources[x],
            avatarSource: imgFromResources[x],
            price: priceFromResources[x]
          }); //Updating the state

          this.setState({
            resourceListArr: joined
          });
          this.setState({
            resourceIndex: this.state.resourceIndex + 1
          });
        } else {
          for (let y = 0; y < priceFromResources[x].length; y++) {
            //Making new item with needed values, and concating it to array
            let newJoined = this.state.resourceListArr.concat({
              resourceIndex: this.state.resourceIndex,
              id: idFromResources[x],
              name: titleFromResources[x],
              avatarSource: imgFromResources[x],
              price: priceFromResources[x][y]
            }); //Updating the state

            this.setState({
              resourceListArr: newJoined
            });
            this.setState({
              resourceIndex: this.state.resourceIndex + 1
            });
          }
        }
      }
    });
  }

  //set state
  render() {
    return (
      /*#__PURE__*/
      //Page styling using Polaris Page element, Text an Button
      Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxs"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_1__["Page"], {
        fullWidth: true,
        title: "Product Selection",
        primaryAction: {
          content: 'Select product',
          onAction: () => this.setState({
            open: true
          })
        },
        children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])(_shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_2__["ResourcePicker"]
        /*Resource Picker, change state on clothong and handling if items selected*/
        , {
          resourceType: "Product",
          open: this.state.open,
          onCancel: () => this.setState({
            open: false
          }),
          onSelection: resources => this.handleSelection(resources)
        }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", {
          children: // Display Clearing Button if any item is selected
          this.state.deleteButtonShown ? /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", {
            style: {
              marginBottom: '15px'
            },
            children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_1__["Button"], {
              destructive: true,
              onClick: () => this.handleClear(),
              children: "Clear all"
            })
          }) : null
        }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_1__["Card"], {
          children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_1__["ResourceList"]
          /*Adding Resource List wich consists of ResourceItems */
          , {
            resourceName: {
              singular: 'product',
              plural: 'product'
            },
            items: this.state.resourceListArr,
            renderItem: item => {
              const {
                resourceIndex,
                id,
                name,
                avatarSource,
                price,
                variant
              } = item; //Setting Item

              return /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxs"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_1__["ResourceItem"], {
                id: id,
                media: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_1__["Avatar"], {
                  customer: true,
                  size: "medium",
                  name: name,
                  source: avatarSource
                }),
                accessibilityLabel: `View details for ${name}`,
                children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("h2", {
                  children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_1__["TextStyle"], {
                    variation: "strong",
                    children: name
                  })
                }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", {
                  children: price
                }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", {
                  style: {
                    position: 'absolute',
                    right: '20px',
                    marginTop: '-40px'
                  },
                  children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_1__["Button"], {
                    destructive: true,
                    resourceIndex: resourceIndex,
                    onClick: ind => this.handleDelete(item.resourceIndex),
                    children: "Delete"
                  })
                })]
              });
            }
          })
        })]
      })
    );
  } //Handling Deletion of element from Resource List


}

/* harmony default export */ __webpack_exports__["default"] = (Index);

/***/ }),

/***/ "@shopify/app-bridge-react":
/*!********************************************!*\
  !*** external "@shopify/app-bridge-react" ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@shopify/app-bridge-react");

/***/ }),

/***/ "@shopify/polaris":
/*!***********************************!*\
  !*** external "@shopify/polaris" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@shopify/polaris");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react/jsx-runtime":
/*!************************************!*\
  !*** external "react/jsx-runtime" ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react/jsx-runtime");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiQHNob3BpZnkvYXBwLWJyaWRnZS1yZWFjdFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcIkBzaG9waWZ5L3BvbGFyaXNcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0L2pzeC1ydW50aW1lXCIiXSwibmFtZXMiOlsiSW5kZXgiLCJDb21wb25lbnQiLCJvcGVuIiwicmVzb3VyY2VMaXN0QXJyIiwiZGVsZXRlQnV0dG9uU2hvd24iLCJyZXNvdXJjZUluZGV4IiwiaW5kIiwicG9zIiwic3RhdGUiLCJtYXAiLCJlIiwiaW5kZXhPZiIsInNwbGljZSIsInNldFN0YXRlIiwibGVuZ3RoIiwicmVzb3VyY2VzIiwidGl0bGVGcm9tUmVzb3VyY2VzIiwic2VsZWN0aW9uIiwicHJvZHVjdCIsInRpdGxlIiwiaWRGcm9tUmVzb3VyY2VzIiwiaWQiLCJwcmljZUZyb21SZXNvdXJjZXMiLCJ2YXJpYW50cyIsInByaWNlIiwidmFyaWFudEZyb21SZXNvdXJjZXMiLCJpbWdGcm9tUmVzb3VyY2VzIiwiaW1hZ2VzIiwib3JpZ2luYWxTcmMiLCJ4Iiwiam9pbmVkIiwiY29uY2F0IiwibmFtZSIsImF2YXRhclNvdXJjZSIsInkiLCJuZXdKb2luZWQiLCJyZW5kZXIiLCJjb250ZW50Iiwib25BY3Rpb24iLCJoYW5kbGVTZWxlY3Rpb24iLCJtYXJnaW5Cb3R0b20iLCJoYW5kbGVDbGVhciIsInNpbmd1bGFyIiwicGx1cmFsIiwiaXRlbSIsInZhcmlhbnQiLCJwb3NpdGlvbiIsInJpZ2h0IiwibWFyZ2luVG9wIiwiaGFuZGxlRGVsZXRlIl0sIm1hcHBpbmdzIjoiOztRQUFBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EsSUFBSTtRQUNKO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7OztRQUdBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwwQ0FBMEMsZ0NBQWdDO1FBQzFFO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0Esd0RBQXdELGtCQUFrQjtRQUMxRTtRQUNBLGlEQUFpRCxjQUFjO1FBQy9EOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSx5Q0FBeUMsaUNBQWlDO1FBQzFFLGdIQUFnSCxtQkFBbUIsRUFBRTtRQUNySTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDJCQUEyQiwwQkFBMEIsRUFBRTtRQUN2RCxpQ0FBaUMsZUFBZTtRQUNoRDtRQUNBO1FBQ0E7O1FBRUE7UUFDQSxzREFBc0QsK0RBQStEOztRQUVySDtRQUNBOzs7UUFHQTtRQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN4RkE7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsTUFBTUEsS0FBTixTQUFvQkMsK0NBQXBCLENBQThCO0FBQUE7QUFBQTs7QUFBQSxtQ0FDbEI7QUFBRUMsVUFBSSxFQUFFLEtBQVI7QUFBZUMscUJBQWUsRUFBRSxFQUFoQztBQUFvQ0MsdUJBQWlCLEVBQUUsS0FBdkQ7QUFBOERDLG1CQUFhLEVBQUU7QUFBN0UsS0FEa0I7O0FBQUEsMENBMkRWQyxHQUFELElBQVM7QUFDcEIsVUFBSUMsR0FBRyxHQUFHLEtBQUtDLEtBQUwsQ0FBV0wsZUFBWCxDQUEyQk0sR0FBM0IsQ0FBK0IsVUFBU0MsQ0FBVCxFQUFZO0FBQUUsZUFBT0EsQ0FBQyxDQUFDTCxhQUFUO0FBQXlCLE9BQXRFLEVBQXdFTSxPQUF4RSxDQUFnRkwsR0FBaEYsQ0FBVixDQURvQixDQUM0RTs7QUFDaEcsV0FBS0UsS0FBTCxDQUFXTCxlQUFYLENBQTJCUyxNQUEzQixDQUFrQ0wsR0FBbEMsRUFBdUMsQ0FBdkMsRUFGb0IsQ0FFdUI7O0FBQzNDLFdBQUtNLFFBQUwsQ0FBYztBQUFFVix1QkFBZSxFQUFHLEtBQUtLLEtBQUwsQ0FBV0w7QUFBL0IsT0FBZCxFQUhvQixDQUc2QztBQUNqRTs7QUFDQSxVQUFJLEtBQUtLLEtBQUwsQ0FBV0wsZUFBWCxDQUEyQlcsTUFBM0IsSUFBcUMsQ0FBekMsRUFBNEM7QUFDeEMsYUFBS0QsUUFBTCxDQUFjO0FBQUNULDJCQUFpQixFQUFFO0FBQXBCLFNBQWQ7QUFDSDtBQUNKLEtBbkV5Qjs7QUFBQSx5Q0FzRVosTUFBTTtBQUNoQixXQUFLUyxRQUFMLENBQWM7QUFBRVYsdUJBQWUsRUFBRTtBQUFuQixPQUFkLEVBRGdCLENBQ3dCOztBQUN4QyxXQUFLVSxRQUFMLENBQWM7QUFBQ1QseUJBQWlCLEVBQUU7QUFBcEIsT0FBZCxFQUZnQixDQUUwQjs7QUFDMUMsV0FBS1MsUUFBTCxDQUFjO0FBQUVSLHFCQUFhLEVBQUU7QUFBakIsT0FBZCxFQUhnQixDQUdvQjtBQUN2QyxLQTFFeUI7O0FBQUEsNkNBNkVQVSxTQUFELElBQWU7QUFDN0I7QUFDQSxZQUFNQyxrQkFBa0IsR0FBR0QsU0FBUyxDQUFDRSxTQUFWLENBQW9CUixHQUFwQixDQUF5QlMsT0FBRCxJQUFhQSxPQUFPLENBQUNDLEtBQTdDLENBQTNCO0FBQ0EsWUFBTUMsZUFBZSxHQUFHTCxTQUFTLENBQUNFLFNBQVYsQ0FBb0JSLEdBQXBCLENBQXlCUyxPQUFELElBQWFBLE9BQU8sQ0FBQ0csRUFBN0MsQ0FBeEI7QUFDQSxZQUFNQyxrQkFBa0IsR0FBR1AsU0FBUyxDQUFDRSxTQUFWLENBQW9CUixHQUFwQixDQUF5QlMsT0FBRCxJQUFhQSxPQUFPLENBQUNLLFFBQVIsQ0FBaUJkLEdBQWpCLENBQXFCLFVBQVNDLENBQVQsRUFBWTtBQUFFLGVBQU9BLENBQUMsQ0FBQ2MsS0FBVDtBQUFpQixPQUFwRCxDQUFyQyxDQUEzQjtBQUNBLFlBQU1DLG9CQUFvQixHQUFHVixTQUFTLENBQUNFLFNBQVYsQ0FBb0JSLEdBQXBCLENBQXlCUyxPQUFELElBQWFBLE9BQU8sQ0FBQ0ssUUFBUixDQUFpQmQsR0FBakIsQ0FBcUIsVUFBU0MsQ0FBVCxFQUFZO0FBQUUsZUFBT0EsQ0FBQyxDQUFDUyxLQUFUO0FBQWlCLE9BQXBELENBQXJDLENBQTdCO0FBQ0EsWUFBTU8sZ0JBQWdCLEdBQUdYLFNBQVMsQ0FBQ0UsU0FBVixDQUFvQlIsR0FBcEIsQ0FBeUJTLE9BQUQsSUFBYUEsT0FBTyxDQUFDUyxNQUFSLENBQWUsQ0FBZixFQUFrQkMsV0FBdkQsQ0FBekIsQ0FONkIsQ0FPN0I7O0FBQ0EsV0FBS2YsUUFBTCxDQUFjO0FBQUNULHlCQUFpQixFQUFFO0FBQXBCLE9BQWQsRUFSNkIsQ0FTN0I7O0FBQ0EsV0FBS1MsUUFBTCxDQUFjO0FBQUNYLFlBQUksRUFBRTtBQUFQLE9BQWQsRUFWNkIsQ0FXN0I7O0FBQ0EsV0FBSyxJQUFJMkIsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR1Asa0JBQWtCLENBQUNSLE1BQXZDLEVBQStDZSxDQUFDLEVBQWhELEVBQW9EO0FBQ2hEO0FBQ0EsWUFBSVAsa0JBQWtCLENBQUNPLENBQUQsQ0FBbEIsQ0FBc0JmLE1BQXRCLElBQWdDLENBQXBDLEVBQXNDO0FBQ2pDO0FBQ0QsY0FBSWdCLE1BQU0sR0FBRyxLQUFLdEIsS0FBTCxDQUFXTCxlQUFYLENBQTJCNEIsTUFBM0IsQ0FBa0M7QUFBQzFCLHlCQUFhLEVBQUUsS0FBS0csS0FBTCxDQUFXSCxhQUEzQjtBQUEwQ2dCLGNBQUUsRUFBRUQsZUFBZSxDQUFDUyxDQUFELENBQTdEO0FBQW1FRyxnQkFBSSxFQUFDaEIsa0JBQWtCLENBQUNhLENBQUQsQ0FBMUY7QUFBK0ZJLHdCQUFZLEVBQUNQLGdCQUFnQixDQUFDRyxDQUFELENBQTVIO0FBQWlJTCxpQkFBSyxFQUFDRixrQkFBa0IsQ0FBQ08sQ0FBRDtBQUF6SixXQUFsQyxDQUFiLENBRmtDLENBR2xDOztBQUNBLGVBQUtoQixRQUFMLENBQWM7QUFBRVYsMkJBQWUsRUFBRTJCO0FBQW5CLFdBQWQ7QUFDQSxlQUFLakIsUUFBTCxDQUFjO0FBQUVSLHlCQUFhLEVBQUUsS0FBS0csS0FBTCxDQUFXSCxhQUFYLEdBQTJCO0FBQTVDLFdBQWQ7QUFDSCxTQU5ELE1BT0s7QUFDRCxlQUFLLElBQUk2QixDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHWixrQkFBa0IsQ0FBQ08sQ0FBRCxDQUFsQixDQUFzQmYsTUFBMUMsRUFBa0RvQixDQUFDLEVBQW5ELEVBQXVEO0FBQ2xEO0FBQ0QsZ0JBQUlDLFNBQVMsR0FBRyxLQUFLM0IsS0FBTCxDQUFXTCxlQUFYLENBQTJCNEIsTUFBM0IsQ0FBa0M7QUFBQzFCLDJCQUFhLEVBQUUsS0FBS0csS0FBTCxDQUFXSCxhQUEzQjtBQUEwQ2dCLGdCQUFFLEVBQUVELGVBQWUsQ0FBQ1MsQ0FBRCxDQUE3RDtBQUFtRUcsa0JBQUksRUFBQ2hCLGtCQUFrQixDQUFDYSxDQUFELENBQTFGO0FBQStGSSwwQkFBWSxFQUFDUCxnQkFBZ0IsQ0FBQ0csQ0FBRCxDQUE1SDtBQUFpSUwsbUJBQUssRUFBQ0Ysa0JBQWtCLENBQUNPLENBQUQsQ0FBbEIsQ0FBc0JLLENBQXRCO0FBQXZJLGFBQWxDLENBQWhCLENBRm1ELENBR25EOztBQUNBLGlCQUFLckIsUUFBTCxDQUFjO0FBQUVWLDZCQUFlLEVBQUVnQztBQUFuQixhQUFkO0FBQ0EsaUJBQUt0QixRQUFMLENBQWM7QUFBRVIsMkJBQWEsRUFBRSxLQUFLRyxLQUFMLENBQVdILGFBQVgsR0FBMkI7QUFBNUMsYUFBZDtBQUNIO0FBQ0o7QUFDSjtBQUNKLEtBNUd5QjtBQUFBOztBQUM4RDtBQUV4RitCLFFBQU0sR0FBRztBQUNMO0FBQUE7QUFDQTtBQUNBLHFFQUFDLHFEQUFEO0FBQ0ksaUJBQVMsTUFEYjtBQUVJLGFBQUssRUFBQyxtQkFGVjtBQUdJLHFCQUFhLEVBQUU7QUFDWEMsaUJBQU8sRUFBRSxnQkFERTtBQUVYQyxrQkFBUSxFQUFFLE1BQU0sS0FBS3pCLFFBQUwsQ0FBYztBQUFDWCxnQkFBSSxFQUFFO0FBQVAsV0FBZDtBQUZMLFNBSG5CO0FBQUEsZ0NBUVEsOERBQUMsd0VBQWM7QUFBRTtBQUFqQjtBQUNJLHNCQUFZLEVBQUMsU0FEakI7QUFFSSxjQUFJLEVBQUUsS0FBS00sS0FBTCxDQUFXTixJQUZyQjtBQUdJLGtCQUFRLEVBQUcsTUFBTSxLQUFLVyxRQUFMLENBQWM7QUFBQ1gsZ0JBQUksRUFBRTtBQUFQLFdBQWQsQ0FIckI7QUFJSSxxQkFBVyxFQUFHYSxTQUFELElBQWUsS0FBS3dCLGVBQUwsQ0FBcUJ4QixTQUFyQjtBQUpoQyxVQVJSLGVBY1E7QUFBQSxvQkFDSztBQUNELGVBQUtQLEtBQUwsQ0FBV0osaUJBQVgsZ0JBQ0k7QUFBSyxpQkFBSyxFQUFFO0FBQUNvQywwQkFBWSxFQUFFO0FBQWYsYUFBWjtBQUFBLG1DQUNJLDhEQUFDLHVEQUFEO0FBQVEseUJBQVcsTUFBbkI7QUFDQSxxQkFBTyxFQUFFLE1BQU0sS0FBS0MsV0FBTCxFQURmO0FBQUE7QUFBQTtBQURKLFlBREosR0FLRTtBQVBOLFVBZFIsZUF1QlEsOERBQUMscURBQUQ7QUFBQSxpQ0FDQSw4REFBQyw2REFBWTtBQUFDO0FBQWQ7QUFDSSx3QkFBWSxFQUFFO0FBQUNDLHNCQUFRLEVBQUUsU0FBWDtBQUFzQkMsb0JBQU0sRUFBRTtBQUE5QixhQURsQjtBQUVJLGlCQUFLLEVBQUcsS0FBS25DLEtBQUwsQ0FBV0wsZUFGdkI7QUFHSSxzQkFBVSxFQUFHeUMsSUFBRCxJQUFVO0FBQ3RCLG9CQUFNO0FBQUN2Qyw2QkFBRDtBQUFnQmdCLGtCQUFoQjtBQUFxQlcsb0JBQXJCO0FBQTJCQyw0QkFBM0I7QUFBeUNULHFCQUF6QztBQUFnRHFCO0FBQWhELGtCQUEyREQsSUFBakUsQ0FEc0IsQ0FDaUQ7O0FBQ3ZFLGtDQUNJLCtEQUFDLDZEQUFEO0FBQ0Esa0JBQUUsRUFBRXZCLEVBREo7QUFFQSxxQkFBSyxlQUFHLDhEQUFDLHVEQUFEO0FBQVEsMEJBQVEsTUFBaEI7QUFBaUIsc0JBQUksRUFBQyxRQUF0QjtBQUErQixzQkFBSSxFQUFFVyxJQUFyQztBQUEyQyx3QkFBTSxFQUFFQztBQUFuRCxrQkFGUjtBQUdBLGtDQUFrQixFQUFHLG9CQUFtQkQsSUFBSyxFQUg3QztBQUFBLHdDQUtBO0FBQUEseUNBQ0ksOERBQUMsMERBQUQ7QUFBVyw2QkFBUyxFQUFDLFFBQXJCO0FBQUEsOEJBQStCQTtBQUEvQjtBQURKLGtCQUxBLGVBUUE7QUFBQSw0QkFBTVI7QUFBTixrQkFSQSxlQVNBO0FBQUssdUJBQUssRUFBRTtBQUFDc0IsNEJBQVEsRUFBQyxVQUFWO0FBQXNCQyx5QkFBSyxFQUFDLE1BQTVCO0FBQW9DQyw2QkFBUyxFQUFFO0FBQS9DLG1CQUFaO0FBQUEseUNBQ0ksOERBQUMsdURBQUQ7QUFBUSwrQkFBVyxNQUFuQjtBQUFvQixpQ0FBYSxFQUFFM0MsYUFBbkM7QUFDQSwyQkFBTyxFQUFHQyxHQUFELElBQVMsS0FBSzJDLFlBQUwsQ0FBa0JMLElBQUksQ0FBQ3ZDLGFBQXZCLENBRGxCO0FBQUE7QUFBQTtBQURKLGtCQVRBO0FBQUEsZ0JBREo7QUFnQkM7QUFyQkw7QUFEQSxVQXZCUjtBQUFBO0FBRkE7QUFvREgsR0F4RHlCLENBMEQxQjs7O0FBMUQwQjs7QUFnSGJMLG9FQUFmLEU7Ozs7Ozs7Ozs7O0FDckhGLHNEOzs7Ozs7Ozs7OztBQ0FBLDZDOzs7Ozs7Ozs7OztBQ0FBLGtDOzs7Ozs7Ozs7OztBQ0FBLDhDIiwiZmlsZSI6InBhZ2VzL2luZGV4LmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSByZXF1aXJlKCcuLi9zc3ItbW9kdWxlLWNhY2hlLmpzJyk7XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdHZhciB0aHJldyA9IHRydWU7XG4gXHRcdHRyeSB7XG4gXHRcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG4gXHRcdFx0dGhyZXcgPSBmYWxzZTtcbiBcdFx0fSBmaW5hbGx5IHtcbiBcdFx0XHRpZih0aHJldykgZGVsZXRlIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdO1xuIFx0XHR9XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9wYWdlcy9pbmRleC5qc1wiKTtcbiIsIi8vSW1wb3J0IFBvbGFyaXMgZWxlbWVudHMsIFJlc291cmNlIFBpY2tlciBhbmQgUmVhY3RcclxuaW1wb3J0IHsgUGFnZSwgQ2FyZCwgUmVzb3VyY2VMaXN0LCBSZXNvdXJjZUl0ZW0sIEF2YXRhciwgVGV4dFN0eWxlLCBCdXR0b24gfSBmcm9tIFwiQHNob3BpZnkvcG9sYXJpc1wiO1xyXG5pbXBvcnQgeyBSZXNvdXJjZVBpY2tlciB9IGZyb20gXCJAc2hvcGlmeS9hcHAtYnJpZGdlLXJlYWN0XCI7XHJcbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XHJcblxyXG5jbGFzcyBJbmRleCBleHRlbmRzIENvbXBvbmVudCB7XHJcbiAgICBzdGF0ZSA9IHsgb3BlbjogZmFsc2UsIHJlc291cmNlTGlzdEFycjogW10sIGRlbGV0ZUJ1dHRvblNob3duOiBmYWxzZSwgcmVzb3VyY2VJbmRleDogMCB9Ly9zZXQgc3RhdGVcclxuXHJcbiAgICByZW5kZXIoKSB7IFxyXG4gICAgICAgIHJldHVybiAoIFxyXG4gICAgICAgIC8vUGFnZSBzdHlsaW5nIHVzaW5nIFBvbGFyaXMgUGFnZSBlbGVtZW50LCBUZXh0IGFuIEJ1dHRvblxyXG4gICAgICAgIDxQYWdlICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGZ1bGxXaWR0aFxyXG4gICAgICAgICAgICB0aXRsZT1cIlByb2R1Y3QgU2VsZWN0aW9uXCJcclxuICAgICAgICAgICAgcHJpbWFyeUFjdGlvbj17e1xyXG4gICAgICAgICAgICAgICAgY29udGVudDogJ1NlbGVjdCBwcm9kdWN0JyxcclxuICAgICAgICAgICAgICAgIG9uQWN0aW9uOiAoKSA9PiB0aGlzLnNldFN0YXRlKHtvcGVuOiB0cnVlfSlcclxuICAgICAgICAgICAgfX1cclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgPFJlc291cmNlUGlja2VyICAvKlJlc291cmNlIFBpY2tlciwgY2hhbmdlIHN0YXRlIG9uIGNsb3Rob25nIGFuZCBoYW5kbGluZyBpZiBpdGVtcyBzZWxlY3RlZCovXHJcbiAgICAgICAgICAgICAgICAgICAgcmVzb3VyY2VUeXBlPVwiUHJvZHVjdFwiXHJcbiAgICAgICAgICAgICAgICAgICAgb3Blbj17dGhpcy5zdGF0ZS5vcGVufVxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2FuY2VsPXsgKCkgPT4gdGhpcy5zZXRTdGF0ZSh7b3BlbjogZmFsc2V9KX1cclxuICAgICAgICAgICAgICAgICAgICBvblNlbGVjdGlvbj17KHJlc291cmNlcykgPT4gdGhpcy5oYW5kbGVTZWxlY3Rpb24ocmVzb3VyY2VzKX0gXHJcbiAgICAgICAgICAgICAgICAvPiBcclxuICAgICAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgey8vIERpc3BsYXkgQ2xlYXJpbmcgQnV0dG9uIGlmIGFueSBpdGVtIGlzIHNlbGVjdGVkXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5kZWxldGVCdXR0b25TaG93biA/IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IHN0eWxlPXt7bWFyZ2luQm90dG9tOiAnMTVweCd9fT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxCdXR0b24gZGVzdHJ1Y3RpdmUgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB0aGlzLmhhbmRsZUNsZWFyKCl9PkNsZWFyIGFsbDwvQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj4gXHJcbiAgICAgICAgICAgICAgICAgICAgOiBudWxsfSBcclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPENhcmQ+XHJcbiAgICAgICAgICAgICAgICA8UmVzb3VyY2VMaXN0IC8qQWRkaW5nIFJlc291cmNlIExpc3Qgd2ljaCBjb25zaXN0cyBvZiBSZXNvdXJjZUl0ZW1zICovXHJcbiAgICAgICAgICAgICAgICAgICAgcmVzb3VyY2VOYW1lPXt7c2luZ3VsYXI6ICdwcm9kdWN0JywgcGx1cmFsOiAncHJvZHVjdCd9fVxyXG4gICAgICAgICAgICAgICAgICAgIGl0ZW1zPXsgdGhpcy5zdGF0ZS5yZXNvdXJjZUxpc3RBcnIgfVxyXG4gICAgICAgICAgICAgICAgICAgIHJlbmRlckl0ZW09eyhpdGVtKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qge3Jlc291cmNlSW5kZXgsIGlkLCAgbmFtZSwgYXZhdGFyU291cmNlLCBwcmljZSwgdmFyaWFudH0gPSBpdGVtOyAvL1NldHRpbmcgSXRlbVxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxSZXNvdXJjZUl0ZW1cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ9e2lkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtZWRpYT0gezxBdmF0YXIgY3VzdG9tZXIgc2l6ZT1cIm1lZGl1bVwiIG5hbWU9e25hbWV9IHNvdXJjZT17YXZhdGFyU291cmNlfSAvPn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgYWNjZXNzaWJpbGl0eUxhYmVsPXtgVmlldyBkZXRhaWxzIGZvciAke25hbWV9YH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8aDI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8VGV4dFN0eWxlIHZhcmlhdGlvbj1cInN0cm9uZ1wiPntuYW1lfTwvVGV4dFN0eWxlPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2gyPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2PntwcmljZX08L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBzdHlsZT17e3Bvc2l0aW9uOidhYnNvbHV0ZScsIHJpZ2h0OicyMHB4JywgbWFyZ2luVG9wOiAnLTQwcHgnfX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QnV0dG9uIGRlc3RydWN0aXZlIHJlc291cmNlSW5kZXg9e3Jlc291cmNlSW5kZXh9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoaW5kKSA9PiB0aGlzLmhhbmRsZURlbGV0ZShpdGVtLnJlc291cmNlSW5kZXgpfT5EZWxldGU8L0J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvUmVzb3VyY2VJdGVtPlxyXG4gICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICAgICAgfX1cclxuICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICA8L0NhcmQ+XHJcbiAgICAgICAgPC9QYWdlPiBcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8vSGFuZGxpbmcgRGVsZXRpb24gb2YgZWxlbWVudCBmcm9tIFJlc291cmNlIExpc3RcclxuICAgIGhhbmRsZURlbGV0ZSA9IChpbmQpID0+IHtcclxuICAgICAgICB2YXIgcG9zID0gdGhpcy5zdGF0ZS5yZXNvdXJjZUxpc3RBcnIubWFwKGZ1bmN0aW9uKGUpIHsgcmV0dXJuIGUucmVzb3VyY2VJbmRleDsgfSkuaW5kZXhPZihpbmQpOyAvL0RldGVjdGluZyBwb3NpdGlvbiBvZiBpdGVtIGJ5IGl0YHMgJ3Jlc291cmNlSW5kZXgnIHZhbHVlXHJcbiAgICAgICAgdGhpcy5zdGF0ZS5yZXNvdXJjZUxpc3RBcnIuc3BsaWNlKHBvcywgMSk7IC8vRGVsZXRlIGl0ZW0gZnJvbSBwcm9kdWN0cyBhcnJheVxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyByZXNvdXJjZUxpc3RBcnI6ICB0aGlzLnN0YXRlLnJlc291cmNlTGlzdEFyciB9KTsgLy9VcGRhdGluZyBzdGF0ZVxyXG4gICAgICAgIC8vSW4gY2FzZSBpZiBhbGwgb2JqZWN0cyByZW1vdmVkLCBoaWRlICdDbGVhciBBbGwnIGJ1dHRvblxyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnJlc291cmNlTGlzdEFyci5sZW5ndGggPT0gMCkge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtkZWxldGVCdXR0b25TaG93bjogZmFsc2V9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy9IYW5kbGluZyAnQ2xlYXIgQWxsJyBidXR0b24gYWN0aW9uXHJcbiAgICBoYW5kbGVDbGVhciA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VMaXN0QXJyOiBbXSB9KTsgLy9SZW1vdmUgYWxsIGl0ZW1zIGZyb20gcHJvZHVjdGBzIGFycmF5XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7ZGVsZXRlQnV0dG9uU2hvd246IGZhbHNlfSk7Ly9IaWRlICdDbGVhciBBbGwnIGJ1dHRvblxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyByZXNvdXJjZUluZGV4OiAwIH0pOy8vU2V0ICdSZXNvdXJjZUluZGV4JyBjb3VudGVyIHRvIDBcclxuICAgIH1cclxuXHJcbiAgICAvL0hhbmRsaW5nIGl0ZW1zIHNlbGVjdGlvbiBmcm9tIHJlc291cmNlIHBpY2tlclxyXG4gICAgaGFuZGxlU2VsZWN0aW9uID0gKHJlc291cmNlcykgPT4ge1xyXG4gICAgICAgIC8vbWFwcGluZyBuZXcgYXJyYXlzIHdpdGggbmVlZGVkIHByb2R1Y3RzIHZhbHVlc1xyXG4gICAgICAgIGNvbnN0IHRpdGxlRnJvbVJlc291cmNlcyA9IHJlc291cmNlcy5zZWxlY3Rpb24ubWFwKChwcm9kdWN0KSA9PiBwcm9kdWN0LnRpdGxlKTtcclxuICAgICAgICBjb25zdCBpZEZyb21SZXNvdXJjZXMgPSByZXNvdXJjZXMuc2VsZWN0aW9uLm1hcCgocHJvZHVjdCkgPT4gcHJvZHVjdC5pZCk7XHJcbiAgICAgICAgY29uc3QgcHJpY2VGcm9tUmVzb3VyY2VzID0gcmVzb3VyY2VzLnNlbGVjdGlvbi5tYXAoKHByb2R1Y3QpID0+IHByb2R1Y3QudmFyaWFudHMubWFwKGZ1bmN0aW9uKGUpIHsgcmV0dXJuIGUucHJpY2U7IH0pKTtcclxuICAgICAgICBjb25zdCB2YXJpYW50RnJvbVJlc291cmNlcyA9IHJlc291cmNlcy5zZWxlY3Rpb24ubWFwKChwcm9kdWN0KSA9PiBwcm9kdWN0LnZhcmlhbnRzLm1hcChmdW5jdGlvbihlKSB7IHJldHVybiBlLnRpdGxlOyB9KSk7XHJcbiAgICAgICAgY29uc3QgaW1nRnJvbVJlc291cmNlcyA9IHJlc291cmNlcy5zZWxlY3Rpb24ubWFwKChwcm9kdWN0KSA9PiBwcm9kdWN0LmltYWdlc1swXS5vcmlnaW5hbFNyYyk7XHJcbiAgICAgICAgLy9TaG93ICdDbGVhciBBbGwnIGJ1dHRvblxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe2RlbGV0ZUJ1dHRvblNob3duOiB0cnVlfSk7XHJcbiAgICAgICAgLy9DbG9zZSBSZXNvdXJjZSBQaWNrZXJcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtvcGVuOiBmYWxzZX0pO1xyXG4gICAgICAgIC8vQWRkaW5nIHByb2R1Y3RzIHRvIHByb2R1Y3RgcyBhcnJheVxyXG4gICAgICAgIGZvciAobGV0IHggPSAwOyB4IDwgcHJpY2VGcm9tUmVzb3VyY2VzLmxlbmd0aDsgeCsrKSB7XHJcbiAgICAgICAgICAgIC8vaWYgcHJvZHVjdCBkb25gdCBoYXZlIHZhcmlhbnRzXHJcbiAgICAgICAgICAgIGlmIChwcmljZUZyb21SZXNvdXJjZXNbeF0ubGVuZ3RoID09IDEpe1xyXG4gICAgICAgICAgICAgICAgIC8vTWFraW5nIG5ldyBpdGVtIHdpdGggbmVlZGVkIHZhbHVlcywgYW5kIGNvbmNhdGluZyBpdCB0byBhcnJheVxyXG4gICAgICAgICAgICAgICAgbGV0IGpvaW5lZCA9IHRoaXMuc3RhdGUucmVzb3VyY2VMaXN0QXJyLmNvbmNhdCh7cmVzb3VyY2VJbmRleDogdGhpcy5zdGF0ZS5yZXNvdXJjZUluZGV4LCBpZDogaWRGcm9tUmVzb3VyY2VzW3hdLCAgbmFtZTp0aXRsZUZyb21SZXNvdXJjZXNbeF0sIGF2YXRhclNvdXJjZTppbWdGcm9tUmVzb3VyY2VzW3hdLCBwcmljZTpwcmljZUZyb21SZXNvdXJjZXNbeF19KTtcclxuICAgICAgICAgICAgICAgIC8vVXBkYXRpbmcgdGhlIHN0YXRlXHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VMaXN0QXJyOiBqb2luZWQgfSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VJbmRleDogdGhpcy5zdGF0ZS5yZXNvdXJjZUluZGV4ICsgMSB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGZvciAobGV0IHkgPSAwOyB5IDwgcHJpY2VGcm9tUmVzb3VyY2VzW3hdLmxlbmd0aDsgeSsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgIC8vTWFraW5nIG5ldyBpdGVtIHdpdGggbmVlZGVkIHZhbHVlcywgYW5kIGNvbmNhdGluZyBpdCB0byBhcnJheVxyXG4gICAgICAgICAgICAgICAgICAgIGxldCBuZXdKb2luZWQgPSB0aGlzLnN0YXRlLnJlc291cmNlTGlzdEFyci5jb25jYXQoe3Jlc291cmNlSW5kZXg6IHRoaXMuc3RhdGUucmVzb3VyY2VJbmRleCwgaWQ6IGlkRnJvbVJlc291cmNlc1t4XSwgIG5hbWU6dGl0bGVGcm9tUmVzb3VyY2VzW3hdLCBhdmF0YXJTb3VyY2U6aW1nRnJvbVJlc291cmNlc1t4XSwgcHJpY2U6cHJpY2VGcm9tUmVzb3VyY2VzW3hdW3ldfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgLy9VcGRhdGluZyB0aGUgc3RhdGVcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VMaXN0QXJyOiBuZXdKb2luZWQgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHJlc291cmNlSW5kZXg6IHRoaXMuc3RhdGUucmVzb3VyY2VJbmRleCArIDEgfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59XHJcbiAgXHJcbiAgZXhwb3J0IGRlZmF1bHQgSW5kZXg7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiQHNob3BpZnkvYXBwLWJyaWRnZS1yZWFjdFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAc2hvcGlmeS9wb2xhcmlzXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0XCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0L2pzeC1ydW50aW1lXCIpOyJdLCJzb3VyY2VSb290IjoiIn0=