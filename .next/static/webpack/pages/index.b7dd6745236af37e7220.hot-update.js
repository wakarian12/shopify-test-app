webpackHotUpdate_N_E("pages/index",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/esm/createClass */ "./node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inherits */ "./node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _shopify_polaris__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @shopify/polaris */ "./node_modules/@shopify/polaris/dist/esm/index.js");
/* harmony import */ var _shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @shopify/app-bridge-react */ "./node_modules/@shopify/app-bridge-react/index.js");
/* harmony import */ var _shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_10__);










function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return Object(_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

//Import Polaris elements, Resource Picker and React




var Index = /*#__PURE__*/function (_Component) {
  Object(_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__["default"])(Index, _Component);

  var _super = _createSuper(Index);

  function Index() {
    var _this;

    Object(_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, Index);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "state", {
      open: false,
      resourceListArr: [],
      deleteButtonShown: false,
      resourceIndex: 0
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleDelete", function (ind) {
      var pos = _this.state.resourceListArr.map(function (e) {
        return e.resourceIndex;
      }).indexOf(ind); //Detecting position of item by it`s 'resourceIndex' value


      _this.state.resourceListArr.splice(pos, 1); //Delete item from products array


      _this.setState({
        resourceListArr: _this.state.resourceListArr
      }); //Updating state
      //In case if all objects removed, hide 'Clear All' button


      if (_this.state.resourceListArr.length == 0) {
        _this.setState({
          deleteButtonShown: false
        });
      }
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleClear", function () {
      _this.setState({
        resourceListArr: []
      }); //Remove all items from product`s array


      _this.setState({
        deleteButtonShown: false
      }); //Hide 'Clear All' button


      _this.setState({
        resourceIndex: 0
      }); //Set 'ResourceIndex' counter to 0

    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleSelection", function (resources) {
      //mapping new arrays with needed products values
      var titleFromResources = resources.selection.map(function (product) {
        return product.title;
      });
      var idFromResources = resources.selection.map(function (product) {
        return product.id;
      });
      var priceFromResources = resources.selection.map(function (product) {
        return product.variants.map(function (e) {
          return e.price;
        });
      });
      var variantFromResources = resources.selection.map(function (product) {
        return product.variants.map(function (e) {
          return e.title;
        });
      });
      var imgFromResources = resources.selection.map(function (product) {
        return product.images[0].originalSrc;
      }); //Show 'Clear All' button

      _this.setState({
        deleteButtonShown: true
      }); //Close Resource Picker


      _this.setState({
        open: false
      }); //Adding products to product`s array


      for (var x = 0; x < priceFromResources.length; x++) {
        //if product don`t have variants
        if (priceFromResources[x].length == 1) {
          //Making new item with needed values, and concating it to array
          var joined = _this.state.resourceListArr.concat({
            resourceIndex: _this.state.resourceIndex,
            id: idFromResources[x],
            name: titleFromResources[x],
            avatarSource: imgFromResources[x],
            price: priceFromResources[x]
          }); //Updating the state


          _this.setState({
            resourceListArr: joined
          });

          _this.setState({
            resourceIndex: _this.state.resourceIndex + 1
          });
        } else {
          for (var y = 0; y < priceFromResources[x].length; y++) {
            //Making new item with needed values, and concating it to array
            var newJoined = _this.state.resourceListArr.concat({
              resourceIndex: _this.state.resourceIndex,
              id: idFromResources[x],
              name: titleFromResources[x],
              avatarSource: imgFromResources[x],
              price: priceFromResources[x][y]
            }); //Updating the state


            _this.setState({
              resourceListArr: newJoined
            });

            _this.setState({
              resourceIndex: _this.state.resourceIndex + 1
            });
          }
        }
      }
    });

    return _this;
  }

  Object(_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(Index, [{
    key: "render",
    //set state
    value: function render() {
      var _this2 = this;

      return (
        /*#__PURE__*/
        //Page styling using Polaris Page element, Text an Button
        Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxs"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Page"], {
          fullWidth: true,
          title: "Product Selection",
          primaryAction: {
            content: 'Select product',
            onAction: function onAction() {
              return _this2.setState({
                open: true
              });
            }
          },
          children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9__["ResourcePicker"]
          /*Resource Picker, change state on clothong and handling if items selected*/
          , {
            resourceType: "Product",
            open: this.state.open,
            onCancel: function onCancel() {
              return _this2.setState({
                open: false
              });
            },
            onSelection: function onSelection(resources) {
              return _this2.handleSelection(resources);
            }
          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
            children: // Display Clearing Button if any item is selected
            this.state.deleteButtonShown ? /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
              style: {
                marginBottom: '15px'
              },
              children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Button"], {
                destructive: true,
                onClick: function onClick() {
                  return _this2.handleClear();
                },
                children: "Clear all"
              })
            }) : null
          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Card"], {
            children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["ResourceList"]
            /*Adding Resource List wich consists of ResourceItems */
            , {
              resourceName: {
                singular: 'product',
                plural: 'product'
              },
              items: this.state.resourceListArr,
              renderItem: function renderItem(item) {
                var resourceIndex = item.resourceIndex,
                    id = item.id,
                    name = item.name,
                    avatarSource = item.avatarSource,
                    price = item.price,
                    variant = item.variant; //Setting Item

                return /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxs"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["ResourceItem"], {
                  id: id,
                  media: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Avatar"], {
                    customer: true,
                    size: "medium",
                    name: name,
                    source: avatarSource
                  }),
                  accessibilityLabel: "View details for ".concat(name),
                  children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("h2", {
                    children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["TextStyle"], {
                      variation: "strong",
                      children: name
                    })
                  }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
                    children: price
                  }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
                    style: {
                      position: 'absolute',
                      right: '20px',
                      marginTop: '-40px'
                    },
                    children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Button"], {
                      destructive: true,
                      resourceIndex: resourceIndex,
                      onClick: function onClick(ind) {
                        return _this2.handleDelete(item.resourceIndex);
                      },
                      children: "Delete"
                    })
                  })]
                });
              }
            })
          })]
        })
      );
    } //Handling Deletion of element from Resource List

  }]);

  return Index;
}(react__WEBPACK_IMPORTED_MODULE_10__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Index);

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvaW5kZXguanMiXSwibmFtZXMiOlsiSW5kZXgiLCJvcGVuIiwicmVzb3VyY2VMaXN0QXJyIiwiZGVsZXRlQnV0dG9uU2hvd24iLCJyZXNvdXJjZUluZGV4IiwiaW5kIiwicG9zIiwic3RhdGUiLCJtYXAiLCJlIiwiaW5kZXhPZiIsInNwbGljZSIsInNldFN0YXRlIiwibGVuZ3RoIiwicmVzb3VyY2VzIiwidGl0bGVGcm9tUmVzb3VyY2VzIiwic2VsZWN0aW9uIiwicHJvZHVjdCIsInRpdGxlIiwiaWRGcm9tUmVzb3VyY2VzIiwiaWQiLCJwcmljZUZyb21SZXNvdXJjZXMiLCJ2YXJpYW50cyIsInByaWNlIiwidmFyaWFudEZyb21SZXNvdXJjZXMiLCJpbWdGcm9tUmVzb3VyY2VzIiwiaW1hZ2VzIiwib3JpZ2luYWxTcmMiLCJ4Iiwiam9pbmVkIiwiY29uY2F0IiwibmFtZSIsImF2YXRhclNvdXJjZSIsInkiLCJuZXdKb2luZWQiLCJjb250ZW50Iiwib25BY3Rpb24iLCJoYW5kbGVTZWxlY3Rpb24iLCJtYXJnaW5Cb3R0b20iLCJoYW5kbGVDbGVhciIsInNpbmd1bGFyIiwicGx1cmFsIiwiaXRlbSIsInZhcmlhbnQiLCJwb3NpdGlvbiIsInJpZ2h0IiwibWFyZ2luVG9wIiwiaGFuZGxlRGVsZXRlIiwiQ29tcG9uZW50Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTs7SUFFTUEsSzs7Ozs7Ozs7Ozs7Ozs7OztnTkFDTTtBQUFFQyxVQUFJLEVBQUUsS0FBUjtBQUFlQyxxQkFBZSxFQUFFLEVBQWhDO0FBQW9DQyx1QkFBaUIsRUFBRSxLQUF2RDtBQUE4REMsbUJBQWEsRUFBRTtBQUE3RSxLOzt1TkEwRE8sVUFBQ0MsR0FBRCxFQUFTO0FBQ3BCLFVBQUlDLEdBQUcsR0FBRyxNQUFLQyxLQUFMLENBQVdMLGVBQVgsQ0FBMkJNLEdBQTNCLENBQStCLFVBQVNDLENBQVQsRUFBWTtBQUFFLGVBQU9BLENBQUMsQ0FBQ0wsYUFBVDtBQUF5QixPQUF0RSxFQUF3RU0sT0FBeEUsQ0FBZ0ZMLEdBQWhGLENBQVYsQ0FEb0IsQ0FDNEU7OztBQUNoRyxZQUFLRSxLQUFMLENBQVdMLGVBQVgsQ0FBMkJTLE1BQTNCLENBQWtDTCxHQUFsQyxFQUF1QyxDQUF2QyxFQUZvQixDQUV1Qjs7O0FBQzNDLFlBQUtNLFFBQUwsQ0FBYztBQUFFVix1QkFBZSxFQUFHLE1BQUtLLEtBQUwsQ0FBV0w7QUFBL0IsT0FBZCxFQUhvQixDQUc2QztBQUNqRTs7O0FBQ0EsVUFBSSxNQUFLSyxLQUFMLENBQVdMLGVBQVgsQ0FBMkJXLE1BQTNCLElBQXFDLENBQXpDLEVBQTRDO0FBQ3hDLGNBQUtELFFBQUwsQ0FBYztBQUFDVCwyQkFBaUIsRUFBRTtBQUFwQixTQUFkO0FBQ0g7QUFDSixLOztzTkFHYSxZQUFNO0FBQ2hCLFlBQUtTLFFBQUwsQ0FBYztBQUFFVix1QkFBZSxFQUFFO0FBQW5CLE9BQWQsRUFEZ0IsQ0FDd0I7OztBQUN4QyxZQUFLVSxRQUFMLENBQWM7QUFBQ1QseUJBQWlCLEVBQUU7QUFBcEIsT0FBZCxFQUZnQixDQUUwQjs7O0FBQzFDLFlBQUtTLFFBQUwsQ0FBYztBQUFFUixxQkFBYSxFQUFFO0FBQWpCLE9BQWQsRUFIZ0IsQ0FHb0I7O0FBQ3ZDLEs7OzBOQUdpQixVQUFDVSxTQUFELEVBQWU7QUFDN0I7QUFDQSxVQUFNQyxrQkFBa0IsR0FBR0QsU0FBUyxDQUFDRSxTQUFWLENBQW9CUixHQUFwQixDQUF3QixVQUFDUyxPQUFEO0FBQUEsZUFBYUEsT0FBTyxDQUFDQyxLQUFyQjtBQUFBLE9BQXhCLENBQTNCO0FBQ0EsVUFBTUMsZUFBZSxHQUFHTCxTQUFTLENBQUNFLFNBQVYsQ0FBb0JSLEdBQXBCLENBQXdCLFVBQUNTLE9BQUQ7QUFBQSxlQUFhQSxPQUFPLENBQUNHLEVBQXJCO0FBQUEsT0FBeEIsQ0FBeEI7QUFDQSxVQUFNQyxrQkFBa0IsR0FBR1AsU0FBUyxDQUFDRSxTQUFWLENBQW9CUixHQUFwQixDQUF3QixVQUFDUyxPQUFEO0FBQUEsZUFBYUEsT0FBTyxDQUFDSyxRQUFSLENBQWlCZCxHQUFqQixDQUFxQixVQUFTQyxDQUFULEVBQVk7QUFBRSxpQkFBT0EsQ0FBQyxDQUFDYyxLQUFUO0FBQWlCLFNBQXBELENBQWI7QUFBQSxPQUF4QixDQUEzQjtBQUNBLFVBQU1DLG9CQUFvQixHQUFHVixTQUFTLENBQUNFLFNBQVYsQ0FBb0JSLEdBQXBCLENBQXdCLFVBQUNTLE9BQUQ7QUFBQSxlQUFhQSxPQUFPLENBQUNLLFFBQVIsQ0FBaUJkLEdBQWpCLENBQXFCLFVBQVNDLENBQVQsRUFBWTtBQUFFLGlCQUFPQSxDQUFDLENBQUNTLEtBQVQ7QUFBaUIsU0FBcEQsQ0FBYjtBQUFBLE9BQXhCLENBQTdCO0FBQ0EsVUFBTU8sZ0JBQWdCLEdBQUdYLFNBQVMsQ0FBQ0UsU0FBVixDQUFvQlIsR0FBcEIsQ0FBd0IsVUFBQ1MsT0FBRDtBQUFBLGVBQWFBLE9BQU8sQ0FBQ1MsTUFBUixDQUFlLENBQWYsRUFBa0JDLFdBQS9CO0FBQUEsT0FBeEIsQ0FBekIsQ0FONkIsQ0FPN0I7O0FBQ0EsWUFBS2YsUUFBTCxDQUFjO0FBQUNULHlCQUFpQixFQUFFO0FBQXBCLE9BQWQsRUFSNkIsQ0FTN0I7OztBQUNBLFlBQUtTLFFBQUwsQ0FBYztBQUFDWCxZQUFJLEVBQUU7QUFBUCxPQUFkLEVBVjZCLENBVzdCOzs7QUFDQSxXQUFLLElBQUkyQixDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHUCxrQkFBa0IsQ0FBQ1IsTUFBdkMsRUFBK0NlLENBQUMsRUFBaEQsRUFBb0Q7QUFDaEQ7QUFDQSxZQUFJUCxrQkFBa0IsQ0FBQ08sQ0FBRCxDQUFsQixDQUFzQmYsTUFBdEIsSUFBZ0MsQ0FBcEMsRUFBc0M7QUFDakM7QUFDRCxjQUFJZ0IsTUFBTSxHQUFHLE1BQUt0QixLQUFMLENBQVdMLGVBQVgsQ0FBMkI0QixNQUEzQixDQUFrQztBQUFDMUIseUJBQWEsRUFBRSxNQUFLRyxLQUFMLENBQVdILGFBQTNCO0FBQTBDZ0IsY0FBRSxFQUFFRCxlQUFlLENBQUNTLENBQUQsQ0FBN0Q7QUFBbUVHLGdCQUFJLEVBQUNoQixrQkFBa0IsQ0FBQ2EsQ0FBRCxDQUExRjtBQUErRkksd0JBQVksRUFBQ1AsZ0JBQWdCLENBQUNHLENBQUQsQ0FBNUg7QUFBaUlMLGlCQUFLLEVBQUNGLGtCQUFrQixDQUFDTyxDQUFEO0FBQXpKLFdBQWxDLENBQWIsQ0FGa0MsQ0FHbEM7OztBQUNBLGdCQUFLaEIsUUFBTCxDQUFjO0FBQUVWLDJCQUFlLEVBQUUyQjtBQUFuQixXQUFkOztBQUNBLGdCQUFLakIsUUFBTCxDQUFjO0FBQUVSLHlCQUFhLEVBQUUsTUFBS0csS0FBTCxDQUFXSCxhQUFYLEdBQTJCO0FBQTVDLFdBQWQ7QUFDSCxTQU5ELE1BT0s7QUFDRCxlQUFLLElBQUk2QixDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHWixrQkFBa0IsQ0FBQ08sQ0FBRCxDQUFsQixDQUFzQmYsTUFBMUMsRUFBa0RvQixDQUFDLEVBQW5ELEVBQXVEO0FBQ2xEO0FBQ0QsZ0JBQUlDLFNBQVMsR0FBRyxNQUFLM0IsS0FBTCxDQUFXTCxlQUFYLENBQTJCNEIsTUFBM0IsQ0FBa0M7QUFBQzFCLDJCQUFhLEVBQUUsTUFBS0csS0FBTCxDQUFXSCxhQUEzQjtBQUEwQ2dCLGdCQUFFLEVBQUVELGVBQWUsQ0FBQ1MsQ0FBRCxDQUE3RDtBQUFtRUcsa0JBQUksRUFBQ2hCLGtCQUFrQixDQUFDYSxDQUFELENBQTFGO0FBQStGSSwwQkFBWSxFQUFDUCxnQkFBZ0IsQ0FBQ0csQ0FBRCxDQUE1SDtBQUFpSUwsbUJBQUssRUFBQ0Ysa0JBQWtCLENBQUNPLENBQUQsQ0FBbEIsQ0FBc0JLLENBQXRCO0FBQXZJLGFBQWxDLENBQWhCLENBRm1ELENBR25EOzs7QUFDQSxrQkFBS3JCLFFBQUwsQ0FBYztBQUFFViw2QkFBZSxFQUFFZ0M7QUFBbkIsYUFBZDs7QUFDQSxrQkFBS3RCLFFBQUwsQ0FBYztBQUFFUiwyQkFBYSxFQUFFLE1BQUtHLEtBQUwsQ0FBV0gsYUFBWCxHQUEyQjtBQUE1QyxhQUFkO0FBQ0g7QUFDSjtBQUNKO0FBQ0osSzs7Ozs7OztBQTNHdUY7NkJBRS9FO0FBQUE7O0FBQ0w7QUFBQTtBQUNBO0FBQ0EsdUVBQUMscURBQUQ7QUFDSSxtQkFBUyxNQURiO0FBRUksZUFBSyxFQUFDLG1CQUZWO0FBR0ksdUJBQWEsRUFBRTtBQUNYK0IsbUJBQU8sRUFBRSxnQkFERTtBQUVYQyxvQkFBUSxFQUFFO0FBQUEscUJBQU0sTUFBSSxDQUFDeEIsUUFBTCxDQUFjO0FBQUNYLG9CQUFJLEVBQUU7QUFBUCxlQUFkLENBQU47QUFBQTtBQUZDLFdBSG5CO0FBQUEsa0NBUVEsOERBQUMsd0VBQWM7QUFBRTtBQUFqQjtBQUNJLHdCQUFZLEVBQUMsU0FEakI7QUFFSSxnQkFBSSxFQUFFLEtBQUtNLEtBQUwsQ0FBV04sSUFGckI7QUFHSSxvQkFBUSxFQUFHO0FBQUEscUJBQU0sTUFBSSxDQUFDVyxRQUFMLENBQWM7QUFBQ1gsb0JBQUksRUFBRTtBQUFQLGVBQWQsQ0FBTjtBQUFBLGFBSGY7QUFJSSx1QkFBVyxFQUFFLHFCQUFDYSxTQUFEO0FBQUEscUJBQWUsTUFBSSxDQUFDdUIsZUFBTCxDQUFxQnZCLFNBQXJCLENBQWY7QUFBQTtBQUpqQixZQVJSLGVBY1E7QUFBQSxzQkFDSztBQUNELGlCQUFLUCxLQUFMLENBQVdKLGlCQUFYLGdCQUNJO0FBQUssbUJBQUssRUFBRTtBQUFDbUMsNEJBQVksRUFBRTtBQUFmLGVBQVo7QUFBQSxxQ0FDSSw4REFBQyx1REFBRDtBQUFRLDJCQUFXLE1BQW5CO0FBQ0EsdUJBQU8sRUFBRTtBQUFBLHlCQUFNLE1BQUksQ0FBQ0MsV0FBTCxFQUFOO0FBQUEsaUJBRFQ7QUFBQTtBQUFBO0FBREosY0FESixHQUtFO0FBUE4sWUFkUixlQXVCUSw4REFBQyxxREFBRDtBQUFBLG1DQUNBLDhEQUFDLDZEQUFZO0FBQUM7QUFBZDtBQUNJLDBCQUFZLEVBQUU7QUFBQ0Msd0JBQVEsRUFBRSxTQUFYO0FBQXNCQyxzQkFBTSxFQUFFO0FBQTlCLGVBRGxCO0FBRUksbUJBQUssRUFBRyxLQUFLbEMsS0FBTCxDQUFXTCxlQUZ2QjtBQUdJLHdCQUFVLEVBQUUsb0JBQUN3QyxJQUFELEVBQVU7QUFBQSxvQkFDZnRDLGFBRGUsR0FDMkNzQyxJQUQzQyxDQUNmdEMsYUFEZTtBQUFBLG9CQUNBZ0IsRUFEQSxHQUMyQ3NCLElBRDNDLENBQ0F0QixFQURBO0FBQUEsb0JBQ0tXLElBREwsR0FDMkNXLElBRDNDLENBQ0tYLElBREw7QUFBQSxvQkFDV0MsWUFEWCxHQUMyQ1UsSUFEM0MsQ0FDV1YsWUFEWDtBQUFBLG9CQUN5QlQsS0FEekIsR0FDMkNtQixJQUQzQyxDQUN5Qm5CLEtBRHpCO0FBQUEsb0JBQ2dDb0IsT0FEaEMsR0FDMkNELElBRDNDLENBQ2dDQyxPQURoQyxFQUNpRDs7QUFDdkUsb0NBQ0ksK0RBQUMsNkRBQUQ7QUFDQSxvQkFBRSxFQUFFdkIsRUFESjtBQUVBLHVCQUFLLGVBQUcsOERBQUMsdURBQUQ7QUFBUSw0QkFBUSxNQUFoQjtBQUFpQix3QkFBSSxFQUFDLFFBQXRCO0FBQStCLHdCQUFJLEVBQUVXLElBQXJDO0FBQTJDLDBCQUFNLEVBQUVDO0FBQW5ELG9CQUZSO0FBR0Esb0NBQWtCLDZCQUFzQkQsSUFBdEIsQ0FIbEI7QUFBQSwwQ0FLQTtBQUFBLDJDQUNJLDhEQUFDLDBEQUFEO0FBQVcsK0JBQVMsRUFBQyxRQUFyQjtBQUFBLGdDQUErQkE7QUFBL0I7QUFESixvQkFMQSxlQVFBO0FBQUEsOEJBQU1SO0FBQU4sb0JBUkEsZUFTQTtBQUFLLHlCQUFLLEVBQUU7QUFBQ3FCLDhCQUFRLEVBQUMsVUFBVjtBQUFzQkMsMkJBQUssRUFBQyxNQUE1QjtBQUFvQ0MsK0JBQVMsRUFBRTtBQUEvQyxxQkFBWjtBQUFBLDJDQUNJLDhEQUFDLHVEQUFEO0FBQVEsaUNBQVcsTUFBbkI7QUFBb0IsbUNBQWEsRUFBRTFDLGFBQW5DO0FBQ0EsNkJBQU8sRUFBRSxpQkFBQ0MsR0FBRDtBQUFBLCtCQUFTLE1BQUksQ0FBQzBDLFlBQUwsQ0FBa0JMLElBQUksQ0FBQ3RDLGFBQXZCLENBQVQ7QUFBQSx1QkFEVDtBQUFBO0FBQUE7QUFESixvQkFUQTtBQUFBLGtCQURKO0FBZ0JDO0FBckJMO0FBREEsWUF2QlI7QUFBQTtBQUZBO0FBb0RILEssQ0FFRDs7Ozs7RUExRGdCNEMsZ0Q7O0FBZ0hIaEQsb0VBQWYiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvaW5kZXguYjdkZDY3NDUyMzZhZjM3ZTcyMjAuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8vSW1wb3J0IFBvbGFyaXMgZWxlbWVudHMsIFJlc291cmNlIFBpY2tlciBhbmQgUmVhY3RcclxuaW1wb3J0IHsgUGFnZSwgQ2FyZCwgUmVzb3VyY2VMaXN0LCBSZXNvdXJjZUl0ZW0sIEF2YXRhciwgVGV4dFN0eWxlLCBCdXR0b24gfSBmcm9tIFwiQHNob3BpZnkvcG9sYXJpc1wiO1xyXG5pbXBvcnQgeyBSZXNvdXJjZVBpY2tlciB9IGZyb20gXCJAc2hvcGlmeS9hcHAtYnJpZGdlLXJlYWN0XCI7XHJcbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XHJcblxyXG5jbGFzcyBJbmRleCBleHRlbmRzIENvbXBvbmVudCB7XHJcbiAgICBzdGF0ZSA9IHsgb3BlbjogZmFsc2UsIHJlc291cmNlTGlzdEFycjogW10sIGRlbGV0ZUJ1dHRvblNob3duOiBmYWxzZSwgcmVzb3VyY2VJbmRleDogMCB9Ly9zZXQgc3RhdGVcclxuXHJcbiAgICByZW5kZXIoKSB7IFxyXG4gICAgICAgIHJldHVybiAoIFxyXG4gICAgICAgIC8vUGFnZSBzdHlsaW5nIHVzaW5nIFBvbGFyaXMgUGFnZSBlbGVtZW50LCBUZXh0IGFuIEJ1dHRvblxyXG4gICAgICAgIDxQYWdlICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGZ1bGxXaWR0aFxyXG4gICAgICAgICAgICB0aXRsZT1cIlByb2R1Y3QgU2VsZWN0aW9uXCJcclxuICAgICAgICAgICAgcHJpbWFyeUFjdGlvbj17e1xyXG4gICAgICAgICAgICAgICAgY29udGVudDogJ1NlbGVjdCBwcm9kdWN0JyxcclxuICAgICAgICAgICAgICAgIG9uQWN0aW9uOiAoKSA9PiB0aGlzLnNldFN0YXRlKHtvcGVuOiB0cnVlfSlcclxuICAgICAgICAgICAgfX1cclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgPFJlc291cmNlUGlja2VyICAvKlJlc291cmNlIFBpY2tlciwgY2hhbmdlIHN0YXRlIG9uIGNsb3Rob25nIGFuZCBoYW5kbGluZyBpZiBpdGVtcyBzZWxlY3RlZCovXHJcbiAgICAgICAgICAgICAgICAgICAgcmVzb3VyY2VUeXBlPVwiUHJvZHVjdFwiXHJcbiAgICAgICAgICAgICAgICAgICAgb3Blbj17dGhpcy5zdGF0ZS5vcGVufVxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2FuY2VsPXsgKCkgPT4gdGhpcy5zZXRTdGF0ZSh7b3BlbjogZmFsc2V9KX1cclxuICAgICAgICAgICAgICAgICAgICBvblNlbGVjdGlvbj17KHJlc291cmNlcykgPT4gdGhpcy5oYW5kbGVTZWxlY3Rpb24ocmVzb3VyY2VzKX0gXHJcbiAgICAgICAgICAgICAgICAvPiBcclxuICAgICAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgey8vIERpc3BsYXkgQ2xlYXJpbmcgQnV0dG9uIGlmIGFueSBpdGVtIGlzIHNlbGVjdGVkXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5kZWxldGVCdXR0b25TaG93biA/IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IHN0eWxlPXt7bWFyZ2luQm90dG9tOiAnMTVweCd9fT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxCdXR0b24gZGVzdHJ1Y3RpdmUgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB0aGlzLmhhbmRsZUNsZWFyKCl9PkNsZWFyIGFsbDwvQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj4gXHJcbiAgICAgICAgICAgICAgICAgICAgOiBudWxsfSBcclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPENhcmQ+XHJcbiAgICAgICAgICAgICAgICA8UmVzb3VyY2VMaXN0IC8qQWRkaW5nIFJlc291cmNlIExpc3Qgd2ljaCBjb25zaXN0cyBvZiBSZXNvdXJjZUl0ZW1zICovXHJcbiAgICAgICAgICAgICAgICAgICAgcmVzb3VyY2VOYW1lPXt7c2luZ3VsYXI6ICdwcm9kdWN0JywgcGx1cmFsOiAncHJvZHVjdCd9fVxyXG4gICAgICAgICAgICAgICAgICAgIGl0ZW1zPXsgdGhpcy5zdGF0ZS5yZXNvdXJjZUxpc3RBcnIgfVxyXG4gICAgICAgICAgICAgICAgICAgIHJlbmRlckl0ZW09eyhpdGVtKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qge3Jlc291cmNlSW5kZXgsIGlkLCAgbmFtZSwgYXZhdGFyU291cmNlLCBwcmljZSwgdmFyaWFudH0gPSBpdGVtOyAvL1NldHRpbmcgSXRlbVxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxSZXNvdXJjZUl0ZW1cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ9e2lkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtZWRpYT0gezxBdmF0YXIgY3VzdG9tZXIgc2l6ZT1cIm1lZGl1bVwiIG5hbWU9e25hbWV9IHNvdXJjZT17YXZhdGFyU291cmNlfSAvPn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgYWNjZXNzaWJpbGl0eUxhYmVsPXtgVmlldyBkZXRhaWxzIGZvciAke25hbWV9YH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8aDI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8VGV4dFN0eWxlIHZhcmlhdGlvbj1cInN0cm9uZ1wiPntuYW1lfTwvVGV4dFN0eWxlPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2gyPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2PntwcmljZX08L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBzdHlsZT17e3Bvc2l0aW9uOidhYnNvbHV0ZScsIHJpZ2h0OicyMHB4JywgbWFyZ2luVG9wOiAnLTQwcHgnfX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QnV0dG9uIGRlc3RydWN0aXZlIHJlc291cmNlSW5kZXg9e3Jlc291cmNlSW5kZXh9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoaW5kKSA9PiB0aGlzLmhhbmRsZURlbGV0ZShpdGVtLnJlc291cmNlSW5kZXgpfT5EZWxldGU8L0J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvUmVzb3VyY2VJdGVtPlxyXG4gICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICAgICAgfX1cclxuICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICA8L0NhcmQ+XHJcbiAgICAgICAgPC9QYWdlPiBcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8vSGFuZGxpbmcgRGVsZXRpb24gb2YgZWxlbWVudCBmcm9tIFJlc291cmNlIExpc3RcclxuICAgIGhhbmRsZURlbGV0ZSA9IChpbmQpID0+IHtcclxuICAgICAgICB2YXIgcG9zID0gdGhpcy5zdGF0ZS5yZXNvdXJjZUxpc3RBcnIubWFwKGZ1bmN0aW9uKGUpIHsgcmV0dXJuIGUucmVzb3VyY2VJbmRleDsgfSkuaW5kZXhPZihpbmQpOyAvL0RldGVjdGluZyBwb3NpdGlvbiBvZiBpdGVtIGJ5IGl0YHMgJ3Jlc291cmNlSW5kZXgnIHZhbHVlXHJcbiAgICAgICAgdGhpcy5zdGF0ZS5yZXNvdXJjZUxpc3RBcnIuc3BsaWNlKHBvcywgMSk7IC8vRGVsZXRlIGl0ZW0gZnJvbSBwcm9kdWN0cyBhcnJheVxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyByZXNvdXJjZUxpc3RBcnI6ICB0aGlzLnN0YXRlLnJlc291cmNlTGlzdEFyciB9KTsgLy9VcGRhdGluZyBzdGF0ZVxyXG4gICAgICAgIC8vSW4gY2FzZSBpZiBhbGwgb2JqZWN0cyByZW1vdmVkLCBoaWRlICdDbGVhciBBbGwnIGJ1dHRvblxyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnJlc291cmNlTGlzdEFyci5sZW5ndGggPT0gMCkge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtkZWxldGVCdXR0b25TaG93bjogZmFsc2V9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy9IYW5kbGluZyAnQ2xlYXIgQWxsJyBidXR0b24gYWN0aW9uXHJcbiAgICBoYW5kbGVDbGVhciA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VMaXN0QXJyOiBbXSB9KTsgLy9SZW1vdmUgYWxsIGl0ZW1zIGZyb20gcHJvZHVjdGBzIGFycmF5XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7ZGVsZXRlQnV0dG9uU2hvd246IGZhbHNlfSk7Ly9IaWRlICdDbGVhciBBbGwnIGJ1dHRvblxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyByZXNvdXJjZUluZGV4OiAwIH0pOy8vU2V0ICdSZXNvdXJjZUluZGV4JyBjb3VudGVyIHRvIDBcclxuICAgIH1cclxuXHJcbiAgICAvL0hhbmRsaW5nIGl0ZW1zIHNlbGVjdGlvbiBmcm9tIHJlc291cmNlIHBpY2tlclxyXG4gICAgaGFuZGxlU2VsZWN0aW9uID0gKHJlc291cmNlcykgPT4ge1xyXG4gICAgICAgIC8vbWFwcGluZyBuZXcgYXJyYXlzIHdpdGggbmVlZGVkIHByb2R1Y3RzIHZhbHVlc1xyXG4gICAgICAgIGNvbnN0IHRpdGxlRnJvbVJlc291cmNlcyA9IHJlc291cmNlcy5zZWxlY3Rpb24ubWFwKChwcm9kdWN0KSA9PiBwcm9kdWN0LnRpdGxlKTtcclxuICAgICAgICBjb25zdCBpZEZyb21SZXNvdXJjZXMgPSByZXNvdXJjZXMuc2VsZWN0aW9uLm1hcCgocHJvZHVjdCkgPT4gcHJvZHVjdC5pZCk7XHJcbiAgICAgICAgY29uc3QgcHJpY2VGcm9tUmVzb3VyY2VzID0gcmVzb3VyY2VzLnNlbGVjdGlvbi5tYXAoKHByb2R1Y3QpID0+IHByb2R1Y3QudmFyaWFudHMubWFwKGZ1bmN0aW9uKGUpIHsgcmV0dXJuIGUucHJpY2U7IH0pKTtcclxuICAgICAgICBjb25zdCB2YXJpYW50RnJvbVJlc291cmNlcyA9IHJlc291cmNlcy5zZWxlY3Rpb24ubWFwKChwcm9kdWN0KSA9PiBwcm9kdWN0LnZhcmlhbnRzLm1hcChmdW5jdGlvbihlKSB7IHJldHVybiBlLnRpdGxlOyB9KSk7XHJcbiAgICAgICAgY29uc3QgaW1nRnJvbVJlc291cmNlcyA9IHJlc291cmNlcy5zZWxlY3Rpb24ubWFwKChwcm9kdWN0KSA9PiBwcm9kdWN0LmltYWdlc1swXS5vcmlnaW5hbFNyYyk7XHJcbiAgICAgICAgLy9TaG93ICdDbGVhciBBbGwnIGJ1dHRvblxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe2RlbGV0ZUJ1dHRvblNob3duOiB0cnVlfSk7XHJcbiAgICAgICAgLy9DbG9zZSBSZXNvdXJjZSBQaWNrZXJcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtvcGVuOiBmYWxzZX0pO1xyXG4gICAgICAgIC8vQWRkaW5nIHByb2R1Y3RzIHRvIHByb2R1Y3RgcyBhcnJheVxyXG4gICAgICAgIGZvciAobGV0IHggPSAwOyB4IDwgcHJpY2VGcm9tUmVzb3VyY2VzLmxlbmd0aDsgeCsrKSB7XHJcbiAgICAgICAgICAgIC8vaWYgcHJvZHVjdCBkb25gdCBoYXZlIHZhcmlhbnRzXHJcbiAgICAgICAgICAgIGlmIChwcmljZUZyb21SZXNvdXJjZXNbeF0ubGVuZ3RoID09IDEpe1xyXG4gICAgICAgICAgICAgICAgIC8vTWFraW5nIG5ldyBpdGVtIHdpdGggbmVlZGVkIHZhbHVlcywgYW5kIGNvbmNhdGluZyBpdCB0byBhcnJheVxyXG4gICAgICAgICAgICAgICAgbGV0IGpvaW5lZCA9IHRoaXMuc3RhdGUucmVzb3VyY2VMaXN0QXJyLmNvbmNhdCh7cmVzb3VyY2VJbmRleDogdGhpcy5zdGF0ZS5yZXNvdXJjZUluZGV4LCBpZDogaWRGcm9tUmVzb3VyY2VzW3hdLCAgbmFtZTp0aXRsZUZyb21SZXNvdXJjZXNbeF0sIGF2YXRhclNvdXJjZTppbWdGcm9tUmVzb3VyY2VzW3hdLCBwcmljZTpwcmljZUZyb21SZXNvdXJjZXNbeF19KTtcclxuICAgICAgICAgICAgICAgIC8vVXBkYXRpbmcgdGhlIHN0YXRlXHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VMaXN0QXJyOiBqb2luZWQgfSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VJbmRleDogdGhpcy5zdGF0ZS5yZXNvdXJjZUluZGV4ICsgMSB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGZvciAobGV0IHkgPSAwOyB5IDwgcHJpY2VGcm9tUmVzb3VyY2VzW3hdLmxlbmd0aDsgeSsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgIC8vTWFraW5nIG5ldyBpdGVtIHdpdGggbmVlZGVkIHZhbHVlcywgYW5kIGNvbmNhdGluZyBpdCB0byBhcnJheVxyXG4gICAgICAgICAgICAgICAgICAgIGxldCBuZXdKb2luZWQgPSB0aGlzLnN0YXRlLnJlc291cmNlTGlzdEFyci5jb25jYXQoe3Jlc291cmNlSW5kZXg6IHRoaXMuc3RhdGUucmVzb3VyY2VJbmRleCwgaWQ6IGlkRnJvbVJlc291cmNlc1t4XSwgIG5hbWU6dGl0bGVGcm9tUmVzb3VyY2VzW3hdLCBhdmF0YXJTb3VyY2U6aW1nRnJvbVJlc291cmNlc1t4XSwgcHJpY2U6cHJpY2VGcm9tUmVzb3VyY2VzW3hdW3ldfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgLy9VcGRhdGluZyB0aGUgc3RhdGVcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VMaXN0QXJyOiBuZXdKb2luZWQgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHJlc291cmNlSW5kZXg6IHRoaXMuc3RhdGUucmVzb3VyY2VJbmRleCArIDEgfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59XHJcbiAgXHJcbiAgZXhwb3J0IGRlZmF1bHQgSW5kZXg7Il0sInNvdXJjZVJvb3QiOiIifQ==