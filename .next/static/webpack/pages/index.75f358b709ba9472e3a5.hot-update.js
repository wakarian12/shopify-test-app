webpackHotUpdate_N_E("pages/index",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/esm/createClass */ "./node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inherits */ "./node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _shopify_polaris__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @shopify/polaris */ "./node_modules/@shopify/polaris/dist/esm/index.js");
/* harmony import */ var _shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @shopify/app-bridge-react */ "./node_modules/@shopify/app-bridge-react/index.js");
/* harmony import */ var _shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_10__);










function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return Object(_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

//Import Polaris elements, Resource Picker and React




var Index = /*#__PURE__*/function (_Component) {
  Object(_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__["default"])(Index, _Component);

  var _super = _createSuper(Index);

  function Index() {
    var _this;

    Object(_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, Index);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "state", {
      open: false,
      resourceListArr: [],
      deleteButtonShown: false,
      resourceIndex: 0
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleDelete", function (ind) {
      var pos = _this.state.resourceListArr.map(function (e) {
        return e.resourceIndex;
      }).indexOf(ind); //Detecting position of item by it`s 'resourceIndex' value


      _this.state.resourceListArr.splice(pos, 1); //Delete item from products array


      _this.setState({
        resourceListArr: _this.state.resourceListArr
      }); //Updating state
      //In case if all objects removed, hide 'Clear All' button


      if (_this.state.resourceListArr.length == 0) {
        _this.setState({
          deleteButtonShown: false
        });
      }
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleClear", function () {
      _this.setState({
        resourceListArr: []
      }); //Remove all items from product`s array


      _this.setState({
        deleteButtonShown: false
      }); //Hide 'Clear All' button


      _this.setState({
        resourceIndex: 0
      }); //Set 'ResourceIndex' counter to 0

    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleSelection", function (resources) {
      //mapping new arrays with needed products values
      var titleFromResources = resources.selection.map(function (product) {
        return product.title;
      });
      var idFromResources = resources.selection.map(function (product) {
        return product.id;
      }); //const priceFromResources = resources.selection.map((product) => product.variants[0].price);

      var priceFromResources = resources.selection.map(function (product) {
        return product.variants.map(function (e) {
          return e.price;
        });
      });
      var imgFromResources = resources.selection.map(function (product) {
        return product.images[0].originalSrc;
      }); //Show 'Clear All' button

      _this.setState({
        deleteButtonShown: true
      }); //Close Resource Picker


      _this.setState({
        open: false
      }); //Adding products to product`s array


      for (var x = 0; x < priceFromResources.length; x++) {
        if (priceFromResources[x].length == 1) {
          var _joined = _this.state.resourceListArr.concat({
            resourceIndex: _this.state.resourceIndex,
            id: idFromResources[x],
            name: titleFromResources[x],
            avatarSource: imgFromResources[x],
            price: priceFromResources[x]
          });

          _this.setState({
            resourceListArr: _joined
          });

          _this.setState({
            resourceIndex: _this.state.resourceIndex + 1
          });
        } else {
          for (var y = 0; y < priceFromResources.length; y++) {
            var newJoined = _this.state.resourceListArr.concat({
              resourceIndex: _this.state.resourceIndex,
              id: idFromResources[x],
              name: titleFromResources[x],
              avatarSource: imgFromResources[x],
              price: priceFromResources[y]
            });

            _this.setState({
              resourceListArr: joined
            });

            _this.setState({
              resourceIndex: _this.state.resourceIndex + 1
            });
          }
        }
      }
    });

    return _this;
  }

  Object(_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(Index, [{
    key: "render",
    //set state
    value: function render() {
      var _this2 = this;

      return (
        /*#__PURE__*/
        //Page styling using Polaris Page element, Text an Button
        Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxs"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Page"], {
          fullWidth: true,
          title: "Product Selection",
          primaryAction: {
            content: 'Select product',
            onAction: function onAction() {
              return _this2.setState({
                open: true
              });
            }
          },
          children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9__["ResourcePicker"]
          /*Resource Picker, change state on clothong and handling if items selected*/
          , {
            resourceType: "Product",
            open: this.state.open,
            onCancel: function onCancel() {
              return _this2.setState({
                open: false
              });
            },
            onSelection: function onSelection(resources) {
              return _this2.handleSelection(resources);
            }
          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
            children: // Display Clearing Button if any item is selected
            this.state.deleteButtonShown ? /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
              style: {
                marginBottom: '15px'
              },
              children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Button"], {
                destructive: true,
                onClick: function onClick() {
                  return _this2.handleClear();
                },
                children: "Clear all"
              })
            }) : null
          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Card"], {
            children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["ResourceList"]
            /*Adding Resource List wich consists of ResourceItems */
            , {
              resourceName: {
                singular: 'product',
                plural: 'product'
              },
              items: this.state.resourceListArr,
              renderItem: function renderItem(item) {
                var resourceIndex = item.resourceIndex,
                    id = item.id,
                    name = item.name,
                    avatarSource = item.avatarSource,
                    price = item.price; //Setting Item

                return /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxs"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["ResourceItem"], {
                  id: id,
                  media: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Avatar"], {
                    customer: true,
                    size: "medium",
                    name: name,
                    source: avatarSource
                  }),
                  accessibilityLabel: "View details for ".concat(name),
                  children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("h2", {
                    children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["TextStyle"], {
                      variation: "strong",
                      children: name
                    })
                  }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
                    children: price
                  }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
                    style: {
                      position: 'absolute',
                      right: '20px',
                      marginTop: '-40px'
                    },
                    children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Button"], {
                      destructive: true,
                      resourceIndex: resourceIndex,
                      onClick: function onClick(ind) {
                        return _this2.handleDelete(item.resourceIndex);
                      },
                      children: "Delete"
                    })
                  })]
                });
              }
            })
          })]
        })
      );
    } //Handling Deletion of element from Resource List

  }]);

  return Index;
}(react__WEBPACK_IMPORTED_MODULE_10__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Index);

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvaW5kZXguanMiXSwibmFtZXMiOlsiSW5kZXgiLCJvcGVuIiwicmVzb3VyY2VMaXN0QXJyIiwiZGVsZXRlQnV0dG9uU2hvd24iLCJyZXNvdXJjZUluZGV4IiwiaW5kIiwicG9zIiwic3RhdGUiLCJtYXAiLCJlIiwiaW5kZXhPZiIsInNwbGljZSIsInNldFN0YXRlIiwibGVuZ3RoIiwicmVzb3VyY2VzIiwidGl0bGVGcm9tUmVzb3VyY2VzIiwic2VsZWN0aW9uIiwicHJvZHVjdCIsInRpdGxlIiwiaWRGcm9tUmVzb3VyY2VzIiwiaWQiLCJwcmljZUZyb21SZXNvdXJjZXMiLCJ2YXJpYW50cyIsInByaWNlIiwiaW1nRnJvbVJlc291cmNlcyIsImltYWdlcyIsIm9yaWdpbmFsU3JjIiwieCIsImpvaW5lZCIsImNvbmNhdCIsIm5hbWUiLCJhdmF0YXJTb3VyY2UiLCJ5IiwibmV3Sm9pbmVkIiwiY29udGVudCIsIm9uQWN0aW9uIiwiaGFuZGxlU2VsZWN0aW9uIiwibWFyZ2luQm90dG9tIiwiaGFuZGxlQ2xlYXIiLCJzaW5ndWxhciIsInBsdXJhbCIsIml0ZW0iLCJwb3NpdGlvbiIsInJpZ2h0IiwibWFyZ2luVG9wIiwiaGFuZGxlRGVsZXRlIiwiQ29tcG9uZW50Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTs7SUFFTUEsSzs7Ozs7Ozs7Ozs7Ozs7OztnTkFDTTtBQUFFQyxVQUFJLEVBQUUsS0FBUjtBQUFlQyxxQkFBZSxFQUFFLEVBQWhDO0FBQW9DQyx1QkFBaUIsRUFBRSxLQUF2RDtBQUE4REMsbUJBQWEsRUFBRTtBQUE3RSxLOzt1TkEwRE8sVUFBQ0MsR0FBRCxFQUFTO0FBQ3BCLFVBQUlDLEdBQUcsR0FBRyxNQUFLQyxLQUFMLENBQVdMLGVBQVgsQ0FBMkJNLEdBQTNCLENBQStCLFVBQVNDLENBQVQsRUFBWTtBQUFFLGVBQU9BLENBQUMsQ0FBQ0wsYUFBVDtBQUF5QixPQUF0RSxFQUF3RU0sT0FBeEUsQ0FBZ0ZMLEdBQWhGLENBQVYsQ0FEb0IsQ0FDNEU7OztBQUNoRyxZQUFLRSxLQUFMLENBQVdMLGVBQVgsQ0FBMkJTLE1BQTNCLENBQWtDTCxHQUFsQyxFQUF1QyxDQUF2QyxFQUZvQixDQUV1Qjs7O0FBQzNDLFlBQUtNLFFBQUwsQ0FBYztBQUFFVix1QkFBZSxFQUFHLE1BQUtLLEtBQUwsQ0FBV0w7QUFBL0IsT0FBZCxFQUhvQixDQUc2QztBQUNqRTs7O0FBQ0EsVUFBSSxNQUFLSyxLQUFMLENBQVdMLGVBQVgsQ0FBMkJXLE1BQTNCLElBQXFDLENBQXpDLEVBQTRDO0FBQ3hDLGNBQUtELFFBQUwsQ0FBYztBQUFDVCwyQkFBaUIsRUFBRTtBQUFwQixTQUFkO0FBQ0g7QUFDSixLOztzTkFHYSxZQUFNO0FBQ2hCLFlBQUtTLFFBQUwsQ0FBYztBQUFFVix1QkFBZSxFQUFFO0FBQW5CLE9BQWQsRUFEZ0IsQ0FDd0I7OztBQUN4QyxZQUFLVSxRQUFMLENBQWM7QUFBQ1QseUJBQWlCLEVBQUU7QUFBcEIsT0FBZCxFQUZnQixDQUUwQjs7O0FBQzFDLFlBQUtTLFFBQUwsQ0FBYztBQUFFUixxQkFBYSxFQUFFO0FBQWpCLE9BQWQsRUFIZ0IsQ0FHb0I7O0FBQ3ZDLEs7OzBOQUdpQixVQUFDVSxTQUFELEVBQWU7QUFDN0I7QUFDQSxVQUFNQyxrQkFBa0IsR0FBR0QsU0FBUyxDQUFDRSxTQUFWLENBQW9CUixHQUFwQixDQUF3QixVQUFDUyxPQUFEO0FBQUEsZUFBYUEsT0FBTyxDQUFDQyxLQUFyQjtBQUFBLE9BQXhCLENBQTNCO0FBQ0EsVUFBTUMsZUFBZSxHQUFHTCxTQUFTLENBQUNFLFNBQVYsQ0FBb0JSLEdBQXBCLENBQXdCLFVBQUNTLE9BQUQ7QUFBQSxlQUFhQSxPQUFPLENBQUNHLEVBQXJCO0FBQUEsT0FBeEIsQ0FBeEIsQ0FINkIsQ0FJN0I7O0FBQ0EsVUFBTUMsa0JBQWtCLEdBQUdQLFNBQVMsQ0FBQ0UsU0FBVixDQUFvQlIsR0FBcEIsQ0FBd0IsVUFBQ1MsT0FBRDtBQUFBLGVBQWFBLE9BQU8sQ0FBQ0ssUUFBUixDQUFpQmQsR0FBakIsQ0FBcUIsVUFBU0MsQ0FBVCxFQUFZO0FBQUUsaUJBQU9BLENBQUMsQ0FBQ2MsS0FBVDtBQUFpQixTQUFwRCxDQUFiO0FBQUEsT0FBeEIsQ0FBM0I7QUFDQSxVQUFNQyxnQkFBZ0IsR0FBR1YsU0FBUyxDQUFDRSxTQUFWLENBQW9CUixHQUFwQixDQUF3QixVQUFDUyxPQUFEO0FBQUEsZUFBYUEsT0FBTyxDQUFDUSxNQUFSLENBQWUsQ0FBZixFQUFrQkMsV0FBL0I7QUFBQSxPQUF4QixDQUF6QixDQU42QixDQU83Qjs7QUFDQSxZQUFLZCxRQUFMLENBQWM7QUFBQ1QseUJBQWlCLEVBQUU7QUFBcEIsT0FBZCxFQVI2QixDQVM3Qjs7O0FBQ0EsWUFBS1MsUUFBTCxDQUFjO0FBQUNYLFlBQUksRUFBRTtBQUFQLE9BQWQsRUFWNkIsQ0FXN0I7OztBQUNBLFdBQUssSUFBSTBCLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdOLGtCQUFrQixDQUFDUixNQUF2QyxFQUErQ2MsQ0FBQyxFQUFoRCxFQUFvRDtBQUNoRCxZQUFJTixrQkFBa0IsQ0FBQ00sQ0FBRCxDQUFsQixDQUFzQmQsTUFBdEIsSUFBZ0MsQ0FBcEMsRUFBc0M7QUFDbEMsY0FBSWUsT0FBTSxHQUFHLE1BQUtyQixLQUFMLENBQVdMLGVBQVgsQ0FBMkIyQixNQUEzQixDQUFrQztBQUFDekIseUJBQWEsRUFBRSxNQUFLRyxLQUFMLENBQVdILGFBQTNCO0FBQTBDZ0IsY0FBRSxFQUFFRCxlQUFlLENBQUNRLENBQUQsQ0FBN0Q7QUFBbUVHLGdCQUFJLEVBQUNmLGtCQUFrQixDQUFDWSxDQUFELENBQTFGO0FBQStGSSx3QkFBWSxFQUFDUCxnQkFBZ0IsQ0FBQ0csQ0FBRCxDQUE1SDtBQUFpSUosaUJBQUssRUFBQ0Ysa0JBQWtCLENBQUNNLENBQUQ7QUFBekosV0FBbEMsQ0FBYjs7QUFDQSxnQkFBS2YsUUFBTCxDQUFjO0FBQUVWLDJCQUFlLEVBQUUwQjtBQUFuQixXQUFkOztBQUNBLGdCQUFLaEIsUUFBTCxDQUFjO0FBQUVSLHlCQUFhLEVBQUUsTUFBS0csS0FBTCxDQUFXSCxhQUFYLEdBQTJCO0FBQTVDLFdBQWQ7QUFDSCxTQUpELE1BS0s7QUFDRCxlQUFLLElBQUk0QixDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHWCxrQkFBa0IsQ0FBQ1IsTUFBdkMsRUFBK0NtQixDQUFDLEVBQWhELEVBQW9EO0FBQ2hELGdCQUFJQyxTQUFTLEdBQUcsTUFBSzFCLEtBQUwsQ0FBV0wsZUFBWCxDQUEyQjJCLE1BQTNCLENBQWtDO0FBQUN6QiwyQkFBYSxFQUFFLE1BQUtHLEtBQUwsQ0FBV0gsYUFBM0I7QUFBMENnQixnQkFBRSxFQUFFRCxlQUFlLENBQUNRLENBQUQsQ0FBN0Q7QUFBbUVHLGtCQUFJLEVBQUNmLGtCQUFrQixDQUFDWSxDQUFELENBQTFGO0FBQStGSSwwQkFBWSxFQUFDUCxnQkFBZ0IsQ0FBQ0csQ0FBRCxDQUE1SDtBQUFpSUosbUJBQUssRUFBQ0Ysa0JBQWtCLENBQUNXLENBQUQ7QUFBekosYUFBbEMsQ0FBaEI7O0FBQ0Esa0JBQUtwQixRQUFMLENBQWM7QUFBRVYsNkJBQWUsRUFBRTBCO0FBQW5CLGFBQWQ7O0FBQ0Esa0JBQUtoQixRQUFMLENBQWM7QUFBRVIsMkJBQWEsRUFBRSxNQUFLRyxLQUFMLENBQVdILGFBQVgsR0FBMkI7QUFBNUMsYUFBZDtBQUNIO0FBQ0o7QUFDSjtBQUNKLEs7Ozs7Ozs7QUF0R3VGOzZCQUUvRTtBQUFBOztBQUNMO0FBQUE7QUFDQTtBQUNBLHVFQUFDLHFEQUFEO0FBQ0ksbUJBQVMsTUFEYjtBQUVJLGVBQUssRUFBQyxtQkFGVjtBQUdJLHVCQUFhLEVBQUU7QUFDWDhCLG1CQUFPLEVBQUUsZ0JBREU7QUFFWEMsb0JBQVEsRUFBRTtBQUFBLHFCQUFNLE1BQUksQ0FBQ3ZCLFFBQUwsQ0FBYztBQUFDWCxvQkFBSSxFQUFFO0FBQVAsZUFBZCxDQUFOO0FBQUE7QUFGQyxXQUhuQjtBQUFBLGtDQVFRLDhEQUFDLHdFQUFjO0FBQUU7QUFBakI7QUFDSSx3QkFBWSxFQUFDLFNBRGpCO0FBRUksZ0JBQUksRUFBRSxLQUFLTSxLQUFMLENBQVdOLElBRnJCO0FBR0ksb0JBQVEsRUFBRztBQUFBLHFCQUFNLE1BQUksQ0FBQ1csUUFBTCxDQUFjO0FBQUNYLG9CQUFJLEVBQUU7QUFBUCxlQUFkLENBQU47QUFBQSxhQUhmO0FBSUksdUJBQVcsRUFBRSxxQkFBQ2EsU0FBRDtBQUFBLHFCQUFlLE1BQUksQ0FBQ3NCLGVBQUwsQ0FBcUJ0QixTQUFyQixDQUFmO0FBQUE7QUFKakIsWUFSUixlQWNRO0FBQUEsc0JBQ0s7QUFDRCxpQkFBS1AsS0FBTCxDQUFXSixpQkFBWCxnQkFDSTtBQUFLLG1CQUFLLEVBQUU7QUFBQ2tDLDRCQUFZLEVBQUU7QUFBZixlQUFaO0FBQUEscUNBQ0ksOERBQUMsdURBQUQ7QUFBUSwyQkFBVyxNQUFuQjtBQUNBLHVCQUFPLEVBQUU7QUFBQSx5QkFBTSxNQUFJLENBQUNDLFdBQUwsRUFBTjtBQUFBLGlCQURUO0FBQUE7QUFBQTtBQURKLGNBREosR0FLRTtBQVBOLFlBZFIsZUF1QlEsOERBQUMscURBQUQ7QUFBQSxtQ0FDQSw4REFBQyw2REFBWTtBQUFDO0FBQWQ7QUFDSSwwQkFBWSxFQUFFO0FBQUNDLHdCQUFRLEVBQUUsU0FBWDtBQUFzQkMsc0JBQU0sRUFBRTtBQUE5QixlQURsQjtBQUVJLG1CQUFLLEVBQUcsS0FBS2pDLEtBQUwsQ0FBV0wsZUFGdkI7QUFHSSx3QkFBVSxFQUFFLG9CQUFDdUMsSUFBRCxFQUFVO0FBQUEsb0JBQ2ZyQyxhQURlLEdBQ2tDcUMsSUFEbEMsQ0FDZnJDLGFBRGU7QUFBQSxvQkFDQWdCLEVBREEsR0FDa0NxQixJQURsQyxDQUNBckIsRUFEQTtBQUFBLG9CQUNLVSxJQURMLEdBQ2tDVyxJQURsQyxDQUNLWCxJQURMO0FBQUEsb0JBQ1dDLFlBRFgsR0FDa0NVLElBRGxDLENBQ1dWLFlBRFg7QUFBQSxvQkFDeUJSLEtBRHpCLEdBQ2tDa0IsSUFEbEMsQ0FDeUJsQixLQUR6QixFQUN3Qzs7QUFDOUQsb0NBQ0ksK0RBQUMsNkRBQUQ7QUFDQSxvQkFBRSxFQUFFSCxFQURKO0FBRUEsdUJBQUssZUFBRyw4REFBQyx1REFBRDtBQUFRLDRCQUFRLE1BQWhCO0FBQWlCLHdCQUFJLEVBQUMsUUFBdEI7QUFBK0Isd0JBQUksRUFBRVUsSUFBckM7QUFBMkMsMEJBQU0sRUFBRUM7QUFBbkQsb0JBRlI7QUFHQSxvQ0FBa0IsNkJBQXNCRCxJQUF0QixDQUhsQjtBQUFBLDBDQUtBO0FBQUEsMkNBQ0ksOERBQUMsMERBQUQ7QUFBVywrQkFBUyxFQUFDLFFBQXJCO0FBQUEsZ0NBQStCQTtBQUEvQjtBQURKLG9CQUxBLGVBUUE7QUFBQSw4QkFBTVA7QUFBTixvQkFSQSxlQVNBO0FBQUsseUJBQUssRUFBRTtBQUFDbUIsOEJBQVEsRUFBQyxVQUFWO0FBQXNCQywyQkFBSyxFQUFDLE1BQTVCO0FBQW9DQywrQkFBUyxFQUFFO0FBQS9DLHFCQUFaO0FBQUEsMkNBQ0ksOERBQUMsdURBQUQ7QUFBUSxpQ0FBVyxNQUFuQjtBQUFvQixtQ0FBYSxFQUFFeEMsYUFBbkM7QUFDQSw2QkFBTyxFQUFFLGlCQUFDQyxHQUFEO0FBQUEsK0JBQVMsTUFBSSxDQUFDd0MsWUFBTCxDQUFrQkosSUFBSSxDQUFDckMsYUFBdkIsQ0FBVDtBQUFBLHVCQURUO0FBQUE7QUFBQTtBQURKLG9CQVRBO0FBQUEsa0JBREo7QUFnQkM7QUFyQkw7QUFEQSxZQXZCUjtBQUFBO0FBRkE7QUFvREgsSyxDQUVEOzs7OztFQTFEZ0IwQyxnRDs7QUEyR0g5QyxvRUFBZiIsImZpbGUiOiJzdGF0aWMvd2VicGFjay9wYWdlcy9pbmRleC43NWYzNThiNzA5YmE5NDcyZTNhNS5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLy9JbXBvcnQgUG9sYXJpcyBlbGVtZW50cywgUmVzb3VyY2UgUGlja2VyIGFuZCBSZWFjdFxyXG5pbXBvcnQgeyBQYWdlLCBDYXJkLCBSZXNvdXJjZUxpc3QsIFJlc291cmNlSXRlbSwgQXZhdGFyLCBUZXh0U3R5bGUsIEJ1dHRvbiB9IGZyb20gXCJAc2hvcGlmeS9wb2xhcmlzXCI7XHJcbmltcG9ydCB7IFJlc291cmNlUGlja2VyIH0gZnJvbSBcIkBzaG9waWZ5L2FwcC1icmlkZ2UtcmVhY3RcIjtcclxuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcclxuXHJcbmNsYXNzIEluZGV4IGV4dGVuZHMgQ29tcG9uZW50IHtcclxuICAgIHN0YXRlID0geyBvcGVuOiBmYWxzZSwgcmVzb3VyY2VMaXN0QXJyOiBbXSwgZGVsZXRlQnV0dG9uU2hvd246IGZhbHNlLCByZXNvdXJjZUluZGV4OiAwIH0vL3NldCBzdGF0ZVxyXG5cclxuICAgIHJlbmRlcigpIHsgXHJcbiAgICAgICAgcmV0dXJuICggXHJcbiAgICAgICAgLy9QYWdlIHN0eWxpbmcgdXNpbmcgUG9sYXJpcyBQYWdlIGVsZW1lbnQsIFRleHQgYW4gQnV0dG9uXHJcbiAgICAgICAgPFBhZ2UgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgZnVsbFdpZHRoXHJcbiAgICAgICAgICAgIHRpdGxlPVwiUHJvZHVjdCBTZWxlY3Rpb25cIlxyXG4gICAgICAgICAgICBwcmltYXJ5QWN0aW9uPXt7XHJcbiAgICAgICAgICAgICAgICBjb250ZW50OiAnU2VsZWN0IHByb2R1Y3QnLFxyXG4gICAgICAgICAgICAgICAgb25BY3Rpb246ICgpID0+IHRoaXMuc2V0U3RhdGUoe29wZW46IHRydWV9KVxyXG4gICAgICAgICAgICB9fVxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICA8UmVzb3VyY2VQaWNrZXIgIC8qUmVzb3VyY2UgUGlja2VyLCBjaGFuZ2Ugc3RhdGUgb24gY2xvdGhvbmcgYW5kIGhhbmRsaW5nIGlmIGl0ZW1zIHNlbGVjdGVkKi9cclxuICAgICAgICAgICAgICAgICAgICByZXNvdXJjZVR5cGU9XCJQcm9kdWN0XCJcclxuICAgICAgICAgICAgICAgICAgICBvcGVuPXt0aGlzLnN0YXRlLm9wZW59XHJcbiAgICAgICAgICAgICAgICAgICAgb25DYW5jZWw9eyAoKSA9PiB0aGlzLnNldFN0YXRlKHtvcGVuOiBmYWxzZX0pfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uU2VsZWN0aW9uPXsocmVzb3VyY2VzKSA9PiB0aGlzLmhhbmRsZVNlbGVjdGlvbihyZXNvdXJjZXMpfSBcclxuICAgICAgICAgICAgICAgIC8+IFxyXG4gICAgICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgICAgICB7Ly8gRGlzcGxheSBDbGVhcmluZyBCdXR0b24gaWYgYW55IGl0ZW0gaXMgc2VsZWN0ZWRcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLmRlbGV0ZUJ1dHRvblNob3duID8gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgc3R5bGU9e3ttYXJnaW5Cb3R0b206ICcxNXB4J319PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEJ1dHRvbiBkZXN0cnVjdGl2ZSBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHRoaXMuaGFuZGxlQ2xlYXIoKX0+Q2xlYXIgYWxsPC9CdXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PiBcclxuICAgICAgICAgICAgICAgICAgICA6IG51bGx9IFxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8Q2FyZD5cclxuICAgICAgICAgICAgICAgIDxSZXNvdXJjZUxpc3QgLypBZGRpbmcgUmVzb3VyY2UgTGlzdCB3aWNoIGNvbnNpc3RzIG9mIFJlc291cmNlSXRlbXMgKi9cclxuICAgICAgICAgICAgICAgICAgICByZXNvdXJjZU5hbWU9e3tzaW5ndWxhcjogJ3Byb2R1Y3QnLCBwbHVyYWw6ICdwcm9kdWN0J319XHJcbiAgICAgICAgICAgICAgICAgICAgaXRlbXM9eyB0aGlzLnN0YXRlLnJlc291cmNlTGlzdEFyciB9XHJcbiAgICAgICAgICAgICAgICAgICAgcmVuZGVySXRlbT17KGl0ZW0pID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCB7cmVzb3VyY2VJbmRleCwgaWQsICBuYW1lLCBhdmF0YXJTb3VyY2UsIHByaWNlfSA9IGl0ZW07IC8vU2V0dGluZyBJdGVtXHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgICAgICAgICAgPFJlc291cmNlSXRlbVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZD17aWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1lZGlhPSB7PEF2YXRhciBjdXN0b21lciBzaXplPVwibWVkaXVtXCIgbmFtZT17bmFtZX0gc291cmNlPXthdmF0YXJTb3VyY2V9IC8+fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhY2Nlc3NpYmlsaXR5TGFiZWw9e2BWaWV3IGRldGFpbHMgZm9yICR7bmFtZX1gfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxoMj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxUZXh0U3R5bGUgdmFyaWF0aW9uPVwic3Ryb25nXCI+e25hbWV9PC9UZXh0U3R5bGU+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvaDI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXY+e3ByaWNlfTwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IHN0eWxlPXt7cG9zaXRpb246J2Fic29sdXRlJywgcmlnaHQ6JzIwcHgnLCBtYXJnaW5Ub3A6ICctNDBweCd9fT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxCdXR0b24gZGVzdHJ1Y3RpdmUgcmVzb3VyY2VJbmRleD17cmVzb3VyY2VJbmRleH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eyhpbmQpID0+IHRoaXMuaGFuZGxlRGVsZXRlKGl0ZW0ucmVzb3VyY2VJbmRleCl9PkRlbGV0ZTwvQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9SZXNvdXJjZUl0ZW0+XHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgICAgICB9fVxyXG4gICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgIDwvQ2FyZD5cclxuICAgICAgICA8L1BhZ2U+IFxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLy9IYW5kbGluZyBEZWxldGlvbiBvZiBlbGVtZW50IGZyb20gUmVzb3VyY2UgTGlzdFxyXG4gICAgaGFuZGxlRGVsZXRlID0gKGluZCkgPT4ge1xyXG4gICAgICAgIHZhciBwb3MgPSB0aGlzLnN0YXRlLnJlc291cmNlTGlzdEFyci5tYXAoZnVuY3Rpb24oZSkgeyByZXR1cm4gZS5yZXNvdXJjZUluZGV4OyB9KS5pbmRleE9mKGluZCk7IC8vRGV0ZWN0aW5nIHBvc2l0aW9uIG9mIGl0ZW0gYnkgaXRgcyAncmVzb3VyY2VJbmRleCcgdmFsdWVcclxuICAgICAgICB0aGlzLnN0YXRlLnJlc291cmNlTGlzdEFyci5zcGxpY2UocG9zLCAxKTsgLy9EZWxldGUgaXRlbSBmcm9tIHByb2R1Y3RzIGFycmF5XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHJlc291cmNlTGlzdEFycjogIHRoaXMuc3RhdGUucmVzb3VyY2VMaXN0QXJyIH0pOyAvL1VwZGF0aW5nIHN0YXRlXHJcbiAgICAgICAgLy9JbiBjYXNlIGlmIGFsbCBvYmplY3RzIHJlbW92ZWQsIGhpZGUgJ0NsZWFyIEFsbCcgYnV0dG9uXHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUucmVzb3VyY2VMaXN0QXJyLmxlbmd0aCA9PSAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2RlbGV0ZUJ1dHRvblNob3duOiBmYWxzZX0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvL0hhbmRsaW5nICdDbGVhciBBbGwnIGJ1dHRvbiBhY3Rpb25cclxuICAgIGhhbmRsZUNsZWFyID0gKCkgPT4ge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyByZXNvdXJjZUxpc3RBcnI6IFtdIH0pOyAvL1JlbW92ZSBhbGwgaXRlbXMgZnJvbSBwcm9kdWN0YHMgYXJyYXlcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtkZWxldGVCdXR0b25TaG93bjogZmFsc2V9KTsvL0hpZGUgJ0NsZWFyIEFsbCcgYnV0dG9uXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHJlc291cmNlSW5kZXg6IDAgfSk7Ly9TZXQgJ1Jlc291cmNlSW5kZXgnIGNvdW50ZXIgdG8gMFxyXG4gICAgfVxyXG5cclxuICAgIC8vSGFuZGxpbmcgaXRlbXMgc2VsZWN0aW9uIGZyb20gcmVzb3VyY2UgcGlja2VyXHJcbiAgICBoYW5kbGVTZWxlY3Rpb24gPSAocmVzb3VyY2VzKSA9PiB7XHJcbiAgICAgICAgLy9tYXBwaW5nIG5ldyBhcnJheXMgd2l0aCBuZWVkZWQgcHJvZHVjdHMgdmFsdWVzXHJcbiAgICAgICAgY29uc3QgdGl0bGVGcm9tUmVzb3VyY2VzID0gcmVzb3VyY2VzLnNlbGVjdGlvbi5tYXAoKHByb2R1Y3QpID0+IHByb2R1Y3QudGl0bGUpO1xyXG4gICAgICAgIGNvbnN0IGlkRnJvbVJlc291cmNlcyA9IHJlc291cmNlcy5zZWxlY3Rpb24ubWFwKChwcm9kdWN0KSA9PiBwcm9kdWN0LmlkKTtcclxuICAgICAgICAvL2NvbnN0IHByaWNlRnJvbVJlc291cmNlcyA9IHJlc291cmNlcy5zZWxlY3Rpb24ubWFwKChwcm9kdWN0KSA9PiBwcm9kdWN0LnZhcmlhbnRzWzBdLnByaWNlKTtcclxuICAgICAgICBjb25zdCBwcmljZUZyb21SZXNvdXJjZXMgPSByZXNvdXJjZXMuc2VsZWN0aW9uLm1hcCgocHJvZHVjdCkgPT4gcHJvZHVjdC52YXJpYW50cy5tYXAoZnVuY3Rpb24oZSkgeyByZXR1cm4gZS5wcmljZTsgfSkpO1xyXG4gICAgICAgIGNvbnN0IGltZ0Zyb21SZXNvdXJjZXMgPSByZXNvdXJjZXMuc2VsZWN0aW9uLm1hcCgocHJvZHVjdCkgPT4gcHJvZHVjdC5pbWFnZXNbMF0ub3JpZ2luYWxTcmMpO1xyXG4gICAgICAgIC8vU2hvdyAnQ2xlYXIgQWxsJyBidXR0b25cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtkZWxldGVCdXR0b25TaG93bjogdHJ1ZX0pO1xyXG4gICAgICAgIC8vQ2xvc2UgUmVzb3VyY2UgUGlja2VyXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7b3BlbjogZmFsc2V9KTtcclxuICAgICAgICAvL0FkZGluZyBwcm9kdWN0cyB0byBwcm9kdWN0YHMgYXJyYXlcclxuICAgICAgICBmb3IgKGxldCB4ID0gMDsgeCA8IHByaWNlRnJvbVJlc291cmNlcy5sZW5ndGg7IHgrKykge1xyXG4gICAgICAgICAgICBpZiAocHJpY2VGcm9tUmVzb3VyY2VzW3hdLmxlbmd0aCA9PSAxKXtcclxuICAgICAgICAgICAgICAgIGxldCBqb2luZWQgPSB0aGlzLnN0YXRlLnJlc291cmNlTGlzdEFyci5jb25jYXQoe3Jlc291cmNlSW5kZXg6IHRoaXMuc3RhdGUucmVzb3VyY2VJbmRleCwgaWQ6IGlkRnJvbVJlc291cmNlc1t4XSwgIG5hbWU6dGl0bGVGcm9tUmVzb3VyY2VzW3hdLCBhdmF0YXJTb3VyY2U6aW1nRnJvbVJlc291cmNlc1t4XSwgcHJpY2U6cHJpY2VGcm9tUmVzb3VyY2VzW3hdfSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VMaXN0QXJyOiBqb2luZWQgfSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VJbmRleDogdGhpcy5zdGF0ZS5yZXNvdXJjZUluZGV4ICsgMSB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGZvciAobGV0IHkgPSAwOyB5IDwgcHJpY2VGcm9tUmVzb3VyY2VzLmxlbmd0aDsgeSsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IG5ld0pvaW5lZCA9IHRoaXMuc3RhdGUucmVzb3VyY2VMaXN0QXJyLmNvbmNhdCh7cmVzb3VyY2VJbmRleDogdGhpcy5zdGF0ZS5yZXNvdXJjZUluZGV4LCBpZDogaWRGcm9tUmVzb3VyY2VzW3hdLCAgbmFtZTp0aXRsZUZyb21SZXNvdXJjZXNbeF0sIGF2YXRhclNvdXJjZTppbWdGcm9tUmVzb3VyY2VzW3hdLCBwcmljZTpwcmljZUZyb21SZXNvdXJjZXNbeV19KTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VMaXN0QXJyOiBqb2luZWQgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHJlc291cmNlSW5kZXg6IHRoaXMuc3RhdGUucmVzb3VyY2VJbmRleCArIDEgfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59XHJcbiAgXHJcbiAgZXhwb3J0IGRlZmF1bHQgSW5kZXg7Il0sInNvdXJjZVJvb3QiOiIifQ==