webpackHotUpdate_N_E("pages/index",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/esm/createClass */ "./node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inherits */ "./node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _shopify_polaris__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @shopify/polaris */ "./node_modules/@shopify/polaris/dist/esm/index.js");
/* harmony import */ var _shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @shopify/app-bridge-react */ "./node_modules/@shopify/app-bridge-react/index.js");
/* harmony import */ var _shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_10__);










function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return Object(_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

//Import Polaris elements, Resource Picker and React




var Index = /*#__PURE__*/function (_Component) {
  Object(_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__["default"])(Index, _Component);

  var _super = _createSuper(Index);

  function Index() {
    var _this;

    Object(_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, Index);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "state", {
      open: false,
      resourceListArr: [],
      deleteButtonShown: false,
      resourceIndex: 0
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleDelete", function (ind) {
      var pos = _this.state.resourceListArr.map(function (e) {
        return e.resourceIndex;
      }).indexOf(ind); //Detecting position of item by it`s 'resourceIndex' value


      _this.state.resourceListArr.splice(pos, 1); //Delete item from products array


      _this.setState({
        resourceListArr: _this.state.resourceListArr
      }); //Updating state
      //In case if all objects removed, hide 'Clear All' button


      if (_this.state.resourceListArr.length == 0) {
        _this.setState({
          deleteButtonShown: false
        });
      }
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleClear", function () {
      _this.setState({
        resourceListArr: []
      }); //Remove all items from product`s array


      _this.setState({
        deleteButtonShown: false
      }); //Hide 'Clear All' button


      _this.setState({
        resourceIndex: 0
      }); //Set 'ResourceIndex' counter to 0

    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleSelection", function (resources) {
      //mapping new arrays with needed products values
      var titleFromResources = resources.selection.map(function (product) {
        return product.title;
      });
      var idFromResources = resources.selection.map(function (product) {
        return product.id;
      });
      var priceFromResources = resources.selection.map(function (product) {
        for (var x = 0; x < product.variants.length; x++) {
          product.variants[x].price;
        }
      });
      var imgFromResources = resources.selection.map(function (product) {
        return product.images[0].originalSrc;
      }); //Show 'Clear All' button

      _this.setState({
        deleteButtonShown: true
      }); //Close Resource Picker


      _this.setState({
        open: false
      }); //Adding products to product`s array


      for (var x = 0; x < priceFromResources.length; x++) {
        var joined = _this.state.resourceListArr.concat({
          resourceIndex: _this.state.resourceIndex,
          id: idFromResources[x],
          name: titleFromResources[x],
          avatarSource: imgFromResources[x],
          price: priceFromResources[x]
        });

        console.log(priceFromResources);

        _this.setState({
          resourceListArr: joined
        });

        _this.setState({
          resourceIndex: _this.state.resourceIndex + 1
        });
      }
    });

    return _this;
  }

  Object(_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(Index, [{
    key: "render",
    //set state
    value: function render() {
      var _this2 = this;

      return (
        /*#__PURE__*/
        //Page styling using Polaris Page element, Text an Button
        Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxs"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Page"], {
          fullWidth: true,
          title: "Product Selection",
          primaryAction: {
            content: 'Select product',
            onAction: function onAction() {
              return _this2.setState({
                open: true
              });
            }
          },
          children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9__["ResourcePicker"]
          /*Resource Picker, change state on clothong and handling if items selected*/
          , {
            resourceType: "Product",
            open: this.state.open,
            onCancel: function onCancel() {
              return _this2.setState({
                open: false
              });
            },
            onSelection: function onSelection(resources) {
              return _this2.handleSelection(resources);
            }
          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
            children: // Display Clearing Button if any item is selected
            this.state.deleteButtonShown ? /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
              style: {
                marginBottom: '15px'
              },
              children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Button"], {
                destructive: true,
                onClick: function onClick() {
                  return _this2.handleClear();
                },
                children: "Clear all"
              })
            }) : null
          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Card"], {
            children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["ResourceList"]
            /*Adding Resource List wich consists of ResourceItems */
            , {
              resourceName: {
                singular: 'product',
                plural: 'product'
              },
              items: this.state.resourceListArr,
              renderItem: function renderItem(item) {
                var resourceIndex = item.resourceIndex,
                    id = item.id,
                    name = item.name,
                    avatarSource = item.avatarSource,
                    price = item.price; //Setting Item

                return /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxs"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["ResourceItem"], {
                  id: id,
                  media: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Avatar"], {
                    customer: true,
                    size: "medium",
                    name: name,
                    source: avatarSource
                  }),
                  accessibilityLabel: "View details for ".concat(name),
                  children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("h2", {
                    children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["TextStyle"], {
                      variation: "strong",
                      children: name
                    })
                  }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
                    children: price
                  }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
                    style: {
                      position: 'absolute',
                      right: '20px',
                      marginTop: '-40px'
                    },
                    children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Button"], {
                      destructive: true,
                      resourceIndex: resourceIndex,
                      onClick: function onClick(ind) {
                        return _this2.handleDelete(item.resourceIndex);
                      },
                      children: "Delete"
                    })
                  })]
                });
              }
            })
          })]
        })
      );
    } //Handling Deletion of element from Resource List

  }]);

  return Index;
}(react__WEBPACK_IMPORTED_MODULE_10__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Index);

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvaW5kZXguanMiXSwibmFtZXMiOlsiSW5kZXgiLCJvcGVuIiwicmVzb3VyY2VMaXN0QXJyIiwiZGVsZXRlQnV0dG9uU2hvd24iLCJyZXNvdXJjZUluZGV4IiwiaW5kIiwicG9zIiwic3RhdGUiLCJtYXAiLCJlIiwiaW5kZXhPZiIsInNwbGljZSIsInNldFN0YXRlIiwibGVuZ3RoIiwicmVzb3VyY2VzIiwidGl0bGVGcm9tUmVzb3VyY2VzIiwic2VsZWN0aW9uIiwicHJvZHVjdCIsInRpdGxlIiwiaWRGcm9tUmVzb3VyY2VzIiwiaWQiLCJwcmljZUZyb21SZXNvdXJjZXMiLCJ4IiwidmFyaWFudHMiLCJwcmljZSIsImltZ0Zyb21SZXNvdXJjZXMiLCJpbWFnZXMiLCJvcmlnaW5hbFNyYyIsImpvaW5lZCIsImNvbmNhdCIsIm5hbWUiLCJhdmF0YXJTb3VyY2UiLCJjb25zb2xlIiwibG9nIiwiY29udGVudCIsIm9uQWN0aW9uIiwiaGFuZGxlU2VsZWN0aW9uIiwibWFyZ2luQm90dG9tIiwiaGFuZGxlQ2xlYXIiLCJzaW5ndWxhciIsInBsdXJhbCIsIml0ZW0iLCJwb3NpdGlvbiIsInJpZ2h0IiwibWFyZ2luVG9wIiwiaGFuZGxlRGVsZXRlIiwiQ29tcG9uZW50Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTs7SUFFTUEsSzs7Ozs7Ozs7Ozs7Ozs7OztnTkFDTTtBQUFFQyxVQUFJLEVBQUUsS0FBUjtBQUFlQyxxQkFBZSxFQUFFLEVBQWhDO0FBQW9DQyx1QkFBaUIsRUFBRSxLQUF2RDtBQUE4REMsbUJBQWEsRUFBRTtBQUE3RSxLOzt1TkEwRE8sVUFBQ0MsR0FBRCxFQUFTO0FBQ3BCLFVBQUlDLEdBQUcsR0FBRyxNQUFLQyxLQUFMLENBQVdMLGVBQVgsQ0FBMkJNLEdBQTNCLENBQStCLFVBQVNDLENBQVQsRUFBWTtBQUFFLGVBQU9BLENBQUMsQ0FBQ0wsYUFBVDtBQUF5QixPQUF0RSxFQUF3RU0sT0FBeEUsQ0FBZ0ZMLEdBQWhGLENBQVYsQ0FEb0IsQ0FDNEU7OztBQUNoRyxZQUFLRSxLQUFMLENBQVdMLGVBQVgsQ0FBMkJTLE1BQTNCLENBQWtDTCxHQUFsQyxFQUF1QyxDQUF2QyxFQUZvQixDQUV1Qjs7O0FBQzNDLFlBQUtNLFFBQUwsQ0FBYztBQUFFVix1QkFBZSxFQUFHLE1BQUtLLEtBQUwsQ0FBV0w7QUFBL0IsT0FBZCxFQUhvQixDQUc2QztBQUNqRTs7O0FBQ0EsVUFBSSxNQUFLSyxLQUFMLENBQVdMLGVBQVgsQ0FBMkJXLE1BQTNCLElBQXFDLENBQXpDLEVBQTRDO0FBQ3hDLGNBQUtELFFBQUwsQ0FBYztBQUFDVCwyQkFBaUIsRUFBRTtBQUFwQixTQUFkO0FBQ0g7QUFDSixLOztzTkFHYSxZQUFNO0FBQ2hCLFlBQUtTLFFBQUwsQ0FBYztBQUFFVix1QkFBZSxFQUFFO0FBQW5CLE9BQWQsRUFEZ0IsQ0FDd0I7OztBQUN4QyxZQUFLVSxRQUFMLENBQWM7QUFBQ1QseUJBQWlCLEVBQUU7QUFBcEIsT0FBZCxFQUZnQixDQUUwQjs7O0FBQzFDLFlBQUtTLFFBQUwsQ0FBYztBQUFFUixxQkFBYSxFQUFFO0FBQWpCLE9BQWQsRUFIZ0IsQ0FHb0I7O0FBQ3ZDLEs7OzBOQUdpQixVQUFDVSxTQUFELEVBQWU7QUFDN0I7QUFDQSxVQUFNQyxrQkFBa0IsR0FBR0QsU0FBUyxDQUFDRSxTQUFWLENBQW9CUixHQUFwQixDQUF3QixVQUFDUyxPQUFEO0FBQUEsZUFBYUEsT0FBTyxDQUFDQyxLQUFyQjtBQUFBLE9BQXhCLENBQTNCO0FBQ0EsVUFBTUMsZUFBZSxHQUFHTCxTQUFTLENBQUNFLFNBQVYsQ0FBb0JSLEdBQXBCLENBQXdCLFVBQUNTLE9BQUQ7QUFBQSxlQUFhQSxPQUFPLENBQUNHLEVBQXJCO0FBQUEsT0FBeEIsQ0FBeEI7QUFDQSxVQUFNQyxrQkFBa0IsR0FBR1AsU0FBUyxDQUFDRSxTQUFWLENBQW9CUixHQUFwQixDQUF3QixVQUFDUyxPQUFELEVBQ25EO0FBQUMsYUFBSyxJQUFJSyxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHTCxPQUFPLENBQUNNLFFBQVIsQ0FBaUJWLE1BQXJDLEVBQTZDUyxDQUFDLEVBQTlDLEVBQWtEO0FBQy9DTCxpQkFBTyxDQUFDTSxRQUFSLENBQWlCRCxDQUFqQixFQUFvQkUsS0FBcEI7QUFDSDtBQUFDLE9BSHlCLENBQTNCO0FBS0EsVUFBTUMsZ0JBQWdCLEdBQUdYLFNBQVMsQ0FBQ0UsU0FBVixDQUFvQlIsR0FBcEIsQ0FBd0IsVUFBQ1MsT0FBRDtBQUFBLGVBQWFBLE9BQU8sQ0FBQ1MsTUFBUixDQUFlLENBQWYsRUFBa0JDLFdBQS9CO0FBQUEsT0FBeEIsQ0FBekIsQ0FUNkIsQ0FVN0I7O0FBQ0EsWUFBS2YsUUFBTCxDQUFjO0FBQUNULHlCQUFpQixFQUFFO0FBQXBCLE9BQWQsRUFYNkIsQ0FZN0I7OztBQUNBLFlBQUtTLFFBQUwsQ0FBYztBQUFDWCxZQUFJLEVBQUU7QUFBUCxPQUFkLEVBYjZCLENBYzdCOzs7QUFDQSxXQUFLLElBQUlxQixDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHRCxrQkFBa0IsQ0FBQ1IsTUFBdkMsRUFBK0NTLENBQUMsRUFBaEQsRUFBb0Q7QUFDNUMsWUFBSU0sTUFBTSxHQUFHLE1BQUtyQixLQUFMLENBQVdMLGVBQVgsQ0FBMkIyQixNQUEzQixDQUFrQztBQUFDekIsdUJBQWEsRUFBRSxNQUFLRyxLQUFMLENBQVdILGFBQTNCO0FBQTBDZ0IsWUFBRSxFQUFFRCxlQUFlLENBQUNHLENBQUQsQ0FBN0Q7QUFBbUVRLGNBQUksRUFBQ2Ysa0JBQWtCLENBQUNPLENBQUQsQ0FBMUY7QUFBK0ZTLHNCQUFZLEVBQUNOLGdCQUFnQixDQUFDSCxDQUFELENBQTVIO0FBQWlJRSxlQUFLLEVBQUNILGtCQUFrQixDQUFDQyxDQUFEO0FBQXpKLFNBQWxDLENBQWI7O0FBQ0FVLGVBQU8sQ0FBQ0MsR0FBUixDQUFZWixrQkFBWjs7QUFDQSxjQUFLVCxRQUFMLENBQWM7QUFBRVYseUJBQWUsRUFBRTBCO0FBQW5CLFNBQWQ7O0FBQ0EsY0FBS2hCLFFBQUwsQ0FBYztBQUFFUix1QkFBYSxFQUFFLE1BQUtHLEtBQUwsQ0FBV0gsYUFBWCxHQUEyQjtBQUE1QyxTQUFkO0FBRVA7QUFDSixLOzs7Ozs7O0FBbEd1Rjs2QkFFL0U7QUFBQTs7QUFDTDtBQUFBO0FBQ0E7QUFDQSx1RUFBQyxxREFBRDtBQUNJLG1CQUFTLE1BRGI7QUFFSSxlQUFLLEVBQUMsbUJBRlY7QUFHSSx1QkFBYSxFQUFFO0FBQ1g4QixtQkFBTyxFQUFFLGdCQURFO0FBRVhDLG9CQUFRLEVBQUU7QUFBQSxxQkFBTSxNQUFJLENBQUN2QixRQUFMLENBQWM7QUFBQ1gsb0JBQUksRUFBRTtBQUFQLGVBQWQsQ0FBTjtBQUFBO0FBRkMsV0FIbkI7QUFBQSxrQ0FRUSw4REFBQyx3RUFBYztBQUFFO0FBQWpCO0FBQ0ksd0JBQVksRUFBQyxTQURqQjtBQUVJLGdCQUFJLEVBQUUsS0FBS00sS0FBTCxDQUFXTixJQUZyQjtBQUdJLG9CQUFRLEVBQUc7QUFBQSxxQkFBTSxNQUFJLENBQUNXLFFBQUwsQ0FBYztBQUFDWCxvQkFBSSxFQUFFO0FBQVAsZUFBZCxDQUFOO0FBQUEsYUFIZjtBQUlJLHVCQUFXLEVBQUUscUJBQUNhLFNBQUQ7QUFBQSxxQkFBZSxNQUFJLENBQUNzQixlQUFMLENBQXFCdEIsU0FBckIsQ0FBZjtBQUFBO0FBSmpCLFlBUlIsZUFjUTtBQUFBLHNCQUNLO0FBQ0QsaUJBQUtQLEtBQUwsQ0FBV0osaUJBQVgsZ0JBQ0k7QUFBSyxtQkFBSyxFQUFFO0FBQUNrQyw0QkFBWSxFQUFFO0FBQWYsZUFBWjtBQUFBLHFDQUNJLDhEQUFDLHVEQUFEO0FBQVEsMkJBQVcsTUFBbkI7QUFDQSx1QkFBTyxFQUFFO0FBQUEseUJBQU0sTUFBSSxDQUFDQyxXQUFMLEVBQU47QUFBQSxpQkFEVDtBQUFBO0FBQUE7QUFESixjQURKLEdBS0U7QUFQTixZQWRSLGVBdUJRLDhEQUFDLHFEQUFEO0FBQUEsbUNBQ0EsOERBQUMsNkRBQVk7QUFBQztBQUFkO0FBQ0ksMEJBQVksRUFBRTtBQUFDQyx3QkFBUSxFQUFFLFNBQVg7QUFBc0JDLHNCQUFNLEVBQUU7QUFBOUIsZUFEbEI7QUFFSSxtQkFBSyxFQUFHLEtBQUtqQyxLQUFMLENBQVdMLGVBRnZCO0FBR0ksd0JBQVUsRUFBRSxvQkFBQ3VDLElBQUQsRUFBVTtBQUFBLG9CQUNmckMsYUFEZSxHQUNrQ3FDLElBRGxDLENBQ2ZyQyxhQURlO0FBQUEsb0JBQ0FnQixFQURBLEdBQ2tDcUIsSUFEbEMsQ0FDQXJCLEVBREE7QUFBQSxvQkFDS1UsSUFETCxHQUNrQ1csSUFEbEMsQ0FDS1gsSUFETDtBQUFBLG9CQUNXQyxZQURYLEdBQ2tDVSxJQURsQyxDQUNXVixZQURYO0FBQUEsb0JBQ3lCUCxLQUR6QixHQUNrQ2lCLElBRGxDLENBQ3lCakIsS0FEekIsRUFDd0M7O0FBQzlELG9DQUNJLCtEQUFDLDZEQUFEO0FBQ0Esb0JBQUUsRUFBRUosRUFESjtBQUVBLHVCQUFLLGVBQUcsOERBQUMsdURBQUQ7QUFBUSw0QkFBUSxNQUFoQjtBQUFpQix3QkFBSSxFQUFDLFFBQXRCO0FBQStCLHdCQUFJLEVBQUVVLElBQXJDO0FBQTJDLDBCQUFNLEVBQUVDO0FBQW5ELG9CQUZSO0FBR0Esb0NBQWtCLDZCQUFzQkQsSUFBdEIsQ0FIbEI7QUFBQSwwQ0FLQTtBQUFBLDJDQUNJLDhEQUFDLDBEQUFEO0FBQVcsK0JBQVMsRUFBQyxRQUFyQjtBQUFBLGdDQUErQkE7QUFBL0I7QUFESixvQkFMQSxlQVFBO0FBQUEsOEJBQU1OO0FBQU4sb0JBUkEsZUFTQTtBQUFLLHlCQUFLLEVBQUU7QUFBQ2tCLDhCQUFRLEVBQUMsVUFBVjtBQUFzQkMsMkJBQUssRUFBQyxNQUE1QjtBQUFvQ0MsK0JBQVMsRUFBRTtBQUEvQyxxQkFBWjtBQUFBLDJDQUNJLDhEQUFDLHVEQUFEO0FBQVEsaUNBQVcsTUFBbkI7QUFBb0IsbUNBQWEsRUFBRXhDLGFBQW5DO0FBQ0EsNkJBQU8sRUFBRSxpQkFBQ0MsR0FBRDtBQUFBLCtCQUFTLE1BQUksQ0FBQ3dDLFlBQUwsQ0FBa0JKLElBQUksQ0FBQ3JDLGFBQXZCLENBQVQ7QUFBQSx1QkFEVDtBQUFBO0FBQUE7QUFESixvQkFUQTtBQUFBLGtCQURKO0FBZ0JDO0FBckJMO0FBREEsWUF2QlI7QUFBQTtBQUZBO0FBb0RILEssQ0FFRDs7Ozs7RUExRGdCMEMsZ0Q7O0FBdUdIOUMsb0VBQWYiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvaW5kZXguNWVkZGEwYjAxMDk4OGJiMTg4YTcuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8vSW1wb3J0IFBvbGFyaXMgZWxlbWVudHMsIFJlc291cmNlIFBpY2tlciBhbmQgUmVhY3RcclxuaW1wb3J0IHsgUGFnZSwgQ2FyZCwgUmVzb3VyY2VMaXN0LCBSZXNvdXJjZUl0ZW0sIEF2YXRhciwgVGV4dFN0eWxlLCBCdXR0b24gfSBmcm9tIFwiQHNob3BpZnkvcG9sYXJpc1wiO1xyXG5pbXBvcnQgeyBSZXNvdXJjZVBpY2tlciB9IGZyb20gXCJAc2hvcGlmeS9hcHAtYnJpZGdlLXJlYWN0XCI7XHJcbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XHJcblxyXG5jbGFzcyBJbmRleCBleHRlbmRzIENvbXBvbmVudCB7XHJcbiAgICBzdGF0ZSA9IHsgb3BlbjogZmFsc2UsIHJlc291cmNlTGlzdEFycjogW10sIGRlbGV0ZUJ1dHRvblNob3duOiBmYWxzZSwgcmVzb3VyY2VJbmRleDogMCB9Ly9zZXQgc3RhdGVcclxuXHJcbiAgICByZW5kZXIoKSB7IFxyXG4gICAgICAgIHJldHVybiAoIFxyXG4gICAgICAgIC8vUGFnZSBzdHlsaW5nIHVzaW5nIFBvbGFyaXMgUGFnZSBlbGVtZW50LCBUZXh0IGFuIEJ1dHRvblxyXG4gICAgICAgIDxQYWdlICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGZ1bGxXaWR0aFxyXG4gICAgICAgICAgICB0aXRsZT1cIlByb2R1Y3QgU2VsZWN0aW9uXCJcclxuICAgICAgICAgICAgcHJpbWFyeUFjdGlvbj17e1xyXG4gICAgICAgICAgICAgICAgY29udGVudDogJ1NlbGVjdCBwcm9kdWN0JyxcclxuICAgICAgICAgICAgICAgIG9uQWN0aW9uOiAoKSA9PiB0aGlzLnNldFN0YXRlKHtvcGVuOiB0cnVlfSlcclxuICAgICAgICAgICAgfX1cclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgPFJlc291cmNlUGlja2VyICAvKlJlc291cmNlIFBpY2tlciwgY2hhbmdlIHN0YXRlIG9uIGNsb3Rob25nIGFuZCBoYW5kbGluZyBpZiBpdGVtcyBzZWxlY3RlZCovXHJcbiAgICAgICAgICAgICAgICAgICAgcmVzb3VyY2VUeXBlPVwiUHJvZHVjdFwiXHJcbiAgICAgICAgICAgICAgICAgICAgb3Blbj17dGhpcy5zdGF0ZS5vcGVufVxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2FuY2VsPXsgKCkgPT4gdGhpcy5zZXRTdGF0ZSh7b3BlbjogZmFsc2V9KX1cclxuICAgICAgICAgICAgICAgICAgICBvblNlbGVjdGlvbj17KHJlc291cmNlcykgPT4gdGhpcy5oYW5kbGVTZWxlY3Rpb24ocmVzb3VyY2VzKX0gXHJcbiAgICAgICAgICAgICAgICAvPiBcclxuICAgICAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgey8vIERpc3BsYXkgQ2xlYXJpbmcgQnV0dG9uIGlmIGFueSBpdGVtIGlzIHNlbGVjdGVkXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5kZWxldGVCdXR0b25TaG93biA/IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IHN0eWxlPXt7bWFyZ2luQm90dG9tOiAnMTVweCd9fT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxCdXR0b24gZGVzdHJ1Y3RpdmUgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB0aGlzLmhhbmRsZUNsZWFyKCl9PkNsZWFyIGFsbDwvQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj4gXHJcbiAgICAgICAgICAgICAgICAgICAgOiBudWxsfSBcclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPENhcmQ+XHJcbiAgICAgICAgICAgICAgICA8UmVzb3VyY2VMaXN0IC8qQWRkaW5nIFJlc291cmNlIExpc3Qgd2ljaCBjb25zaXN0cyBvZiBSZXNvdXJjZUl0ZW1zICovXHJcbiAgICAgICAgICAgICAgICAgICAgcmVzb3VyY2VOYW1lPXt7c2luZ3VsYXI6ICdwcm9kdWN0JywgcGx1cmFsOiAncHJvZHVjdCd9fVxyXG4gICAgICAgICAgICAgICAgICAgIGl0ZW1zPXsgdGhpcy5zdGF0ZS5yZXNvdXJjZUxpc3RBcnIgfVxyXG4gICAgICAgICAgICAgICAgICAgIHJlbmRlckl0ZW09eyhpdGVtKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qge3Jlc291cmNlSW5kZXgsIGlkLCAgbmFtZSwgYXZhdGFyU291cmNlLCBwcmljZX0gPSBpdGVtOyAvL1NldHRpbmcgSXRlbVxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxSZXNvdXJjZUl0ZW1cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ9e2lkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtZWRpYT0gezxBdmF0YXIgY3VzdG9tZXIgc2l6ZT1cIm1lZGl1bVwiIG5hbWU9e25hbWV9IHNvdXJjZT17YXZhdGFyU291cmNlfSAvPn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgYWNjZXNzaWJpbGl0eUxhYmVsPXtgVmlldyBkZXRhaWxzIGZvciAke25hbWV9YH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8aDI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8VGV4dFN0eWxlIHZhcmlhdGlvbj1cInN0cm9uZ1wiPntuYW1lfTwvVGV4dFN0eWxlPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2gyPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2PntwcmljZX08L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBzdHlsZT17e3Bvc2l0aW9uOidhYnNvbHV0ZScsIHJpZ2h0OicyMHB4JywgbWFyZ2luVG9wOiAnLTQwcHgnfX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QnV0dG9uIGRlc3RydWN0aXZlIHJlc291cmNlSW5kZXg9e3Jlc291cmNlSW5kZXh9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoaW5kKSA9PiB0aGlzLmhhbmRsZURlbGV0ZShpdGVtLnJlc291cmNlSW5kZXgpfT5EZWxldGU8L0J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvUmVzb3VyY2VJdGVtPlxyXG4gICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICAgICAgfX1cclxuICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICA8L0NhcmQ+XHJcbiAgICAgICAgPC9QYWdlPiBcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8vSGFuZGxpbmcgRGVsZXRpb24gb2YgZWxlbWVudCBmcm9tIFJlc291cmNlIExpc3RcclxuICAgIGhhbmRsZURlbGV0ZSA9IChpbmQpID0+IHtcclxuICAgICAgICB2YXIgcG9zID0gdGhpcy5zdGF0ZS5yZXNvdXJjZUxpc3RBcnIubWFwKGZ1bmN0aW9uKGUpIHsgcmV0dXJuIGUucmVzb3VyY2VJbmRleDsgfSkuaW5kZXhPZihpbmQpOyAvL0RldGVjdGluZyBwb3NpdGlvbiBvZiBpdGVtIGJ5IGl0YHMgJ3Jlc291cmNlSW5kZXgnIHZhbHVlXHJcbiAgICAgICAgdGhpcy5zdGF0ZS5yZXNvdXJjZUxpc3RBcnIuc3BsaWNlKHBvcywgMSk7IC8vRGVsZXRlIGl0ZW0gZnJvbSBwcm9kdWN0cyBhcnJheVxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyByZXNvdXJjZUxpc3RBcnI6ICB0aGlzLnN0YXRlLnJlc291cmNlTGlzdEFyciB9KTsgLy9VcGRhdGluZyBzdGF0ZVxyXG4gICAgICAgIC8vSW4gY2FzZSBpZiBhbGwgb2JqZWN0cyByZW1vdmVkLCBoaWRlICdDbGVhciBBbGwnIGJ1dHRvblxyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnJlc291cmNlTGlzdEFyci5sZW5ndGggPT0gMCkge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtkZWxldGVCdXR0b25TaG93bjogZmFsc2V9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy9IYW5kbGluZyAnQ2xlYXIgQWxsJyBidXR0b24gYWN0aW9uXHJcbiAgICBoYW5kbGVDbGVhciA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VMaXN0QXJyOiBbXSB9KTsgLy9SZW1vdmUgYWxsIGl0ZW1zIGZyb20gcHJvZHVjdGBzIGFycmF5XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7ZGVsZXRlQnV0dG9uU2hvd246IGZhbHNlfSk7Ly9IaWRlICdDbGVhciBBbGwnIGJ1dHRvblxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyByZXNvdXJjZUluZGV4OiAwIH0pOy8vU2V0ICdSZXNvdXJjZUluZGV4JyBjb3VudGVyIHRvIDBcclxuICAgIH1cclxuXHJcbiAgICAvL0hhbmRsaW5nIGl0ZW1zIHNlbGVjdGlvbiBmcm9tIHJlc291cmNlIHBpY2tlclxyXG4gICAgaGFuZGxlU2VsZWN0aW9uID0gKHJlc291cmNlcykgPT4ge1xyXG4gICAgICAgIC8vbWFwcGluZyBuZXcgYXJyYXlzIHdpdGggbmVlZGVkIHByb2R1Y3RzIHZhbHVlc1xyXG4gICAgICAgIGNvbnN0IHRpdGxlRnJvbVJlc291cmNlcyA9IHJlc291cmNlcy5zZWxlY3Rpb24ubWFwKChwcm9kdWN0KSA9PiBwcm9kdWN0LnRpdGxlKTtcclxuICAgICAgICBjb25zdCBpZEZyb21SZXNvdXJjZXMgPSByZXNvdXJjZXMuc2VsZWN0aW9uLm1hcCgocHJvZHVjdCkgPT4gcHJvZHVjdC5pZCk7XHJcbiAgICAgICAgY29uc3QgcHJpY2VGcm9tUmVzb3VyY2VzID0gcmVzb3VyY2VzLnNlbGVjdGlvbi5tYXAoKHByb2R1Y3QpID0+IFxyXG4gICAgICAgIHtmb3IgKGxldCB4ID0gMDsgeCA8IHByb2R1Y3QudmFyaWFudHMubGVuZ3RoOyB4KyspIHtcclxuICAgICAgICAgICAgcHJvZHVjdC52YXJpYW50c1t4XS5wcmljZVxyXG4gICAgICAgIH19XHJcbiAgICAgICAgKTtcclxuICAgICAgICBjb25zdCBpbWdGcm9tUmVzb3VyY2VzID0gcmVzb3VyY2VzLnNlbGVjdGlvbi5tYXAoKHByb2R1Y3QpID0+IHByb2R1Y3QuaW1hZ2VzWzBdLm9yaWdpbmFsU3JjKTtcclxuICAgICAgICAvL1Nob3cgJ0NsZWFyIEFsbCcgYnV0dG9uXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7ZGVsZXRlQnV0dG9uU2hvd246IHRydWV9KTtcclxuICAgICAgICAvL0Nsb3NlIFJlc291cmNlIFBpY2tlclxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe29wZW46IGZhbHNlfSk7XHJcbiAgICAgICAgLy9BZGRpbmcgcHJvZHVjdHMgdG8gcHJvZHVjdGBzIGFycmF5XHJcbiAgICAgICAgZm9yIChsZXQgeCA9IDA7IHggPCBwcmljZUZyb21SZXNvdXJjZXMubGVuZ3RoOyB4KyspIHtcclxuICAgICAgICAgICAgICAgIGxldCBqb2luZWQgPSB0aGlzLnN0YXRlLnJlc291cmNlTGlzdEFyci5jb25jYXQoe3Jlc291cmNlSW5kZXg6IHRoaXMuc3RhdGUucmVzb3VyY2VJbmRleCwgaWQ6IGlkRnJvbVJlc291cmNlc1t4XSwgIG5hbWU6dGl0bGVGcm9tUmVzb3VyY2VzW3hdLCBhdmF0YXJTb3VyY2U6aW1nRnJvbVJlc291cmNlc1t4XSwgcHJpY2U6cHJpY2VGcm9tUmVzb3VyY2VzW3hdfSk7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhwcmljZUZyb21SZXNvdXJjZXMpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHJlc291cmNlTGlzdEFycjogam9pbmVkIH0pO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHJlc291cmNlSW5kZXg6IHRoaXMuc3RhdGUucmVzb3VyY2VJbmRleCArIDEgfSk7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbn1cclxuICBcclxuICBleHBvcnQgZGVmYXVsdCBJbmRleDsiXSwic291cmNlUm9vdCI6IiJ9