webpackHotUpdate_N_E("pages/index",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/esm/createClass */ "./node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inherits */ "./node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _shopify_polaris__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @shopify/polaris */ "./node_modules/@shopify/polaris/dist/esm/index.js");
/* harmony import */ var _shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @shopify/app-bridge-react */ "./node_modules/@shopify/app-bridge-react/index.js");
/* harmony import */ var _shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_10__);










function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return Object(_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

//Import Polaris elements, Resource Picker and React




var Index = /*#__PURE__*/function (_Component) {
  Object(_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__["default"])(Index, _Component);

  var _super = _createSuper(Index);

  function Index() {
    var _this;

    Object(_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, Index);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "state", {
      open: false,
      resourceListArr: [],
      deleteButtonShown: false,
      resourceIndex: 0
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleDelete", function (ind) {
      var pos = _this.state.resourceListArr.map(function (e) {
        return e.resourceIndex;
      }).indexOf(ind); //Detecting position of item by it`s 'resourceIndex' value


      _this.state.resourceListArr.splice(pos, 1); //Delete item from products array


      _this.setState({
        resourceListArr: _this.state.resourceListArr
      }); //Updating state
      //In case if all objects removed, hide 'Clear All' button


      if (_this.state.resourceListArr.length == 0) {
        _this.setState({
          deleteButtonShown: false
        });
      }
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleClear", function () {
      _this.setState({
        resourceListArr: []
      }); //Remove all items from product`s array


      _this.setState({
        deleteButtonShown: false
      }); //Hide 'Clear All' button


      _this.setState({
        resourceIndex: 0
      }); //Set 'ResourceIndex' counter to 0

    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleSelection", function (resources) {
      //mapping new arrays with needed products values
      var titleFromResources = resources.selection.map(function (product) {
        return product.title;
      });
      var idFromResources = resources.selection.map(function (product) {
        return product.id;
      });
      var priceFromResources = resources.selection.map(function (product) {
        return product.variants.map(function (e) {
          return e.price;
        });
      });
      var variantFromResources = resources.selection.map(function (product) {
        return product.variants.map(function (e) {
          return e.title;
        });
      });
      var imgFromResources = resources.selection.map(function (product) {
        return product.images[0].originalSrc;
      }); //Show 'Clear All' button

      _this.setState({
        deleteButtonShown: true
      }); //Close Resource Picker


      _this.setState({
        open: false
      }); //Adding products to product`s array


      console.log(resources);
      console.log(variantFromResources);

      for (var x = 0; x < priceFromResources.length; x++) {
        if (priceFromResources[x].length == 1) {
          var joined = _this.state.resourceListArr.concat({
            resourceIndex: _this.state.resourceIndex,
            id: idFromResources[x],
            name: titleFromResources[x],
            avatarSource: imgFromResources[x],
            price: priceFromResources[x],
            variant: variantFromResources[x] == 'Title' ? variantFromResources[x] : ' '
          });

          _this.setState({
            resourceListArr: joined
          });

          _this.setState({
            resourceIndex: _this.state.resourceIndex + 1
          });
        } else {
          for (var y = 0; y < priceFromResources[x].length; y++) {
            var newJoined = _this.state.resourceListArr.concat({
              resourceIndex: _this.state.resourceIndex,
              id: idFromResources[x],
              name: titleFromResources[x],
              avatarSource: imgFromResources[x],
              price: priceFromResources[x][y],
              variant: variantFromResources[x][y]
            });

            _this.setState({
              resourceListArr: newJoined
            });

            _this.setState({
              resourceIndex: _this.state.resourceIndex + 1
            });
          }
        }
      }
    });

    return _this;
  }

  Object(_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(Index, [{
    key: "render",
    //set state
    value: function render() {
      var _this2 = this;

      return (
        /*#__PURE__*/
        //Page styling using Polaris Page element, Text an Button
        Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxs"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Page"], {
          fullWidth: true,
          title: "Product Selection",
          primaryAction: {
            content: 'Select product',
            onAction: function onAction() {
              return _this2.setState({
                open: true
              });
            }
          },
          children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9__["ResourcePicker"]
          /*Resource Picker, change state on clothong and handling if items selected*/
          , {
            resourceType: "Product",
            open: this.state.open,
            onCancel: function onCancel() {
              return _this2.setState({
                open: false
              });
            },
            onSelection: function onSelection(resources) {
              return _this2.handleSelection(resources);
            }
          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
            children: // Display Clearing Button if any item is selected
            this.state.deleteButtonShown ? /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
              style: {
                marginBottom: '15px'
              },
              children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Button"], {
                destructive: true,
                onClick: function onClick() {
                  return _this2.handleClear();
                },
                children: "Clear all"
              })
            }) : null
          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Card"], {
            children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["ResourceList"]
            /*Adding Resource List wich consists of ResourceItems */
            , {
              resourceName: {
                singular: 'product',
                plural: 'product'
              },
              items: this.state.resourceListArr,
              renderItem: function renderItem(item) {
                var resourceIndex = item.resourceIndex,
                    id = item.id,
                    name = item.name,
                    avatarSource = item.avatarSource,
                    price = item.price,
                    variant = item.variant; //Setting Item

                return /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxs"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["ResourceItem"], {
                  id: id,
                  media: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Avatar"], {
                    customer: true,
                    size: "medium",
                    name: name,
                    source: avatarSource
                  }),
                  accessibilityLabel: "View details for ".concat(name),
                  children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxs"])("h2", {
                    children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["TextStyle"], {
                      variation: "strong",
                      children: name + ' '
                    }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["TextStyle"], {
                      variation: "strong",
                      children: ' ' + variant
                    })]
                  }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
                    children: price
                  }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
                    style: {
                      position: 'absolute',
                      right: '20px',
                      marginTop: '-40px'
                    },
                    children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Button"], {
                      destructive: true,
                      resourceIndex: resourceIndex,
                      onClick: function onClick(ind) {
                        return _this2.handleDelete(item.resourceIndex);
                      },
                      children: "Delete"
                    })
                  })]
                });
              }
            })
          })]
        })
      );
    } //Handling Deletion of element from Resource List

  }]);

  return Index;
}(react__WEBPACK_IMPORTED_MODULE_10__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Index);

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvaW5kZXguanMiXSwibmFtZXMiOlsiSW5kZXgiLCJvcGVuIiwicmVzb3VyY2VMaXN0QXJyIiwiZGVsZXRlQnV0dG9uU2hvd24iLCJyZXNvdXJjZUluZGV4IiwiaW5kIiwicG9zIiwic3RhdGUiLCJtYXAiLCJlIiwiaW5kZXhPZiIsInNwbGljZSIsInNldFN0YXRlIiwibGVuZ3RoIiwicmVzb3VyY2VzIiwidGl0bGVGcm9tUmVzb3VyY2VzIiwic2VsZWN0aW9uIiwicHJvZHVjdCIsInRpdGxlIiwiaWRGcm9tUmVzb3VyY2VzIiwiaWQiLCJwcmljZUZyb21SZXNvdXJjZXMiLCJ2YXJpYW50cyIsInByaWNlIiwidmFyaWFudEZyb21SZXNvdXJjZXMiLCJpbWdGcm9tUmVzb3VyY2VzIiwiaW1hZ2VzIiwib3JpZ2luYWxTcmMiLCJjb25zb2xlIiwibG9nIiwieCIsImpvaW5lZCIsImNvbmNhdCIsIm5hbWUiLCJhdmF0YXJTb3VyY2UiLCJ2YXJpYW50IiwieSIsIm5ld0pvaW5lZCIsImNvbnRlbnQiLCJvbkFjdGlvbiIsImhhbmRsZVNlbGVjdGlvbiIsIm1hcmdpbkJvdHRvbSIsImhhbmRsZUNsZWFyIiwic2luZ3VsYXIiLCJwbHVyYWwiLCJpdGVtIiwicG9zaXRpb24iLCJyaWdodCIsIm1hcmdpblRvcCIsImhhbmRsZURlbGV0ZSIsIkNvbXBvbmVudCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7O0lBRU1BLEs7Ozs7Ozs7Ozs7Ozs7Ozs7Z05BQ007QUFBRUMsVUFBSSxFQUFFLEtBQVI7QUFBZUMscUJBQWUsRUFBRSxFQUFoQztBQUFvQ0MsdUJBQWlCLEVBQUUsS0FBdkQ7QUFBOERDLG1CQUFhLEVBQUU7QUFBN0UsSzs7dU5BMkRPLFVBQUNDLEdBQUQsRUFBUztBQUNwQixVQUFJQyxHQUFHLEdBQUcsTUFBS0MsS0FBTCxDQUFXTCxlQUFYLENBQTJCTSxHQUEzQixDQUErQixVQUFTQyxDQUFULEVBQVk7QUFBRSxlQUFPQSxDQUFDLENBQUNMLGFBQVQ7QUFBeUIsT0FBdEUsRUFBd0VNLE9BQXhFLENBQWdGTCxHQUFoRixDQUFWLENBRG9CLENBQzRFOzs7QUFDaEcsWUFBS0UsS0FBTCxDQUFXTCxlQUFYLENBQTJCUyxNQUEzQixDQUFrQ0wsR0FBbEMsRUFBdUMsQ0FBdkMsRUFGb0IsQ0FFdUI7OztBQUMzQyxZQUFLTSxRQUFMLENBQWM7QUFBRVYsdUJBQWUsRUFBRyxNQUFLSyxLQUFMLENBQVdMO0FBQS9CLE9BQWQsRUFIb0IsQ0FHNkM7QUFDakU7OztBQUNBLFVBQUksTUFBS0ssS0FBTCxDQUFXTCxlQUFYLENBQTJCVyxNQUEzQixJQUFxQyxDQUF6QyxFQUE0QztBQUN4QyxjQUFLRCxRQUFMLENBQWM7QUFBQ1QsMkJBQWlCLEVBQUU7QUFBcEIsU0FBZDtBQUNIO0FBQ0osSzs7c05BR2EsWUFBTTtBQUNoQixZQUFLUyxRQUFMLENBQWM7QUFBRVYsdUJBQWUsRUFBRTtBQUFuQixPQUFkLEVBRGdCLENBQ3dCOzs7QUFDeEMsWUFBS1UsUUFBTCxDQUFjO0FBQUNULHlCQUFpQixFQUFFO0FBQXBCLE9BQWQsRUFGZ0IsQ0FFMEI7OztBQUMxQyxZQUFLUyxRQUFMLENBQWM7QUFBRVIscUJBQWEsRUFBRTtBQUFqQixPQUFkLEVBSGdCLENBR29COztBQUN2QyxLOzswTkFHaUIsVUFBQ1UsU0FBRCxFQUFlO0FBQzdCO0FBQ0EsVUFBTUMsa0JBQWtCLEdBQUdELFNBQVMsQ0FBQ0UsU0FBVixDQUFvQlIsR0FBcEIsQ0FBd0IsVUFBQ1MsT0FBRDtBQUFBLGVBQWFBLE9BQU8sQ0FBQ0MsS0FBckI7QUFBQSxPQUF4QixDQUEzQjtBQUNBLFVBQU1DLGVBQWUsR0FBR0wsU0FBUyxDQUFDRSxTQUFWLENBQW9CUixHQUFwQixDQUF3QixVQUFDUyxPQUFEO0FBQUEsZUFBYUEsT0FBTyxDQUFDRyxFQUFyQjtBQUFBLE9BQXhCLENBQXhCO0FBQ0EsVUFBTUMsa0JBQWtCLEdBQUdQLFNBQVMsQ0FBQ0UsU0FBVixDQUFvQlIsR0FBcEIsQ0FBd0IsVUFBQ1MsT0FBRDtBQUFBLGVBQWFBLE9BQU8sQ0FBQ0ssUUFBUixDQUFpQmQsR0FBakIsQ0FBcUIsVUFBU0MsQ0FBVCxFQUFZO0FBQUUsaUJBQU9BLENBQUMsQ0FBQ2MsS0FBVDtBQUFpQixTQUFwRCxDQUFiO0FBQUEsT0FBeEIsQ0FBM0I7QUFDQSxVQUFNQyxvQkFBb0IsR0FBR1YsU0FBUyxDQUFDRSxTQUFWLENBQW9CUixHQUFwQixDQUF3QixVQUFDUyxPQUFEO0FBQUEsZUFBYUEsT0FBTyxDQUFDSyxRQUFSLENBQWlCZCxHQUFqQixDQUFxQixVQUFTQyxDQUFULEVBQVk7QUFBRSxpQkFBT0EsQ0FBQyxDQUFDUyxLQUFUO0FBQWlCLFNBQXBELENBQWI7QUFBQSxPQUF4QixDQUE3QjtBQUNBLFVBQU1PLGdCQUFnQixHQUFHWCxTQUFTLENBQUNFLFNBQVYsQ0FBb0JSLEdBQXBCLENBQXdCLFVBQUNTLE9BQUQ7QUFBQSxlQUFhQSxPQUFPLENBQUNTLE1BQVIsQ0FBZSxDQUFmLEVBQWtCQyxXQUEvQjtBQUFBLE9BQXhCLENBQXpCLENBTjZCLENBTzdCOztBQUNBLFlBQUtmLFFBQUwsQ0FBYztBQUFDVCx5QkFBaUIsRUFBRTtBQUFwQixPQUFkLEVBUjZCLENBUzdCOzs7QUFDQSxZQUFLUyxRQUFMLENBQWM7QUFBQ1gsWUFBSSxFQUFFO0FBQVAsT0FBZCxFQVY2QixDQVc3Qjs7O0FBQ0EyQixhQUFPLENBQUNDLEdBQVIsQ0FBWWYsU0FBWjtBQUNBYyxhQUFPLENBQUNDLEdBQVIsQ0FBWUwsb0JBQVo7O0FBQ0EsV0FBSyxJQUFJTSxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHVCxrQkFBa0IsQ0FBQ1IsTUFBdkMsRUFBK0NpQixDQUFDLEVBQWhELEVBQW9EO0FBQ2hELFlBQUlULGtCQUFrQixDQUFDUyxDQUFELENBQWxCLENBQXNCakIsTUFBdEIsSUFBZ0MsQ0FBcEMsRUFBc0M7QUFDbEMsY0FBSWtCLE1BQU0sR0FBRyxNQUFLeEIsS0FBTCxDQUFXTCxlQUFYLENBQTJCOEIsTUFBM0IsQ0FBa0M7QUFBQzVCLHlCQUFhLEVBQUUsTUFBS0csS0FBTCxDQUFXSCxhQUEzQjtBQUEwQ2dCLGNBQUUsRUFBRUQsZUFBZSxDQUFDVyxDQUFELENBQTdEO0FBQW1FRyxnQkFBSSxFQUFDbEIsa0JBQWtCLENBQUNlLENBQUQsQ0FBMUY7QUFBK0ZJLHdCQUFZLEVBQUNULGdCQUFnQixDQUFDSyxDQUFELENBQTVIO0FBQWlJUCxpQkFBSyxFQUFDRixrQkFBa0IsQ0FBQ1MsQ0FBRCxDQUF6SjtBQUE4SkssbUJBQU8sRUFBRVgsb0JBQW9CLENBQUNNLENBQUQsQ0FBcEIsSUFBMkIsT0FBM0IsR0FBc0NOLG9CQUFvQixDQUFDTSxDQUFELENBQTFELEdBQWdFO0FBQXZPLFdBQWxDLENBQWI7O0FBQ0EsZ0JBQUtsQixRQUFMLENBQWM7QUFBRVYsMkJBQWUsRUFBRTZCO0FBQW5CLFdBQWQ7O0FBQ0EsZ0JBQUtuQixRQUFMLENBQWM7QUFBRVIseUJBQWEsRUFBRSxNQUFLRyxLQUFMLENBQVdILGFBQVgsR0FBMkI7QUFBNUMsV0FBZDtBQUNILFNBSkQsTUFLSztBQUNELGVBQUssSUFBSWdDLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdmLGtCQUFrQixDQUFDUyxDQUFELENBQWxCLENBQXNCakIsTUFBMUMsRUFBa0R1QixDQUFDLEVBQW5ELEVBQXVEO0FBQ25ELGdCQUFJQyxTQUFTLEdBQUcsTUFBSzlCLEtBQUwsQ0FBV0wsZUFBWCxDQUEyQjhCLE1BQTNCLENBQWtDO0FBQUM1QiwyQkFBYSxFQUFFLE1BQUtHLEtBQUwsQ0FBV0gsYUFBM0I7QUFBMENnQixnQkFBRSxFQUFFRCxlQUFlLENBQUNXLENBQUQsQ0FBN0Q7QUFBbUVHLGtCQUFJLEVBQUNsQixrQkFBa0IsQ0FBQ2UsQ0FBRCxDQUExRjtBQUErRkksMEJBQVksRUFBQ1QsZ0JBQWdCLENBQUNLLENBQUQsQ0FBNUg7QUFBaUlQLG1CQUFLLEVBQUNGLGtCQUFrQixDQUFDUyxDQUFELENBQWxCLENBQXNCTSxDQUF0QixDQUF2STtBQUFpS0QscUJBQU8sRUFBRVgsb0JBQW9CLENBQUNNLENBQUQsQ0FBcEIsQ0FBd0JNLENBQXhCO0FBQTFLLGFBQWxDLENBQWhCOztBQUNBLGtCQUFLeEIsUUFBTCxDQUFjO0FBQUVWLDZCQUFlLEVBQUVtQztBQUFuQixhQUFkOztBQUNBLGtCQUFLekIsUUFBTCxDQUFjO0FBQUVSLDJCQUFhLEVBQUUsTUFBS0csS0FBTCxDQUFXSCxhQUFYLEdBQTJCO0FBQTVDLGFBQWQ7QUFDSDtBQUNKO0FBQ0o7QUFDSixLOzs7Ozs7O0FBekd1Rjs2QkFFL0U7QUFBQTs7QUFDTDtBQUFBO0FBQ0E7QUFDQSx1RUFBQyxxREFBRDtBQUNJLG1CQUFTLE1BRGI7QUFFSSxlQUFLLEVBQUMsbUJBRlY7QUFHSSx1QkFBYSxFQUFFO0FBQ1hrQyxtQkFBTyxFQUFFLGdCQURFO0FBRVhDLG9CQUFRLEVBQUU7QUFBQSxxQkFBTSxNQUFJLENBQUMzQixRQUFMLENBQWM7QUFBQ1gsb0JBQUksRUFBRTtBQUFQLGVBQWQsQ0FBTjtBQUFBO0FBRkMsV0FIbkI7QUFBQSxrQ0FRUSw4REFBQyx3RUFBYztBQUFFO0FBQWpCO0FBQ0ksd0JBQVksRUFBQyxTQURqQjtBQUVJLGdCQUFJLEVBQUUsS0FBS00sS0FBTCxDQUFXTixJQUZyQjtBQUdJLG9CQUFRLEVBQUc7QUFBQSxxQkFBTSxNQUFJLENBQUNXLFFBQUwsQ0FBYztBQUFDWCxvQkFBSSxFQUFFO0FBQVAsZUFBZCxDQUFOO0FBQUEsYUFIZjtBQUlJLHVCQUFXLEVBQUUscUJBQUNhLFNBQUQ7QUFBQSxxQkFBZSxNQUFJLENBQUMwQixlQUFMLENBQXFCMUIsU0FBckIsQ0FBZjtBQUFBO0FBSmpCLFlBUlIsZUFjUTtBQUFBLHNCQUNLO0FBQ0QsaUJBQUtQLEtBQUwsQ0FBV0osaUJBQVgsZ0JBQ0k7QUFBSyxtQkFBSyxFQUFFO0FBQUNzQyw0QkFBWSxFQUFFO0FBQWYsZUFBWjtBQUFBLHFDQUNJLDhEQUFDLHVEQUFEO0FBQVEsMkJBQVcsTUFBbkI7QUFDQSx1QkFBTyxFQUFFO0FBQUEseUJBQU0sTUFBSSxDQUFDQyxXQUFMLEVBQU47QUFBQSxpQkFEVDtBQUFBO0FBQUE7QUFESixjQURKLEdBS0U7QUFQTixZQWRSLGVBdUJRLDhEQUFDLHFEQUFEO0FBQUEsbUNBQ0EsOERBQUMsNkRBQVk7QUFBQztBQUFkO0FBQ0ksMEJBQVksRUFBRTtBQUFDQyx3QkFBUSxFQUFFLFNBQVg7QUFBc0JDLHNCQUFNLEVBQUU7QUFBOUIsZUFEbEI7QUFFSSxtQkFBSyxFQUFHLEtBQUtyQyxLQUFMLENBQVdMLGVBRnZCO0FBR0ksd0JBQVUsRUFBRSxvQkFBQzJDLElBQUQsRUFBVTtBQUFBLG9CQUNmekMsYUFEZSxHQUMyQ3lDLElBRDNDLENBQ2Z6QyxhQURlO0FBQUEsb0JBQ0FnQixFQURBLEdBQzJDeUIsSUFEM0MsQ0FDQXpCLEVBREE7QUFBQSxvQkFDS2EsSUFETCxHQUMyQ1ksSUFEM0MsQ0FDS1osSUFETDtBQUFBLG9CQUNXQyxZQURYLEdBQzJDVyxJQUQzQyxDQUNXWCxZQURYO0FBQUEsb0JBQ3lCWCxLQUR6QixHQUMyQ3NCLElBRDNDLENBQ3lCdEIsS0FEekI7QUFBQSxvQkFDZ0NZLE9BRGhDLEdBQzJDVSxJQUQzQyxDQUNnQ1YsT0FEaEMsRUFDaUQ7O0FBQ3ZFLG9DQUNJLCtEQUFDLDZEQUFEO0FBQ0Esb0JBQUUsRUFBRWYsRUFESjtBQUVBLHVCQUFLLGVBQUcsOERBQUMsdURBQUQ7QUFBUSw0QkFBUSxNQUFoQjtBQUFpQix3QkFBSSxFQUFDLFFBQXRCO0FBQStCLHdCQUFJLEVBQUVhLElBQXJDO0FBQTJDLDBCQUFNLEVBQUVDO0FBQW5ELG9CQUZSO0FBR0Esb0NBQWtCLDZCQUFzQkQsSUFBdEIsQ0FIbEI7QUFBQSwwQ0FLQTtBQUFBLDRDQUNJLDhEQUFDLDBEQUFEO0FBQVcsK0JBQVMsRUFBQyxRQUFyQjtBQUFBLGdDQUErQkEsSUFBSSxHQUFHO0FBQXRDLHNCQURKLGVBRUksOERBQUMsMERBQUQ7QUFBVywrQkFBUyxFQUFDLFFBQXJCO0FBQUEsZ0NBQStCLE1BQU1FO0FBQXJDLHNCQUZKO0FBQUEsb0JBTEEsZUFTQTtBQUFBLDhCQUFNWjtBQUFOLG9CQVRBLGVBVUE7QUFBSyx5QkFBSyxFQUFFO0FBQUN1Qiw4QkFBUSxFQUFDLFVBQVY7QUFBc0JDLDJCQUFLLEVBQUMsTUFBNUI7QUFBb0NDLCtCQUFTLEVBQUU7QUFBL0MscUJBQVo7QUFBQSwyQ0FDSSw4REFBQyx1REFBRDtBQUFRLGlDQUFXLE1BQW5CO0FBQW9CLG1DQUFhLEVBQUU1QyxhQUFuQztBQUNBLDZCQUFPLEVBQUUsaUJBQUNDLEdBQUQ7QUFBQSwrQkFBUyxNQUFJLENBQUM0QyxZQUFMLENBQWtCSixJQUFJLENBQUN6QyxhQUF2QixDQUFUO0FBQUEsdUJBRFQ7QUFBQTtBQUFBO0FBREosb0JBVkE7QUFBQSxrQkFESjtBQWlCQztBQXRCTDtBQURBLFlBdkJSO0FBQUE7QUFGQTtBQXFESCxLLENBRUQ7Ozs7O0VBM0RnQjhDLGdEOztBQThHSGxELG9FQUFmIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3BhZ2VzL2luZGV4LmNmMjA3YTA1OWE1YzVkMzkxM2RjLmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvL0ltcG9ydCBQb2xhcmlzIGVsZW1lbnRzLCBSZXNvdXJjZSBQaWNrZXIgYW5kIFJlYWN0XHJcbmltcG9ydCB7IFBhZ2UsIENhcmQsIFJlc291cmNlTGlzdCwgUmVzb3VyY2VJdGVtLCBBdmF0YXIsIFRleHRTdHlsZSwgQnV0dG9uIH0gZnJvbSBcIkBzaG9waWZ5L3BvbGFyaXNcIjtcclxuaW1wb3J0IHsgUmVzb3VyY2VQaWNrZXIgfSBmcm9tIFwiQHNob3BpZnkvYXBwLWJyaWRnZS1yZWFjdFwiO1xyXG5pbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xyXG5cclxuY2xhc3MgSW5kZXggZXh0ZW5kcyBDb21wb25lbnQge1xyXG4gICAgc3RhdGUgPSB7IG9wZW46IGZhbHNlLCByZXNvdXJjZUxpc3RBcnI6IFtdLCBkZWxldGVCdXR0b25TaG93bjogZmFsc2UsIHJlc291cmNlSW5kZXg6IDAgfS8vc2V0IHN0YXRlXHJcblxyXG4gICAgcmVuZGVyKCkgeyBcclxuICAgICAgICByZXR1cm4gKCBcclxuICAgICAgICAvL1BhZ2Ugc3R5bGluZyB1c2luZyBQb2xhcmlzIFBhZ2UgZWxlbWVudCwgVGV4dCBhbiBCdXR0b25cclxuICAgICAgICA8UGFnZSAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBmdWxsV2lkdGhcclxuICAgICAgICAgICAgdGl0bGU9XCJQcm9kdWN0IFNlbGVjdGlvblwiXHJcbiAgICAgICAgICAgIHByaW1hcnlBY3Rpb249e3tcclxuICAgICAgICAgICAgICAgIGNvbnRlbnQ6ICdTZWxlY3QgcHJvZHVjdCcsXHJcbiAgICAgICAgICAgICAgICBvbkFjdGlvbjogKCkgPT4gdGhpcy5zZXRTdGF0ZSh7b3BlbjogdHJ1ZX0pXHJcbiAgICAgICAgICAgIH19XHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIDxSZXNvdXJjZVBpY2tlciAgLypSZXNvdXJjZSBQaWNrZXIsIGNoYW5nZSBzdGF0ZSBvbiBjbG90aG9uZyBhbmQgaGFuZGxpbmcgaWYgaXRlbXMgc2VsZWN0ZWQqL1xyXG4gICAgICAgICAgICAgICAgICAgIHJlc291cmNlVHlwZT1cIlByb2R1Y3RcIlxyXG4gICAgICAgICAgICAgICAgICAgIG9wZW49e3RoaXMuc3RhdGUub3Blbn1cclxuICAgICAgICAgICAgICAgICAgICBvbkNhbmNlbD17ICgpID0+IHRoaXMuc2V0U3RhdGUoe29wZW46IGZhbHNlfSl9XHJcbiAgICAgICAgICAgICAgICAgICAgb25TZWxlY3Rpb249eyhyZXNvdXJjZXMpID0+IHRoaXMuaGFuZGxlU2VsZWN0aW9uKHJlc291cmNlcyl9IFxyXG4gICAgICAgICAgICAgICAgLz4gXHJcbiAgICAgICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIHsvLyBEaXNwbGF5IENsZWFyaW5nIEJ1dHRvbiBpZiBhbnkgaXRlbSBpcyBzZWxlY3RlZFxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc3RhdGUuZGVsZXRlQnV0dG9uU2hvd24gPyBcclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBzdHlsZT17e21hcmdpbkJvdHRvbTogJzE1cHgnfX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QnV0dG9uIGRlc3RydWN0aXZlIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gdGhpcy5oYW5kbGVDbGVhcigpfT5DbGVhciBhbGw8L0J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+IFxyXG4gICAgICAgICAgICAgICAgICAgIDogbnVsbH0gXHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxDYXJkPlxyXG4gICAgICAgICAgICAgICAgPFJlc291cmNlTGlzdCAvKkFkZGluZyBSZXNvdXJjZSBMaXN0IHdpY2ggY29uc2lzdHMgb2YgUmVzb3VyY2VJdGVtcyAqL1xyXG4gICAgICAgICAgICAgICAgICAgIHJlc291cmNlTmFtZT17e3Npbmd1bGFyOiAncHJvZHVjdCcsIHBsdXJhbDogJ3Byb2R1Y3QnfX1cclxuICAgICAgICAgICAgICAgICAgICBpdGVtcz17IHRoaXMuc3RhdGUucmVzb3VyY2VMaXN0QXJyIH1cclxuICAgICAgICAgICAgICAgICAgICByZW5kZXJJdGVtPXsoaXRlbSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHtyZXNvdXJjZUluZGV4LCBpZCwgIG5hbWUsIGF2YXRhclNvdXJjZSwgcHJpY2UsIHZhcmlhbnR9ID0gaXRlbTsgLy9TZXR0aW5nIEl0ZW1cclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8UmVzb3VyY2VJdGVtXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkPXtpZH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgbWVkaWE9IHs8QXZhdGFyIGN1c3RvbWVyIHNpemU9XCJtZWRpdW1cIiBuYW1lPXtuYW1lfSBzb3VyY2U9e2F2YXRhclNvdXJjZX0gLz59XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFjY2Vzc2liaWxpdHlMYWJlbD17YFZpZXcgZGV0YWlscyBmb3IgJHtuYW1lfWB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGgyPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPFRleHRTdHlsZSB2YXJpYXRpb249XCJzdHJvbmdcIj57bmFtZSArICcgJ308L1RleHRTdHlsZT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxUZXh0U3R5bGUgdmFyaWF0aW9uPVwic3Ryb25nXCI+eycgJyArIHZhcmlhbnR9PC9UZXh0U3R5bGU+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvaDI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXY+e3ByaWNlfTwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IHN0eWxlPXt7cG9zaXRpb246J2Fic29sdXRlJywgcmlnaHQ6JzIwcHgnLCBtYXJnaW5Ub3A6ICctNDBweCd9fT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxCdXR0b24gZGVzdHJ1Y3RpdmUgcmVzb3VyY2VJbmRleD17cmVzb3VyY2VJbmRleH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eyhpbmQpID0+IHRoaXMuaGFuZGxlRGVsZXRlKGl0ZW0ucmVzb3VyY2VJbmRleCl9PkRlbGV0ZTwvQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9SZXNvdXJjZUl0ZW0+XHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgICAgICB9fVxyXG4gICAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICAgIDwvQ2FyZD5cclxuICAgICAgICA8L1BhZ2U+IFxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLy9IYW5kbGluZyBEZWxldGlvbiBvZiBlbGVtZW50IGZyb20gUmVzb3VyY2UgTGlzdFxyXG4gICAgaGFuZGxlRGVsZXRlID0gKGluZCkgPT4ge1xyXG4gICAgICAgIHZhciBwb3MgPSB0aGlzLnN0YXRlLnJlc291cmNlTGlzdEFyci5tYXAoZnVuY3Rpb24oZSkgeyByZXR1cm4gZS5yZXNvdXJjZUluZGV4OyB9KS5pbmRleE9mKGluZCk7IC8vRGV0ZWN0aW5nIHBvc2l0aW9uIG9mIGl0ZW0gYnkgaXRgcyAncmVzb3VyY2VJbmRleCcgdmFsdWVcclxuICAgICAgICB0aGlzLnN0YXRlLnJlc291cmNlTGlzdEFyci5zcGxpY2UocG9zLCAxKTsgLy9EZWxldGUgaXRlbSBmcm9tIHByb2R1Y3RzIGFycmF5XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHJlc291cmNlTGlzdEFycjogIHRoaXMuc3RhdGUucmVzb3VyY2VMaXN0QXJyIH0pOyAvL1VwZGF0aW5nIHN0YXRlXHJcbiAgICAgICAgLy9JbiBjYXNlIGlmIGFsbCBvYmplY3RzIHJlbW92ZWQsIGhpZGUgJ0NsZWFyIEFsbCcgYnV0dG9uXHJcbiAgICAgICAgaWYgKHRoaXMuc3RhdGUucmVzb3VyY2VMaXN0QXJyLmxlbmd0aCA9PSAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe2RlbGV0ZUJ1dHRvblNob3duOiBmYWxzZX0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvL0hhbmRsaW5nICdDbGVhciBBbGwnIGJ1dHRvbiBhY3Rpb25cclxuICAgIGhhbmRsZUNsZWFyID0gKCkgPT4ge1xyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyByZXNvdXJjZUxpc3RBcnI6IFtdIH0pOyAvL1JlbW92ZSBhbGwgaXRlbXMgZnJvbSBwcm9kdWN0YHMgYXJyYXlcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtkZWxldGVCdXR0b25TaG93bjogZmFsc2V9KTsvL0hpZGUgJ0NsZWFyIEFsbCcgYnV0dG9uXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHJlc291cmNlSW5kZXg6IDAgfSk7Ly9TZXQgJ1Jlc291cmNlSW5kZXgnIGNvdW50ZXIgdG8gMFxyXG4gICAgfVxyXG5cclxuICAgIC8vSGFuZGxpbmcgaXRlbXMgc2VsZWN0aW9uIGZyb20gcmVzb3VyY2UgcGlja2VyXHJcbiAgICBoYW5kbGVTZWxlY3Rpb24gPSAocmVzb3VyY2VzKSA9PiB7XHJcbiAgICAgICAgLy9tYXBwaW5nIG5ldyBhcnJheXMgd2l0aCBuZWVkZWQgcHJvZHVjdHMgdmFsdWVzXHJcbiAgICAgICAgY29uc3QgdGl0bGVGcm9tUmVzb3VyY2VzID0gcmVzb3VyY2VzLnNlbGVjdGlvbi5tYXAoKHByb2R1Y3QpID0+IHByb2R1Y3QudGl0bGUpO1xyXG4gICAgICAgIGNvbnN0IGlkRnJvbVJlc291cmNlcyA9IHJlc291cmNlcy5zZWxlY3Rpb24ubWFwKChwcm9kdWN0KSA9PiBwcm9kdWN0LmlkKTtcclxuICAgICAgICBjb25zdCBwcmljZUZyb21SZXNvdXJjZXMgPSByZXNvdXJjZXMuc2VsZWN0aW9uLm1hcCgocHJvZHVjdCkgPT4gcHJvZHVjdC52YXJpYW50cy5tYXAoZnVuY3Rpb24oZSkgeyByZXR1cm4gZS5wcmljZTsgfSkpO1xyXG4gICAgICAgIGNvbnN0IHZhcmlhbnRGcm9tUmVzb3VyY2VzID0gcmVzb3VyY2VzLnNlbGVjdGlvbi5tYXAoKHByb2R1Y3QpID0+IHByb2R1Y3QudmFyaWFudHMubWFwKGZ1bmN0aW9uKGUpIHsgcmV0dXJuIGUudGl0bGU7IH0pKTtcclxuICAgICAgICBjb25zdCBpbWdGcm9tUmVzb3VyY2VzID0gcmVzb3VyY2VzLnNlbGVjdGlvbi5tYXAoKHByb2R1Y3QpID0+IHByb2R1Y3QuaW1hZ2VzWzBdLm9yaWdpbmFsU3JjKTtcclxuICAgICAgICAvL1Nob3cgJ0NsZWFyIEFsbCcgYnV0dG9uXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7ZGVsZXRlQnV0dG9uU2hvd246IHRydWV9KTtcclxuICAgICAgICAvL0Nsb3NlIFJlc291cmNlIFBpY2tlclxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe29wZW46IGZhbHNlfSk7XHJcbiAgICAgICAgLy9BZGRpbmcgcHJvZHVjdHMgdG8gcHJvZHVjdGBzIGFycmF5XHJcbiAgICAgICAgY29uc29sZS5sb2cocmVzb3VyY2VzKTtcclxuICAgICAgICBjb25zb2xlLmxvZyh2YXJpYW50RnJvbVJlc291cmNlcyk7XHJcbiAgICAgICAgZm9yIChsZXQgeCA9IDA7IHggPCBwcmljZUZyb21SZXNvdXJjZXMubGVuZ3RoOyB4KyspIHtcclxuICAgICAgICAgICAgaWYgKHByaWNlRnJvbVJlc291cmNlc1t4XS5sZW5ndGggPT0gMSl7XHJcbiAgICAgICAgICAgICAgICBsZXQgam9pbmVkID0gdGhpcy5zdGF0ZS5yZXNvdXJjZUxpc3RBcnIuY29uY2F0KHtyZXNvdXJjZUluZGV4OiB0aGlzLnN0YXRlLnJlc291cmNlSW5kZXgsIGlkOiBpZEZyb21SZXNvdXJjZXNbeF0sICBuYW1lOnRpdGxlRnJvbVJlc291cmNlc1t4XSwgYXZhdGFyU291cmNlOmltZ0Zyb21SZXNvdXJjZXNbeF0sIHByaWNlOnByaWNlRnJvbVJlc291cmNlc1t4XSwgdmFyaWFudDogdmFyaWFudEZyb21SZXNvdXJjZXNbeF0gPT0gJ1RpdGxlJyA/ICB2YXJpYW50RnJvbVJlc291cmNlc1t4XSA6ICcgJ30pO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHJlc291cmNlTGlzdEFycjogam9pbmVkIH0pO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHJlc291cmNlSW5kZXg6IHRoaXMuc3RhdGUucmVzb3VyY2VJbmRleCArIDEgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBmb3IgKGxldCB5ID0gMDsgeSA8IHByaWNlRnJvbVJlc291cmNlc1t4XS5sZW5ndGg7IHkrKykge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBuZXdKb2luZWQgPSB0aGlzLnN0YXRlLnJlc291cmNlTGlzdEFyci5jb25jYXQoe3Jlc291cmNlSW5kZXg6IHRoaXMuc3RhdGUucmVzb3VyY2VJbmRleCwgaWQ6IGlkRnJvbVJlc291cmNlc1t4XSwgIG5hbWU6dGl0bGVGcm9tUmVzb3VyY2VzW3hdLCBhdmF0YXJTb3VyY2U6aW1nRnJvbVJlc291cmNlc1t4XSwgcHJpY2U6cHJpY2VGcm9tUmVzb3VyY2VzW3hdW3ldLCB2YXJpYW50OiB2YXJpYW50RnJvbVJlc291cmNlc1t4XVt5XX0pO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyByZXNvdXJjZUxpc3RBcnI6IG5ld0pvaW5lZCB9KTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VJbmRleDogdGhpcy5zdGF0ZS5yZXNvdXJjZUluZGV4ICsgMSB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbn1cclxuICBcclxuICBleHBvcnQgZGVmYXVsdCBJbmRleDsiXSwic291cmNlUm9vdCI6IiJ9