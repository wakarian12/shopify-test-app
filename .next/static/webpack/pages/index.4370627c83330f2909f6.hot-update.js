webpackHotUpdate_N_E("pages/index",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/esm/createClass */ "./node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inherits */ "./node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _shopify_polaris__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @shopify/polaris */ "./node_modules/@shopify/polaris/dist/esm/index.js");
/* harmony import */ var _shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @shopify/app-bridge-react */ "./node_modules/@shopify/app-bridge-react/index.js");
/* harmony import */ var _shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_10__);










function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return Object(_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

//Import Polaris elements, Resource Picker and React




var Index = /*#__PURE__*/function (_Component) {
  Object(_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__["default"])(Index, _Component);

  var _super = _createSuper(Index);

  function Index() {
    var _this;

    Object(_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, Index);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "state", {
      open: false,
      resourceListArr: [],
      deleteButtonShown: false,
      resourceIndex: 0
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleDelete", function (ind) {
      var pos = _this.state.resourceListArr.map(function (e) {
        return e.resourceIndex;
      }).indexOf(ind); //Detecting position of item by it`s 'resourceIndex' value


      _this.state.resourceListArr.splice(pos, 1); //Delete item from products array


      _this.setState({
        resourceListArr: _this.state.resourceListArr
      }); //Updating state
      //In case if all objects removed, hide 'Clear All' button


      if (_this.state.resourceListArr.length == 0) {
        _this.setState({
          deleteButtonShown: false
        });
      }
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleClear", function () {
      _this.setState({
        resourceListArr: []
      }); //Remove all items from product`s array


      _this.setState({
        deleteButtonShown: false
      }); //Hide 'Clear All' button


      _this.setState({
        resourceIndex: 0
      }); //Set 'ResourceIndex' counter to 0

    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleSelection", function (resources) {
      //mapping new arrays with needed products values
      var titleFromResources = resources.selection.map(function (product) {
        return product.title;
      });
      var idFromResources = resources.selection.map(function (product) {
        return product.id;
      });
      var priceFromResources = resources.selection.map(function (product) {
        return product.variants.map(function (e) {
          return e.price;
        });
      });
      var imgFromResources = resources.selection.map(function (product) {
        return product.images[0].originalSrc;
      }); //Show 'Clear All' button

      _this.setState({
        deleteButtonShown: true
      }); //Close Resource Picker


      _this.setState({
        open: false
      }); //Adding products to product`s array


      console.log(resources);

      for (var x = 0; x < priceFromResources.length; x++) {
        if (priceFromResources[x].length == 1) {
          var joined = _this.state.resourceListArr.concat({
            resourceIndex: _this.state.resourceIndex,
            id: idFromResources[x],
            name: titleFromResources[x],
            avatarSource: imgFromResources[x],
            price: priceFromResources[x]
          });

          _this.setState({
            resourceListArr: joined
          });

          _this.setState({
            resourceIndex: _this.state.resourceIndex + 1
          });
        } else {
          for (var y = 0; y < priceFromResources[x].length; y++) {
            var newJoined = _this.state.resourceListArr.concat({
              resourceIndex: _this.state.resourceIndex,
              id: idFromResources[x],
              name: titleFromResources[x],
              avatarSource: imgFromResources[x],
              price: priceFromResources[x][y]
            });

            _this.setState({
              resourceListArr: newJoined
            });

            _this.setState({
              resourceIndex: _this.state.resourceIndex + 1
            });
          }
        }
      }
    });

    return _this;
  }

  Object(_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(Index, [{
    key: "render",
    //set state
    value: function render() {
      var _this2 = this;

      return (
        /*#__PURE__*/
        //Page styling using Polaris Page element, Text an Button
        Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxs"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Page"], {
          fullWidth: true,
          title: "Product Selection",
          primaryAction: {
            content: 'Select product',
            onAction: function onAction() {
              return _this2.setState({
                open: true
              });
            }
          },
          children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9__["ResourcePicker"]
          /*Resource Picker, change state on clothong and handling if items selected*/
          , {
            resourceType: "Product",
            open: this.state.open,
            onCancel: function onCancel() {
              return _this2.setState({
                open: false
              });
            },
            onSelection: function onSelection(resources) {
              return _this2.handleSelection(resources);
            }
          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
            children: // Display Clearing Button if any item is selected
            this.state.deleteButtonShown ? /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
              style: {
                marginBottom: '15px'
              },
              children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Button"], {
                destructive: true,
                onClick: function onClick() {
                  return _this2.handleClear();
                },
                children: "Clear all"
              })
            }) : null
          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Card"], {
            children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["ResourceList"]
            /*Adding Resource List wich consists of ResourceItems */
            , {
              resourceName: {
                singular: 'product',
                plural: 'product'
              },
              items: this.state.resourceListArr,
              renderItem: function renderItem(item) {
                var resourceIndex = item.resourceIndex,
                    id = item.id,
                    name = item.name,
                    avatarSource = item.avatarSource,
                    price = item.price; //Setting Item

                return /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxs"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["ResourceItem"], {
                  id: id,
                  media: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Avatar"], {
                    customer: true,
                    size: "medium",
                    name: name,
                    source: avatarSource
                  }),
                  accessibilityLabel: "View details for ".concat(name),
                  children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxs"])("h2", {
                    children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["TextStyle"], {
                      variation: "strong",
                      children: name
                    }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("span", {
                      children: "text"
                    })]
                  }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
                    children: price
                  }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
                    style: {
                      position: 'absolute',
                      right: '20px',
                      marginTop: '-40px'
                    },
                    children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Button"], {
                      destructive: true,
                      resourceIndex: resourceIndex,
                      onClick: function onClick(ind) {
                        return _this2.handleDelete(item.resourceIndex);
                      },
                      children: "Delete"
                    })
                  })]
                });
              }
            })
          })]
        })
      );
    } //Handling Deletion of element from Resource List

  }]);

  return Index;
}(react__WEBPACK_IMPORTED_MODULE_10__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Index);

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvaW5kZXguanMiXSwibmFtZXMiOlsiSW5kZXgiLCJvcGVuIiwicmVzb3VyY2VMaXN0QXJyIiwiZGVsZXRlQnV0dG9uU2hvd24iLCJyZXNvdXJjZUluZGV4IiwiaW5kIiwicG9zIiwic3RhdGUiLCJtYXAiLCJlIiwiaW5kZXhPZiIsInNwbGljZSIsInNldFN0YXRlIiwibGVuZ3RoIiwicmVzb3VyY2VzIiwidGl0bGVGcm9tUmVzb3VyY2VzIiwic2VsZWN0aW9uIiwicHJvZHVjdCIsInRpdGxlIiwiaWRGcm9tUmVzb3VyY2VzIiwiaWQiLCJwcmljZUZyb21SZXNvdXJjZXMiLCJ2YXJpYW50cyIsInByaWNlIiwiaW1nRnJvbVJlc291cmNlcyIsImltYWdlcyIsIm9yaWdpbmFsU3JjIiwiY29uc29sZSIsImxvZyIsIngiLCJqb2luZWQiLCJjb25jYXQiLCJuYW1lIiwiYXZhdGFyU291cmNlIiwieSIsIm5ld0pvaW5lZCIsImNvbnRlbnQiLCJvbkFjdGlvbiIsImhhbmRsZVNlbGVjdGlvbiIsIm1hcmdpbkJvdHRvbSIsImhhbmRsZUNsZWFyIiwic2luZ3VsYXIiLCJwbHVyYWwiLCJpdGVtIiwicG9zaXRpb24iLCJyaWdodCIsIm1hcmdpblRvcCIsImhhbmRsZURlbGV0ZSIsIkNvbXBvbmVudCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7O0lBRU1BLEs7Ozs7Ozs7Ozs7Ozs7Ozs7Z05BQ007QUFBRUMsVUFBSSxFQUFFLEtBQVI7QUFBZUMscUJBQWUsRUFBRSxFQUFoQztBQUFvQ0MsdUJBQWlCLEVBQUUsS0FBdkQ7QUFBOERDLG1CQUFhLEVBQUU7QUFBN0UsSzs7dU5BMkRPLFVBQUNDLEdBQUQsRUFBUztBQUNwQixVQUFJQyxHQUFHLEdBQUcsTUFBS0MsS0FBTCxDQUFXTCxlQUFYLENBQTJCTSxHQUEzQixDQUErQixVQUFTQyxDQUFULEVBQVk7QUFBRSxlQUFPQSxDQUFDLENBQUNMLGFBQVQ7QUFBeUIsT0FBdEUsRUFBd0VNLE9BQXhFLENBQWdGTCxHQUFoRixDQUFWLENBRG9CLENBQzRFOzs7QUFDaEcsWUFBS0UsS0FBTCxDQUFXTCxlQUFYLENBQTJCUyxNQUEzQixDQUFrQ0wsR0FBbEMsRUFBdUMsQ0FBdkMsRUFGb0IsQ0FFdUI7OztBQUMzQyxZQUFLTSxRQUFMLENBQWM7QUFBRVYsdUJBQWUsRUFBRyxNQUFLSyxLQUFMLENBQVdMO0FBQS9CLE9BQWQsRUFIb0IsQ0FHNkM7QUFDakU7OztBQUNBLFVBQUksTUFBS0ssS0FBTCxDQUFXTCxlQUFYLENBQTJCVyxNQUEzQixJQUFxQyxDQUF6QyxFQUE0QztBQUN4QyxjQUFLRCxRQUFMLENBQWM7QUFBQ1QsMkJBQWlCLEVBQUU7QUFBcEIsU0FBZDtBQUNIO0FBQ0osSzs7c05BR2EsWUFBTTtBQUNoQixZQUFLUyxRQUFMLENBQWM7QUFBRVYsdUJBQWUsRUFBRTtBQUFuQixPQUFkLEVBRGdCLENBQ3dCOzs7QUFDeEMsWUFBS1UsUUFBTCxDQUFjO0FBQUNULHlCQUFpQixFQUFFO0FBQXBCLE9BQWQsRUFGZ0IsQ0FFMEI7OztBQUMxQyxZQUFLUyxRQUFMLENBQWM7QUFBRVIscUJBQWEsRUFBRTtBQUFqQixPQUFkLEVBSGdCLENBR29COztBQUN2QyxLOzswTkFHaUIsVUFBQ1UsU0FBRCxFQUFlO0FBQzdCO0FBQ0EsVUFBTUMsa0JBQWtCLEdBQUdELFNBQVMsQ0FBQ0UsU0FBVixDQUFvQlIsR0FBcEIsQ0FBd0IsVUFBQ1MsT0FBRDtBQUFBLGVBQWFBLE9BQU8sQ0FBQ0MsS0FBckI7QUFBQSxPQUF4QixDQUEzQjtBQUNBLFVBQU1DLGVBQWUsR0FBR0wsU0FBUyxDQUFDRSxTQUFWLENBQW9CUixHQUFwQixDQUF3QixVQUFDUyxPQUFEO0FBQUEsZUFBYUEsT0FBTyxDQUFDRyxFQUFyQjtBQUFBLE9BQXhCLENBQXhCO0FBQ0EsVUFBTUMsa0JBQWtCLEdBQUdQLFNBQVMsQ0FBQ0UsU0FBVixDQUFvQlIsR0FBcEIsQ0FBd0IsVUFBQ1MsT0FBRDtBQUFBLGVBQWFBLE9BQU8sQ0FBQ0ssUUFBUixDQUFpQmQsR0FBakIsQ0FBcUIsVUFBU0MsQ0FBVCxFQUFZO0FBQUUsaUJBQU9BLENBQUMsQ0FBQ2MsS0FBVDtBQUFpQixTQUFwRCxDQUFiO0FBQUEsT0FBeEIsQ0FBM0I7QUFDQSxVQUFNQyxnQkFBZ0IsR0FBR1YsU0FBUyxDQUFDRSxTQUFWLENBQW9CUixHQUFwQixDQUF3QixVQUFDUyxPQUFEO0FBQUEsZUFBYUEsT0FBTyxDQUFDUSxNQUFSLENBQWUsQ0FBZixFQUFrQkMsV0FBL0I7QUFBQSxPQUF4QixDQUF6QixDQUw2QixDQU03Qjs7QUFDQSxZQUFLZCxRQUFMLENBQWM7QUFBQ1QseUJBQWlCLEVBQUU7QUFBcEIsT0FBZCxFQVA2QixDQVE3Qjs7O0FBQ0EsWUFBS1MsUUFBTCxDQUFjO0FBQUNYLFlBQUksRUFBRTtBQUFQLE9BQWQsRUFUNkIsQ0FVN0I7OztBQUNBMEIsYUFBTyxDQUFDQyxHQUFSLENBQVlkLFNBQVo7O0FBQ0EsV0FBSyxJQUFJZSxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHUixrQkFBa0IsQ0FBQ1IsTUFBdkMsRUFBK0NnQixDQUFDLEVBQWhELEVBQW9EO0FBQ2hELFlBQUlSLGtCQUFrQixDQUFDUSxDQUFELENBQWxCLENBQXNCaEIsTUFBdEIsSUFBZ0MsQ0FBcEMsRUFBc0M7QUFDbEMsY0FBSWlCLE1BQU0sR0FBRyxNQUFLdkIsS0FBTCxDQUFXTCxlQUFYLENBQTJCNkIsTUFBM0IsQ0FBa0M7QUFBQzNCLHlCQUFhLEVBQUUsTUFBS0csS0FBTCxDQUFXSCxhQUEzQjtBQUEwQ2dCLGNBQUUsRUFBRUQsZUFBZSxDQUFDVSxDQUFELENBQTdEO0FBQW1FRyxnQkFBSSxFQUFDakIsa0JBQWtCLENBQUNjLENBQUQsQ0FBMUY7QUFBK0ZJLHdCQUFZLEVBQUNULGdCQUFnQixDQUFDSyxDQUFELENBQTVIO0FBQWlJTixpQkFBSyxFQUFDRixrQkFBa0IsQ0FBQ1EsQ0FBRDtBQUF6SixXQUFsQyxDQUFiOztBQUNBLGdCQUFLakIsUUFBTCxDQUFjO0FBQUVWLDJCQUFlLEVBQUU0QjtBQUFuQixXQUFkOztBQUNBLGdCQUFLbEIsUUFBTCxDQUFjO0FBQUVSLHlCQUFhLEVBQUUsTUFBS0csS0FBTCxDQUFXSCxhQUFYLEdBQTJCO0FBQTVDLFdBQWQ7QUFDSCxTQUpELE1BS0s7QUFDRCxlQUFLLElBQUk4QixDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHYixrQkFBa0IsQ0FBQ1EsQ0FBRCxDQUFsQixDQUFzQmhCLE1BQTFDLEVBQWtEcUIsQ0FBQyxFQUFuRCxFQUF1RDtBQUNuRCxnQkFBSUMsU0FBUyxHQUFHLE1BQUs1QixLQUFMLENBQVdMLGVBQVgsQ0FBMkI2QixNQUEzQixDQUFrQztBQUFDM0IsMkJBQWEsRUFBRSxNQUFLRyxLQUFMLENBQVdILGFBQTNCO0FBQTBDZ0IsZ0JBQUUsRUFBRUQsZUFBZSxDQUFDVSxDQUFELENBQTdEO0FBQW1FRyxrQkFBSSxFQUFDakIsa0JBQWtCLENBQUNjLENBQUQsQ0FBMUY7QUFBK0ZJLDBCQUFZLEVBQUNULGdCQUFnQixDQUFDSyxDQUFELENBQTVIO0FBQWlJTixtQkFBSyxFQUFDRixrQkFBa0IsQ0FBQ1EsQ0FBRCxDQUFsQixDQUFzQkssQ0FBdEI7QUFBdkksYUFBbEMsQ0FBaEI7O0FBQ0Esa0JBQUt0QixRQUFMLENBQWM7QUFBRVYsNkJBQWUsRUFBRWlDO0FBQW5CLGFBQWQ7O0FBQ0Esa0JBQUt2QixRQUFMLENBQWM7QUFBRVIsMkJBQWEsRUFBRSxNQUFLRyxLQUFMLENBQVdILGFBQVgsR0FBMkI7QUFBNUMsYUFBZDtBQUNIO0FBQ0o7QUFDSjtBQUNKLEs7Ozs7Ozs7QUF2R3VGOzZCQUUvRTtBQUFBOztBQUNMO0FBQUE7QUFDQTtBQUNBLHVFQUFDLHFEQUFEO0FBQ0ksbUJBQVMsTUFEYjtBQUVJLGVBQUssRUFBQyxtQkFGVjtBQUdJLHVCQUFhLEVBQUU7QUFDWGdDLG1CQUFPLEVBQUUsZ0JBREU7QUFFWEMsb0JBQVEsRUFBRTtBQUFBLHFCQUFNLE1BQUksQ0FBQ3pCLFFBQUwsQ0FBYztBQUFDWCxvQkFBSSxFQUFFO0FBQVAsZUFBZCxDQUFOO0FBQUE7QUFGQyxXQUhuQjtBQUFBLGtDQVFRLDhEQUFDLHdFQUFjO0FBQUU7QUFBakI7QUFDSSx3QkFBWSxFQUFDLFNBRGpCO0FBRUksZ0JBQUksRUFBRSxLQUFLTSxLQUFMLENBQVdOLElBRnJCO0FBR0ksb0JBQVEsRUFBRztBQUFBLHFCQUFNLE1BQUksQ0FBQ1csUUFBTCxDQUFjO0FBQUNYLG9CQUFJLEVBQUU7QUFBUCxlQUFkLENBQU47QUFBQSxhQUhmO0FBSUksdUJBQVcsRUFBRSxxQkFBQ2EsU0FBRDtBQUFBLHFCQUFlLE1BQUksQ0FBQ3dCLGVBQUwsQ0FBcUJ4QixTQUFyQixDQUFmO0FBQUE7QUFKakIsWUFSUixlQWNRO0FBQUEsc0JBQ0s7QUFDRCxpQkFBS1AsS0FBTCxDQUFXSixpQkFBWCxnQkFDSTtBQUFLLG1CQUFLLEVBQUU7QUFBQ29DLDRCQUFZLEVBQUU7QUFBZixlQUFaO0FBQUEscUNBQ0ksOERBQUMsdURBQUQ7QUFBUSwyQkFBVyxNQUFuQjtBQUNBLHVCQUFPLEVBQUU7QUFBQSx5QkFBTSxNQUFJLENBQUNDLFdBQUwsRUFBTjtBQUFBLGlCQURUO0FBQUE7QUFBQTtBQURKLGNBREosR0FLRTtBQVBOLFlBZFIsZUF1QlEsOERBQUMscURBQUQ7QUFBQSxtQ0FDQSw4REFBQyw2REFBWTtBQUFDO0FBQWQ7QUFDSSwwQkFBWSxFQUFFO0FBQUNDLHdCQUFRLEVBQUUsU0FBWDtBQUFzQkMsc0JBQU0sRUFBRTtBQUE5QixlQURsQjtBQUVJLG1CQUFLLEVBQUcsS0FBS25DLEtBQUwsQ0FBV0wsZUFGdkI7QUFHSSx3QkFBVSxFQUFFLG9CQUFDeUMsSUFBRCxFQUFVO0FBQUEsb0JBQ2Z2QyxhQURlLEdBQ2tDdUMsSUFEbEMsQ0FDZnZDLGFBRGU7QUFBQSxvQkFDQWdCLEVBREEsR0FDa0N1QixJQURsQyxDQUNBdkIsRUFEQTtBQUFBLG9CQUNLWSxJQURMLEdBQ2tDVyxJQURsQyxDQUNLWCxJQURMO0FBQUEsb0JBQ1dDLFlBRFgsR0FDa0NVLElBRGxDLENBQ1dWLFlBRFg7QUFBQSxvQkFDeUJWLEtBRHpCLEdBQ2tDb0IsSUFEbEMsQ0FDeUJwQixLQUR6QixFQUN3Qzs7QUFDOUQsb0NBQ0ksK0RBQUMsNkRBQUQ7QUFDQSxvQkFBRSxFQUFFSCxFQURKO0FBRUEsdUJBQUssZUFBRyw4REFBQyx1REFBRDtBQUFRLDRCQUFRLE1BQWhCO0FBQWlCLHdCQUFJLEVBQUMsUUFBdEI7QUFBK0Isd0JBQUksRUFBRVksSUFBckM7QUFBMkMsMEJBQU0sRUFBRUM7QUFBbkQsb0JBRlI7QUFHQSxvQ0FBa0IsNkJBQXNCRCxJQUF0QixDQUhsQjtBQUFBLDBDQUtBO0FBQUEsNENBQ0ksOERBQUMsMERBQUQ7QUFBVywrQkFBUyxFQUFDLFFBQXJCO0FBQUEsZ0NBQStCQTtBQUEvQixzQkFESixlQUVJO0FBQUE7QUFBQSxzQkFGSjtBQUFBLG9CQUxBLGVBU0E7QUFBQSw4QkFBTVQ7QUFBTixvQkFUQSxlQVVBO0FBQUsseUJBQUssRUFBRTtBQUFDcUIsOEJBQVEsRUFBQyxVQUFWO0FBQXNCQywyQkFBSyxFQUFDLE1BQTVCO0FBQW9DQywrQkFBUyxFQUFFO0FBQS9DLHFCQUFaO0FBQUEsMkNBQ0ksOERBQUMsdURBQUQ7QUFBUSxpQ0FBVyxNQUFuQjtBQUFvQixtQ0FBYSxFQUFFMUMsYUFBbkM7QUFDQSw2QkFBTyxFQUFFLGlCQUFDQyxHQUFEO0FBQUEsK0JBQVMsTUFBSSxDQUFDMEMsWUFBTCxDQUFrQkosSUFBSSxDQUFDdkMsYUFBdkIsQ0FBVDtBQUFBLHVCQURUO0FBQUE7QUFBQTtBQURKLG9CQVZBO0FBQUEsa0JBREo7QUFpQkM7QUF0Qkw7QUFEQSxZQXZCUjtBQUFBO0FBRkE7QUFxREgsSyxDQUVEOzs7OztFQTNEZ0I0QyxnRDs7QUE0R0hoRCxvRUFBZiIsImZpbGUiOiJzdGF0aWMvd2VicGFjay9wYWdlcy9pbmRleC40MzcwNjI3YzgzMzMwZjI5MDlmNi5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLy9JbXBvcnQgUG9sYXJpcyBlbGVtZW50cywgUmVzb3VyY2UgUGlja2VyIGFuZCBSZWFjdFxyXG5pbXBvcnQgeyBQYWdlLCBDYXJkLCBSZXNvdXJjZUxpc3QsIFJlc291cmNlSXRlbSwgQXZhdGFyLCBUZXh0U3R5bGUsIEJ1dHRvbiB9IGZyb20gXCJAc2hvcGlmeS9wb2xhcmlzXCI7XHJcbmltcG9ydCB7IFJlc291cmNlUGlja2VyIH0gZnJvbSBcIkBzaG9waWZ5L2FwcC1icmlkZ2UtcmVhY3RcIjtcclxuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcclxuXHJcbmNsYXNzIEluZGV4IGV4dGVuZHMgQ29tcG9uZW50IHtcclxuICAgIHN0YXRlID0geyBvcGVuOiBmYWxzZSwgcmVzb3VyY2VMaXN0QXJyOiBbXSwgZGVsZXRlQnV0dG9uU2hvd246IGZhbHNlLCByZXNvdXJjZUluZGV4OiAwIH0vL3NldCBzdGF0ZVxyXG5cclxuICAgIHJlbmRlcigpIHsgXHJcbiAgICAgICAgcmV0dXJuICggXHJcbiAgICAgICAgLy9QYWdlIHN0eWxpbmcgdXNpbmcgUG9sYXJpcyBQYWdlIGVsZW1lbnQsIFRleHQgYW4gQnV0dG9uXHJcbiAgICAgICAgPFBhZ2UgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgZnVsbFdpZHRoXHJcbiAgICAgICAgICAgIHRpdGxlPVwiUHJvZHVjdCBTZWxlY3Rpb25cIlxyXG4gICAgICAgICAgICBwcmltYXJ5QWN0aW9uPXt7XHJcbiAgICAgICAgICAgICAgICBjb250ZW50OiAnU2VsZWN0IHByb2R1Y3QnLFxyXG4gICAgICAgICAgICAgICAgb25BY3Rpb246ICgpID0+IHRoaXMuc2V0U3RhdGUoe29wZW46IHRydWV9KVxyXG4gICAgICAgICAgICB9fVxyXG4gICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICA8UmVzb3VyY2VQaWNrZXIgIC8qUmVzb3VyY2UgUGlja2VyLCBjaGFuZ2Ugc3RhdGUgb24gY2xvdGhvbmcgYW5kIGhhbmRsaW5nIGlmIGl0ZW1zIHNlbGVjdGVkKi9cclxuICAgICAgICAgICAgICAgICAgICByZXNvdXJjZVR5cGU9XCJQcm9kdWN0XCJcclxuICAgICAgICAgICAgICAgICAgICBvcGVuPXt0aGlzLnN0YXRlLm9wZW59XHJcbiAgICAgICAgICAgICAgICAgICAgb25DYW5jZWw9eyAoKSA9PiB0aGlzLnNldFN0YXRlKHtvcGVuOiBmYWxzZX0pfVxyXG4gICAgICAgICAgICAgICAgICAgIG9uU2VsZWN0aW9uPXsocmVzb3VyY2VzKSA9PiB0aGlzLmhhbmRsZVNlbGVjdGlvbihyZXNvdXJjZXMpfSBcclxuICAgICAgICAgICAgICAgIC8+IFxyXG4gICAgICAgICAgICAgICAgPGRpdj5cclxuICAgICAgICAgICAgICAgICAgICB7Ly8gRGlzcGxheSBDbGVhcmluZyBCdXR0b24gaWYgYW55IGl0ZW0gaXMgc2VsZWN0ZWRcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLmRlbGV0ZUJ1dHRvblNob3duID8gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgc3R5bGU9e3ttYXJnaW5Cb3R0b206ICcxNXB4J319PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEJ1dHRvbiBkZXN0cnVjdGl2ZSBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHRoaXMuaGFuZGxlQ2xlYXIoKX0+Q2xlYXIgYWxsPC9CdXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PiBcclxuICAgICAgICAgICAgICAgICAgICA6IG51bGx9IFxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8Q2FyZD5cclxuICAgICAgICAgICAgICAgIDxSZXNvdXJjZUxpc3QgLypBZGRpbmcgUmVzb3VyY2UgTGlzdCB3aWNoIGNvbnNpc3RzIG9mIFJlc291cmNlSXRlbXMgKi9cclxuICAgICAgICAgICAgICAgICAgICByZXNvdXJjZU5hbWU9e3tzaW5ndWxhcjogJ3Byb2R1Y3QnLCBwbHVyYWw6ICdwcm9kdWN0J319XHJcbiAgICAgICAgICAgICAgICAgICAgaXRlbXM9eyB0aGlzLnN0YXRlLnJlc291cmNlTGlzdEFyciB9XHJcbiAgICAgICAgICAgICAgICAgICAgcmVuZGVySXRlbT17KGl0ZW0pID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCB7cmVzb3VyY2VJbmRleCwgaWQsICBuYW1lLCBhdmF0YXJTb3VyY2UsIHByaWNlfSA9IGl0ZW07IC8vU2V0dGluZyBJdGVtXHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgICAgICAgICAgICAgPFJlc291cmNlSXRlbVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZD17aWR9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1lZGlhPSB7PEF2YXRhciBjdXN0b21lciBzaXplPVwibWVkaXVtXCIgbmFtZT17bmFtZX0gc291cmNlPXthdmF0YXJTb3VyY2V9IC8+fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhY2Nlc3NpYmlsaXR5TGFiZWw9e2BWaWV3IGRldGFpbHMgZm9yICR7bmFtZX1gfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxoMj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxUZXh0U3R5bGUgdmFyaWF0aW9uPVwic3Ryb25nXCI+e25hbWV9PC9UZXh0U3R5bGU+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8c3Bhbj50ZXh0PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2gyPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2PntwcmljZX08L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBzdHlsZT17e3Bvc2l0aW9uOidhYnNvbHV0ZScsIHJpZ2h0OicyMHB4JywgbWFyZ2luVG9wOiAnLTQwcHgnfX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QnV0dG9uIGRlc3RydWN0aXZlIHJlc291cmNlSW5kZXg9e3Jlc291cmNlSW5kZXh9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoaW5kKSA9PiB0aGlzLmhhbmRsZURlbGV0ZShpdGVtLnJlc291cmNlSW5kZXgpfT5EZWxldGU8L0J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvUmVzb3VyY2VJdGVtPlxyXG4gICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICAgICAgfX1cclxuICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICA8L0NhcmQ+XHJcbiAgICAgICAgPC9QYWdlPiBcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8vSGFuZGxpbmcgRGVsZXRpb24gb2YgZWxlbWVudCBmcm9tIFJlc291cmNlIExpc3RcclxuICAgIGhhbmRsZURlbGV0ZSA9IChpbmQpID0+IHtcclxuICAgICAgICB2YXIgcG9zID0gdGhpcy5zdGF0ZS5yZXNvdXJjZUxpc3RBcnIubWFwKGZ1bmN0aW9uKGUpIHsgcmV0dXJuIGUucmVzb3VyY2VJbmRleDsgfSkuaW5kZXhPZihpbmQpOyAvL0RldGVjdGluZyBwb3NpdGlvbiBvZiBpdGVtIGJ5IGl0YHMgJ3Jlc291cmNlSW5kZXgnIHZhbHVlXHJcbiAgICAgICAgdGhpcy5zdGF0ZS5yZXNvdXJjZUxpc3RBcnIuc3BsaWNlKHBvcywgMSk7IC8vRGVsZXRlIGl0ZW0gZnJvbSBwcm9kdWN0cyBhcnJheVxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyByZXNvdXJjZUxpc3RBcnI6ICB0aGlzLnN0YXRlLnJlc291cmNlTGlzdEFyciB9KTsgLy9VcGRhdGluZyBzdGF0ZVxyXG4gICAgICAgIC8vSW4gY2FzZSBpZiBhbGwgb2JqZWN0cyByZW1vdmVkLCBoaWRlICdDbGVhciBBbGwnIGJ1dHRvblxyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnJlc291cmNlTGlzdEFyci5sZW5ndGggPT0gMCkge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtkZWxldGVCdXR0b25TaG93bjogZmFsc2V9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy9IYW5kbGluZyAnQ2xlYXIgQWxsJyBidXR0b24gYWN0aW9uXHJcbiAgICBoYW5kbGVDbGVhciA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VMaXN0QXJyOiBbXSB9KTsgLy9SZW1vdmUgYWxsIGl0ZW1zIGZyb20gcHJvZHVjdGBzIGFycmF5XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7ZGVsZXRlQnV0dG9uU2hvd246IGZhbHNlfSk7Ly9IaWRlICdDbGVhciBBbGwnIGJ1dHRvblxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyByZXNvdXJjZUluZGV4OiAwIH0pOy8vU2V0ICdSZXNvdXJjZUluZGV4JyBjb3VudGVyIHRvIDBcclxuICAgIH1cclxuXHJcbiAgICAvL0hhbmRsaW5nIGl0ZW1zIHNlbGVjdGlvbiBmcm9tIHJlc291cmNlIHBpY2tlclxyXG4gICAgaGFuZGxlU2VsZWN0aW9uID0gKHJlc291cmNlcykgPT4ge1xyXG4gICAgICAgIC8vbWFwcGluZyBuZXcgYXJyYXlzIHdpdGggbmVlZGVkIHByb2R1Y3RzIHZhbHVlc1xyXG4gICAgICAgIGNvbnN0IHRpdGxlRnJvbVJlc291cmNlcyA9IHJlc291cmNlcy5zZWxlY3Rpb24ubWFwKChwcm9kdWN0KSA9PiBwcm9kdWN0LnRpdGxlKTtcclxuICAgICAgICBjb25zdCBpZEZyb21SZXNvdXJjZXMgPSByZXNvdXJjZXMuc2VsZWN0aW9uLm1hcCgocHJvZHVjdCkgPT4gcHJvZHVjdC5pZCk7XHJcbiAgICAgICAgY29uc3QgcHJpY2VGcm9tUmVzb3VyY2VzID0gcmVzb3VyY2VzLnNlbGVjdGlvbi5tYXAoKHByb2R1Y3QpID0+IHByb2R1Y3QudmFyaWFudHMubWFwKGZ1bmN0aW9uKGUpIHsgcmV0dXJuIGUucHJpY2U7IH0pKTtcclxuICAgICAgICBjb25zdCBpbWdGcm9tUmVzb3VyY2VzID0gcmVzb3VyY2VzLnNlbGVjdGlvbi5tYXAoKHByb2R1Y3QpID0+IHByb2R1Y3QuaW1hZ2VzWzBdLm9yaWdpbmFsU3JjKTtcclxuICAgICAgICAvL1Nob3cgJ0NsZWFyIEFsbCcgYnV0dG9uXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7ZGVsZXRlQnV0dG9uU2hvd246IHRydWV9KTtcclxuICAgICAgICAvL0Nsb3NlIFJlc291cmNlIFBpY2tlclxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe29wZW46IGZhbHNlfSk7XHJcbiAgICAgICAgLy9BZGRpbmcgcHJvZHVjdHMgdG8gcHJvZHVjdGBzIGFycmF5XHJcbiAgICAgICAgY29uc29sZS5sb2cocmVzb3VyY2VzKTtcclxuICAgICAgICBmb3IgKGxldCB4ID0gMDsgeCA8IHByaWNlRnJvbVJlc291cmNlcy5sZW5ndGg7IHgrKykge1xyXG4gICAgICAgICAgICBpZiAocHJpY2VGcm9tUmVzb3VyY2VzW3hdLmxlbmd0aCA9PSAxKXtcclxuICAgICAgICAgICAgICAgIGxldCBqb2luZWQgPSB0aGlzLnN0YXRlLnJlc291cmNlTGlzdEFyci5jb25jYXQoe3Jlc291cmNlSW5kZXg6IHRoaXMuc3RhdGUucmVzb3VyY2VJbmRleCwgaWQ6IGlkRnJvbVJlc291cmNlc1t4XSwgIG5hbWU6dGl0bGVGcm9tUmVzb3VyY2VzW3hdLCBhdmF0YXJTb3VyY2U6aW1nRnJvbVJlc291cmNlc1t4XSwgcHJpY2U6cHJpY2VGcm9tUmVzb3VyY2VzW3hdfSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VMaXN0QXJyOiBqb2luZWQgfSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VJbmRleDogdGhpcy5zdGF0ZS5yZXNvdXJjZUluZGV4ICsgMSB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGZvciAobGV0IHkgPSAwOyB5IDwgcHJpY2VGcm9tUmVzb3VyY2VzW3hdLmxlbmd0aDsgeSsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IG5ld0pvaW5lZCA9IHRoaXMuc3RhdGUucmVzb3VyY2VMaXN0QXJyLmNvbmNhdCh7cmVzb3VyY2VJbmRleDogdGhpcy5zdGF0ZS5yZXNvdXJjZUluZGV4LCBpZDogaWRGcm9tUmVzb3VyY2VzW3hdLCAgbmFtZTp0aXRsZUZyb21SZXNvdXJjZXNbeF0sIGF2YXRhclNvdXJjZTppbWdGcm9tUmVzb3VyY2VzW3hdLCBwcmljZTpwcmljZUZyb21SZXNvdXJjZXNbeF1beV19KTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VMaXN0QXJyOiBuZXdKb2luZWQgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHJlc291cmNlSW5kZXg6IHRoaXMuc3RhdGUucmVzb3VyY2VJbmRleCArIDEgfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59XHJcbiAgXHJcbiAgZXhwb3J0IGRlZmF1bHQgSW5kZXg7Il0sInNvdXJjZVJvb3QiOiIifQ==