webpackHotUpdate_N_E("pages/index",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/esm/createClass */ "./node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inherits */ "./node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _shopify_polaris__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @shopify/polaris */ "./node_modules/@shopify/polaris/dist/esm/index.js");
/* harmony import */ var _shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @shopify/app-bridge-react */ "./node_modules/@shopify/app-bridge-react/index.js");
/* harmony import */ var _shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_10__);










function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return Object(_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

//Import Polaris elements, Resource Picker and React




var Index = /*#__PURE__*/function (_Component) {
  Object(_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__["default"])(Index, _Component);

  var _super = _createSuper(Index);

  function Index() {
    var _this;

    Object(_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, Index);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "state", {
      open: false,
      resourceListArr: [],
      deleteButtonShown: false,
      resourceIndex: 0
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleDelete", function (ind) {
      var pos = _this.state.resourceListArr.map(function (e) {
        return e.resourceIndex;
      }).indexOf(ind); //Detecting position of item by it`s 'resourceIndex' value


      _this.state.resourceListArr.splice(pos, 1); //Delete item from products array


      _this.setState({
        resourceListArr: _this.state.resourceListArr
      }); //Updating state
      //In case if all objects removed, hide 'Clear All' button


      if (_this.state.resourceListArr.length == 0) {
        _this.setState({
          deleteButtonShown: false
        });
      }
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleClear", function () {
      _this.setState({
        resourceListArr: []
      }); //Remove all items from product`s array


      _this.setState({
        deleteButtonShown: false
      }); //Hide 'Clear All' button


      _this.setState({
        resourceIndex: 0
      }); //Set 'ResourceIndex' counter to 0

    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleSelection", function (resources) {
      //mapping new arrays with needed products values
      var titleFromResources = resources.selection.map(function (product) {
        return product.title;
      });
      var idFromResources = resources.selection.map(function (product) {
        return product.id;
      });
      var priceFromResources = resources.selection.map(function (product) {
        return product.variants.map(function (e) {
          return e.price;
        });
      });
      var variantFromResources = resources.selection.map(function (product) {
        return product.variants.map(function (e) {
          return e.title;
        });
      });
      var imgFromResources = resources.selection.map(function (product) {
        return product.images[0].originalSrc;
      }); //Show 'Clear All' button

      _this.setState({
        deleteButtonShown: true
      }); //Close Resource Picker


      _this.setState({
        open: false
      }); //Adding products to product`s array


      console.log(resources);

      for (var x = 0; x < priceFromResources.length; x++) {
        if (priceFromResources[x].length == 1) {
          var joined = _this.state.resourceListArr.concat({
            resourceIndex: _this.state.resourceIndex,
            id: idFromResources[x],
            name: titleFromResources[x],
            avatarSource: imgFromResources[x],
            price: priceFromResources[x],
            variant: variantFromResources[x] == 'Title' ? 'true' : 'false'
          });

          _this.setState({
            resourceListArr: joined
          });

          _this.setState({
            resourceIndex: _this.state.resourceIndex + 1
          });
        } else {
          for (var y = 0; y < priceFromResources[x].length; y++) {
            var newJoined = _this.state.resourceListArr.concat({
              resourceIndex: _this.state.resourceIndex,
              id: idFromResources[x],
              name: titleFromResources[x],
              avatarSource: imgFromResources[x],
              price: priceFromResources[x][y],
              variant: variantFromResources[x][y]
            });

            _this.setState({
              resourceListArr: newJoined
            });

            _this.setState({
              resourceIndex: _this.state.resourceIndex + 1
            });
          }
        }
      }
    });

    return _this;
  }

  Object(_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(Index, [{
    key: "render",
    //set state
    value: function render() {
      var _this2 = this;

      return (
        /*#__PURE__*/
        //Page styling using Polaris Page element, Text an Button
        Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxs"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Page"], {
          fullWidth: true,
          title: "Product Selection",
          primaryAction: {
            content: 'Select product',
            onAction: function onAction() {
              return _this2.setState({
                open: true
              });
            }
          },
          children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9__["ResourcePicker"]
          /*Resource Picker, change state on clothong and handling if items selected*/
          , {
            resourceType: "Product",
            open: this.state.open,
            onCancel: function onCancel() {
              return _this2.setState({
                open: false
              });
            },
            onSelection: function onSelection(resources) {
              return _this2.handleSelection(resources);
            }
          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
            children: // Display Clearing Button if any item is selected
            this.state.deleteButtonShown ? /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
              style: {
                marginBottom: '15px'
              },
              children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Button"], {
                destructive: true,
                onClick: function onClick() {
                  return _this2.handleClear();
                },
                children: "Clear all"
              })
            }) : null
          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Card"], {
            children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["ResourceList"]
            /*Adding Resource List wich consists of ResourceItems */
            , {
              resourceName: {
                singular: 'product',
                plural: 'product'
              },
              items: this.state.resourceListArr,
              renderItem: function renderItem(item) {
                var resourceIndex = item.resourceIndex,
                    id = item.id,
                    name = item.name,
                    avatarSource = item.avatarSource,
                    price = item.price,
                    variant = item.variant; //Setting Item

                return /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxs"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["ResourceItem"], {
                  id: id,
                  media: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Avatar"], {
                    customer: true,
                    size: "medium",
                    name: name,
                    source: avatarSource
                  }),
                  accessibilityLabel: "View details for ".concat(name),
                  children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxs"])("h2", {
                    children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["TextStyle"], {
                      variation: "strong",
                      children: name + ' '
                    }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["TextStyle"], {
                      variation: "strong",
                      children: ' ' + variant
                    })]
                  }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
                    children: price
                  }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
                    style: {
                      position: 'absolute',
                      right: '20px',
                      marginTop: '-40px'
                    },
                    children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Button"], {
                      destructive: true,
                      resourceIndex: resourceIndex,
                      onClick: function onClick(ind) {
                        return _this2.handleDelete(item.resourceIndex);
                      },
                      children: "Delete"
                    })
                  })]
                });
              }
            })
          })]
        })
      );
    } //Handling Deletion of element from Resource List

  }]);

  return Index;
}(react__WEBPACK_IMPORTED_MODULE_10__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Index);

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvaW5kZXguanMiXSwibmFtZXMiOlsiSW5kZXgiLCJvcGVuIiwicmVzb3VyY2VMaXN0QXJyIiwiZGVsZXRlQnV0dG9uU2hvd24iLCJyZXNvdXJjZUluZGV4IiwiaW5kIiwicG9zIiwic3RhdGUiLCJtYXAiLCJlIiwiaW5kZXhPZiIsInNwbGljZSIsInNldFN0YXRlIiwibGVuZ3RoIiwicmVzb3VyY2VzIiwidGl0bGVGcm9tUmVzb3VyY2VzIiwic2VsZWN0aW9uIiwicHJvZHVjdCIsInRpdGxlIiwiaWRGcm9tUmVzb3VyY2VzIiwiaWQiLCJwcmljZUZyb21SZXNvdXJjZXMiLCJ2YXJpYW50cyIsInByaWNlIiwidmFyaWFudEZyb21SZXNvdXJjZXMiLCJpbWdGcm9tUmVzb3VyY2VzIiwiaW1hZ2VzIiwib3JpZ2luYWxTcmMiLCJjb25zb2xlIiwibG9nIiwieCIsImpvaW5lZCIsImNvbmNhdCIsIm5hbWUiLCJhdmF0YXJTb3VyY2UiLCJ2YXJpYW50IiwieSIsIm5ld0pvaW5lZCIsImNvbnRlbnQiLCJvbkFjdGlvbiIsImhhbmRsZVNlbGVjdGlvbiIsIm1hcmdpbkJvdHRvbSIsImhhbmRsZUNsZWFyIiwic2luZ3VsYXIiLCJwbHVyYWwiLCJpdGVtIiwicG9zaXRpb24iLCJyaWdodCIsIm1hcmdpblRvcCIsImhhbmRsZURlbGV0ZSIsIkNvbXBvbmVudCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7O0lBRU1BLEs7Ozs7Ozs7Ozs7Ozs7Ozs7Z05BQ007QUFBRUMsVUFBSSxFQUFFLEtBQVI7QUFBZUMscUJBQWUsRUFBRSxFQUFoQztBQUFvQ0MsdUJBQWlCLEVBQUUsS0FBdkQ7QUFBOERDLG1CQUFhLEVBQUU7QUFBN0UsSzs7dU5BMkRPLFVBQUNDLEdBQUQsRUFBUztBQUNwQixVQUFJQyxHQUFHLEdBQUcsTUFBS0MsS0FBTCxDQUFXTCxlQUFYLENBQTJCTSxHQUEzQixDQUErQixVQUFTQyxDQUFULEVBQVk7QUFBRSxlQUFPQSxDQUFDLENBQUNMLGFBQVQ7QUFBeUIsT0FBdEUsRUFBd0VNLE9BQXhFLENBQWdGTCxHQUFoRixDQUFWLENBRG9CLENBQzRFOzs7QUFDaEcsWUFBS0UsS0FBTCxDQUFXTCxlQUFYLENBQTJCUyxNQUEzQixDQUFrQ0wsR0FBbEMsRUFBdUMsQ0FBdkMsRUFGb0IsQ0FFdUI7OztBQUMzQyxZQUFLTSxRQUFMLENBQWM7QUFBRVYsdUJBQWUsRUFBRyxNQUFLSyxLQUFMLENBQVdMO0FBQS9CLE9BQWQsRUFIb0IsQ0FHNkM7QUFDakU7OztBQUNBLFVBQUksTUFBS0ssS0FBTCxDQUFXTCxlQUFYLENBQTJCVyxNQUEzQixJQUFxQyxDQUF6QyxFQUE0QztBQUN4QyxjQUFLRCxRQUFMLENBQWM7QUFBQ1QsMkJBQWlCLEVBQUU7QUFBcEIsU0FBZDtBQUNIO0FBQ0osSzs7c05BR2EsWUFBTTtBQUNoQixZQUFLUyxRQUFMLENBQWM7QUFBRVYsdUJBQWUsRUFBRTtBQUFuQixPQUFkLEVBRGdCLENBQ3dCOzs7QUFDeEMsWUFBS1UsUUFBTCxDQUFjO0FBQUNULHlCQUFpQixFQUFFO0FBQXBCLE9BQWQsRUFGZ0IsQ0FFMEI7OztBQUMxQyxZQUFLUyxRQUFMLENBQWM7QUFBRVIscUJBQWEsRUFBRTtBQUFqQixPQUFkLEVBSGdCLENBR29COztBQUN2QyxLOzswTkFHaUIsVUFBQ1UsU0FBRCxFQUFlO0FBQzdCO0FBQ0EsVUFBTUMsa0JBQWtCLEdBQUdELFNBQVMsQ0FBQ0UsU0FBVixDQUFvQlIsR0FBcEIsQ0FBd0IsVUFBQ1MsT0FBRDtBQUFBLGVBQWFBLE9BQU8sQ0FBQ0MsS0FBckI7QUFBQSxPQUF4QixDQUEzQjtBQUNBLFVBQU1DLGVBQWUsR0FBR0wsU0FBUyxDQUFDRSxTQUFWLENBQW9CUixHQUFwQixDQUF3QixVQUFDUyxPQUFEO0FBQUEsZUFBYUEsT0FBTyxDQUFDRyxFQUFyQjtBQUFBLE9BQXhCLENBQXhCO0FBQ0EsVUFBTUMsa0JBQWtCLEdBQUdQLFNBQVMsQ0FBQ0UsU0FBVixDQUFvQlIsR0FBcEIsQ0FBd0IsVUFBQ1MsT0FBRDtBQUFBLGVBQWFBLE9BQU8sQ0FBQ0ssUUFBUixDQUFpQmQsR0FBakIsQ0FBcUIsVUFBU0MsQ0FBVCxFQUFZO0FBQUUsaUJBQU9BLENBQUMsQ0FBQ2MsS0FBVDtBQUFpQixTQUFwRCxDQUFiO0FBQUEsT0FBeEIsQ0FBM0I7QUFDQSxVQUFNQyxvQkFBb0IsR0FBR1YsU0FBUyxDQUFDRSxTQUFWLENBQW9CUixHQUFwQixDQUF3QixVQUFDUyxPQUFEO0FBQUEsZUFBYUEsT0FBTyxDQUFDSyxRQUFSLENBQWlCZCxHQUFqQixDQUFxQixVQUFTQyxDQUFULEVBQVk7QUFBRSxpQkFBT0EsQ0FBQyxDQUFDUyxLQUFUO0FBQWlCLFNBQXBELENBQWI7QUFBQSxPQUF4QixDQUE3QjtBQUNBLFVBQU1PLGdCQUFnQixHQUFHWCxTQUFTLENBQUNFLFNBQVYsQ0FBb0JSLEdBQXBCLENBQXdCLFVBQUNTLE9BQUQ7QUFBQSxlQUFhQSxPQUFPLENBQUNTLE1BQVIsQ0FBZSxDQUFmLEVBQWtCQyxXQUEvQjtBQUFBLE9BQXhCLENBQXpCLENBTjZCLENBTzdCOztBQUNBLFlBQUtmLFFBQUwsQ0FBYztBQUFDVCx5QkFBaUIsRUFBRTtBQUFwQixPQUFkLEVBUjZCLENBUzdCOzs7QUFDQSxZQUFLUyxRQUFMLENBQWM7QUFBQ1gsWUFBSSxFQUFFO0FBQVAsT0FBZCxFQVY2QixDQVc3Qjs7O0FBQ0EyQixhQUFPLENBQUNDLEdBQVIsQ0FBWWYsU0FBWjs7QUFDQSxXQUFLLElBQUlnQixDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHVCxrQkFBa0IsQ0FBQ1IsTUFBdkMsRUFBK0NpQixDQUFDLEVBQWhELEVBQW9EO0FBQ2hELFlBQUlULGtCQUFrQixDQUFDUyxDQUFELENBQWxCLENBQXNCakIsTUFBdEIsSUFBZ0MsQ0FBcEMsRUFBc0M7QUFDbEMsY0FBSWtCLE1BQU0sR0FBRyxNQUFLeEIsS0FBTCxDQUFXTCxlQUFYLENBQTJCOEIsTUFBM0IsQ0FBa0M7QUFBQzVCLHlCQUFhLEVBQUUsTUFBS0csS0FBTCxDQUFXSCxhQUEzQjtBQUEwQ2dCLGNBQUUsRUFBRUQsZUFBZSxDQUFDVyxDQUFELENBQTdEO0FBQW1FRyxnQkFBSSxFQUFDbEIsa0JBQWtCLENBQUNlLENBQUQsQ0FBMUY7QUFBK0ZJLHdCQUFZLEVBQUNULGdCQUFnQixDQUFDSyxDQUFELENBQTVIO0FBQWlJUCxpQkFBSyxFQUFDRixrQkFBa0IsQ0FBQ1MsQ0FBRCxDQUF6SjtBQUE4SkssbUJBQU8sRUFBRVgsb0JBQW9CLENBQUNNLENBQUQsQ0FBcEIsSUFBMkIsT0FBM0IsR0FBcUMsTUFBckMsR0FBOEM7QUFBck4sV0FBbEMsQ0FBYjs7QUFDQSxnQkFBS2xCLFFBQUwsQ0FBYztBQUFFViwyQkFBZSxFQUFFNkI7QUFBbkIsV0FBZDs7QUFDQSxnQkFBS25CLFFBQUwsQ0FBYztBQUFFUix5QkFBYSxFQUFFLE1BQUtHLEtBQUwsQ0FBV0gsYUFBWCxHQUEyQjtBQUE1QyxXQUFkO0FBQ0gsU0FKRCxNQUtLO0FBQ0QsZUFBSyxJQUFJZ0MsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR2Ysa0JBQWtCLENBQUNTLENBQUQsQ0FBbEIsQ0FBc0JqQixNQUExQyxFQUFrRHVCLENBQUMsRUFBbkQsRUFBdUQ7QUFDbkQsZ0JBQUlDLFNBQVMsR0FBRyxNQUFLOUIsS0FBTCxDQUFXTCxlQUFYLENBQTJCOEIsTUFBM0IsQ0FBa0M7QUFBQzVCLDJCQUFhLEVBQUUsTUFBS0csS0FBTCxDQUFXSCxhQUEzQjtBQUEwQ2dCLGdCQUFFLEVBQUVELGVBQWUsQ0FBQ1csQ0FBRCxDQUE3RDtBQUFtRUcsa0JBQUksRUFBQ2xCLGtCQUFrQixDQUFDZSxDQUFELENBQTFGO0FBQStGSSwwQkFBWSxFQUFDVCxnQkFBZ0IsQ0FBQ0ssQ0FBRCxDQUE1SDtBQUFpSVAsbUJBQUssRUFBQ0Ysa0JBQWtCLENBQUNTLENBQUQsQ0FBbEIsQ0FBc0JNLENBQXRCLENBQXZJO0FBQWlLRCxxQkFBTyxFQUFFWCxvQkFBb0IsQ0FBQ00sQ0FBRCxDQUFwQixDQUF3Qk0sQ0FBeEI7QUFBMUssYUFBbEMsQ0FBaEI7O0FBQ0Esa0JBQUt4QixRQUFMLENBQWM7QUFBRVYsNkJBQWUsRUFBRW1DO0FBQW5CLGFBQWQ7O0FBQ0Esa0JBQUt6QixRQUFMLENBQWM7QUFBRVIsMkJBQWEsRUFBRSxNQUFLRyxLQUFMLENBQVdILGFBQVgsR0FBMkI7QUFBNUMsYUFBZDtBQUNIO0FBQ0o7QUFDSjtBQUNKLEs7Ozs7Ozs7QUF4R3VGOzZCQUUvRTtBQUFBOztBQUNMO0FBQUE7QUFDQTtBQUNBLHVFQUFDLHFEQUFEO0FBQ0ksbUJBQVMsTUFEYjtBQUVJLGVBQUssRUFBQyxtQkFGVjtBQUdJLHVCQUFhLEVBQUU7QUFDWGtDLG1CQUFPLEVBQUUsZ0JBREU7QUFFWEMsb0JBQVEsRUFBRTtBQUFBLHFCQUFNLE1BQUksQ0FBQzNCLFFBQUwsQ0FBYztBQUFDWCxvQkFBSSxFQUFFO0FBQVAsZUFBZCxDQUFOO0FBQUE7QUFGQyxXQUhuQjtBQUFBLGtDQVFRLDhEQUFDLHdFQUFjO0FBQUU7QUFBakI7QUFDSSx3QkFBWSxFQUFDLFNBRGpCO0FBRUksZ0JBQUksRUFBRSxLQUFLTSxLQUFMLENBQVdOLElBRnJCO0FBR0ksb0JBQVEsRUFBRztBQUFBLHFCQUFNLE1BQUksQ0FBQ1csUUFBTCxDQUFjO0FBQUNYLG9CQUFJLEVBQUU7QUFBUCxlQUFkLENBQU47QUFBQSxhQUhmO0FBSUksdUJBQVcsRUFBRSxxQkFBQ2EsU0FBRDtBQUFBLHFCQUFlLE1BQUksQ0FBQzBCLGVBQUwsQ0FBcUIxQixTQUFyQixDQUFmO0FBQUE7QUFKakIsWUFSUixlQWNRO0FBQUEsc0JBQ0s7QUFDRCxpQkFBS1AsS0FBTCxDQUFXSixpQkFBWCxnQkFDSTtBQUFLLG1CQUFLLEVBQUU7QUFBQ3NDLDRCQUFZLEVBQUU7QUFBZixlQUFaO0FBQUEscUNBQ0ksOERBQUMsdURBQUQ7QUFBUSwyQkFBVyxNQUFuQjtBQUNBLHVCQUFPLEVBQUU7QUFBQSx5QkFBTSxNQUFJLENBQUNDLFdBQUwsRUFBTjtBQUFBLGlCQURUO0FBQUE7QUFBQTtBQURKLGNBREosR0FLRTtBQVBOLFlBZFIsZUF1QlEsOERBQUMscURBQUQ7QUFBQSxtQ0FDQSw4REFBQyw2REFBWTtBQUFDO0FBQWQ7QUFDSSwwQkFBWSxFQUFFO0FBQUNDLHdCQUFRLEVBQUUsU0FBWDtBQUFzQkMsc0JBQU0sRUFBRTtBQUE5QixlQURsQjtBQUVJLG1CQUFLLEVBQUcsS0FBS3JDLEtBQUwsQ0FBV0wsZUFGdkI7QUFHSSx3QkFBVSxFQUFFLG9CQUFDMkMsSUFBRCxFQUFVO0FBQUEsb0JBQ2Z6QyxhQURlLEdBQzJDeUMsSUFEM0MsQ0FDZnpDLGFBRGU7QUFBQSxvQkFDQWdCLEVBREEsR0FDMkN5QixJQUQzQyxDQUNBekIsRUFEQTtBQUFBLG9CQUNLYSxJQURMLEdBQzJDWSxJQUQzQyxDQUNLWixJQURMO0FBQUEsb0JBQ1dDLFlBRFgsR0FDMkNXLElBRDNDLENBQ1dYLFlBRFg7QUFBQSxvQkFDeUJYLEtBRHpCLEdBQzJDc0IsSUFEM0MsQ0FDeUJ0QixLQUR6QjtBQUFBLG9CQUNnQ1ksT0FEaEMsR0FDMkNVLElBRDNDLENBQ2dDVixPQURoQyxFQUNpRDs7QUFDdkUsb0NBQ0ksK0RBQUMsNkRBQUQ7QUFDQSxvQkFBRSxFQUFFZixFQURKO0FBRUEsdUJBQUssZUFBRyw4REFBQyx1REFBRDtBQUFRLDRCQUFRLE1BQWhCO0FBQWlCLHdCQUFJLEVBQUMsUUFBdEI7QUFBK0Isd0JBQUksRUFBRWEsSUFBckM7QUFBMkMsMEJBQU0sRUFBRUM7QUFBbkQsb0JBRlI7QUFHQSxvQ0FBa0IsNkJBQXNCRCxJQUF0QixDQUhsQjtBQUFBLDBDQUtBO0FBQUEsNENBQ0ksOERBQUMsMERBQUQ7QUFBVywrQkFBUyxFQUFDLFFBQXJCO0FBQUEsZ0NBQStCQSxJQUFJLEdBQUc7QUFBdEMsc0JBREosZUFFSSw4REFBQywwREFBRDtBQUFXLCtCQUFTLEVBQUMsUUFBckI7QUFBQSxnQ0FBK0IsTUFBTUU7QUFBckMsc0JBRko7QUFBQSxvQkFMQSxlQVNBO0FBQUEsOEJBQU1aO0FBQU4sb0JBVEEsZUFVQTtBQUFLLHlCQUFLLEVBQUU7QUFBQ3VCLDhCQUFRLEVBQUMsVUFBVjtBQUFzQkMsMkJBQUssRUFBQyxNQUE1QjtBQUFvQ0MsK0JBQVMsRUFBRTtBQUEvQyxxQkFBWjtBQUFBLDJDQUNJLDhEQUFDLHVEQUFEO0FBQVEsaUNBQVcsTUFBbkI7QUFBb0IsbUNBQWEsRUFBRTVDLGFBQW5DO0FBQ0EsNkJBQU8sRUFBRSxpQkFBQ0MsR0FBRDtBQUFBLCtCQUFTLE1BQUksQ0FBQzRDLFlBQUwsQ0FBa0JKLElBQUksQ0FBQ3pDLGFBQXZCLENBQVQ7QUFBQSx1QkFEVDtBQUFBO0FBQUE7QUFESixvQkFWQTtBQUFBLGtCQURKO0FBaUJDO0FBdEJMO0FBREEsWUF2QlI7QUFBQTtBQUZBO0FBcURILEssQ0FFRDs7Ozs7RUEzRGdCOEMsZ0Q7O0FBNkdIbEQsb0VBQWYiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvaW5kZXguNWY4N2JiODA3Y2QxZThjOTM0ZmUuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8vSW1wb3J0IFBvbGFyaXMgZWxlbWVudHMsIFJlc291cmNlIFBpY2tlciBhbmQgUmVhY3RcclxuaW1wb3J0IHsgUGFnZSwgQ2FyZCwgUmVzb3VyY2VMaXN0LCBSZXNvdXJjZUl0ZW0sIEF2YXRhciwgVGV4dFN0eWxlLCBCdXR0b24gfSBmcm9tIFwiQHNob3BpZnkvcG9sYXJpc1wiO1xyXG5pbXBvcnQgeyBSZXNvdXJjZVBpY2tlciB9IGZyb20gXCJAc2hvcGlmeS9hcHAtYnJpZGdlLXJlYWN0XCI7XHJcbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XHJcblxyXG5jbGFzcyBJbmRleCBleHRlbmRzIENvbXBvbmVudCB7XHJcbiAgICBzdGF0ZSA9IHsgb3BlbjogZmFsc2UsIHJlc291cmNlTGlzdEFycjogW10sIGRlbGV0ZUJ1dHRvblNob3duOiBmYWxzZSwgcmVzb3VyY2VJbmRleDogMCB9Ly9zZXQgc3RhdGVcclxuXHJcbiAgICByZW5kZXIoKSB7IFxyXG4gICAgICAgIHJldHVybiAoIFxyXG4gICAgICAgIC8vUGFnZSBzdHlsaW5nIHVzaW5nIFBvbGFyaXMgUGFnZSBlbGVtZW50LCBUZXh0IGFuIEJ1dHRvblxyXG4gICAgICAgIDxQYWdlICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGZ1bGxXaWR0aFxyXG4gICAgICAgICAgICB0aXRsZT1cIlByb2R1Y3QgU2VsZWN0aW9uXCJcclxuICAgICAgICAgICAgcHJpbWFyeUFjdGlvbj17e1xyXG4gICAgICAgICAgICAgICAgY29udGVudDogJ1NlbGVjdCBwcm9kdWN0JyxcclxuICAgICAgICAgICAgICAgIG9uQWN0aW9uOiAoKSA9PiB0aGlzLnNldFN0YXRlKHtvcGVuOiB0cnVlfSlcclxuICAgICAgICAgICAgfX1cclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgPFJlc291cmNlUGlja2VyICAvKlJlc291cmNlIFBpY2tlciwgY2hhbmdlIHN0YXRlIG9uIGNsb3Rob25nIGFuZCBoYW5kbGluZyBpZiBpdGVtcyBzZWxlY3RlZCovXHJcbiAgICAgICAgICAgICAgICAgICAgcmVzb3VyY2VUeXBlPVwiUHJvZHVjdFwiXHJcbiAgICAgICAgICAgICAgICAgICAgb3Blbj17dGhpcy5zdGF0ZS5vcGVufVxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2FuY2VsPXsgKCkgPT4gdGhpcy5zZXRTdGF0ZSh7b3BlbjogZmFsc2V9KX1cclxuICAgICAgICAgICAgICAgICAgICBvblNlbGVjdGlvbj17KHJlc291cmNlcykgPT4gdGhpcy5oYW5kbGVTZWxlY3Rpb24ocmVzb3VyY2VzKX0gXHJcbiAgICAgICAgICAgICAgICAvPiBcclxuICAgICAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgey8vIERpc3BsYXkgQ2xlYXJpbmcgQnV0dG9uIGlmIGFueSBpdGVtIGlzIHNlbGVjdGVkXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5kZWxldGVCdXR0b25TaG93biA/IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IHN0eWxlPXt7bWFyZ2luQm90dG9tOiAnMTVweCd9fT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxCdXR0b24gZGVzdHJ1Y3RpdmUgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB0aGlzLmhhbmRsZUNsZWFyKCl9PkNsZWFyIGFsbDwvQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj4gXHJcbiAgICAgICAgICAgICAgICAgICAgOiBudWxsfSBcclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPENhcmQ+XHJcbiAgICAgICAgICAgICAgICA8UmVzb3VyY2VMaXN0IC8qQWRkaW5nIFJlc291cmNlIExpc3Qgd2ljaCBjb25zaXN0cyBvZiBSZXNvdXJjZUl0ZW1zICovXHJcbiAgICAgICAgICAgICAgICAgICAgcmVzb3VyY2VOYW1lPXt7c2luZ3VsYXI6ICdwcm9kdWN0JywgcGx1cmFsOiAncHJvZHVjdCd9fVxyXG4gICAgICAgICAgICAgICAgICAgIGl0ZW1zPXsgdGhpcy5zdGF0ZS5yZXNvdXJjZUxpc3RBcnIgfVxyXG4gICAgICAgICAgICAgICAgICAgIHJlbmRlckl0ZW09eyhpdGVtKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qge3Jlc291cmNlSW5kZXgsIGlkLCAgbmFtZSwgYXZhdGFyU291cmNlLCBwcmljZSwgdmFyaWFudH0gPSBpdGVtOyAvL1NldHRpbmcgSXRlbVxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxSZXNvdXJjZUl0ZW1cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ9e2lkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtZWRpYT0gezxBdmF0YXIgY3VzdG9tZXIgc2l6ZT1cIm1lZGl1bVwiIG5hbWU9e25hbWV9IHNvdXJjZT17YXZhdGFyU291cmNlfSAvPn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgYWNjZXNzaWJpbGl0eUxhYmVsPXtgVmlldyBkZXRhaWxzIGZvciAke25hbWV9YH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8aDI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8VGV4dFN0eWxlIHZhcmlhdGlvbj1cInN0cm9uZ1wiPntuYW1lICsgJyAnfTwvVGV4dFN0eWxlPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPFRleHRTdHlsZSB2YXJpYXRpb249XCJzdHJvbmdcIj57JyAnICsgdmFyaWFudH08L1RleHRTdHlsZT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9oMj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdj57cHJpY2V9PC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgc3R5bGU9e3twb3NpdGlvbjonYWJzb2x1dGUnLCByaWdodDonMjBweCcsIG1hcmdpblRvcDogJy00MHB4J319PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEJ1dHRvbiBkZXN0cnVjdGl2ZSByZXNvdXJjZUluZGV4PXtyZXNvdXJjZUluZGV4fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KGluZCkgPT4gdGhpcy5oYW5kbGVEZWxldGUoaXRlbS5yZXNvdXJjZUluZGV4KX0+RGVsZXRlPC9CdXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L1Jlc291cmNlSXRlbT5cclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgICAgIH19XHJcbiAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgPC9DYXJkPlxyXG4gICAgICAgIDwvUGFnZT4gXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvL0hhbmRsaW5nIERlbGV0aW9uIG9mIGVsZW1lbnQgZnJvbSBSZXNvdXJjZSBMaXN0XHJcbiAgICBoYW5kbGVEZWxldGUgPSAoaW5kKSA9PiB7XHJcbiAgICAgICAgdmFyIHBvcyA9IHRoaXMuc3RhdGUucmVzb3VyY2VMaXN0QXJyLm1hcChmdW5jdGlvbihlKSB7IHJldHVybiBlLnJlc291cmNlSW5kZXg7IH0pLmluZGV4T2YoaW5kKTsgLy9EZXRlY3RpbmcgcG9zaXRpb24gb2YgaXRlbSBieSBpdGBzICdyZXNvdXJjZUluZGV4JyB2YWx1ZVxyXG4gICAgICAgIHRoaXMuc3RhdGUucmVzb3VyY2VMaXN0QXJyLnNwbGljZShwb3MsIDEpOyAvL0RlbGV0ZSBpdGVtIGZyb20gcHJvZHVjdHMgYXJyYXlcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VMaXN0QXJyOiAgdGhpcy5zdGF0ZS5yZXNvdXJjZUxpc3RBcnIgfSk7IC8vVXBkYXRpbmcgc3RhdGVcclxuICAgICAgICAvL0luIGNhc2UgaWYgYWxsIG9iamVjdHMgcmVtb3ZlZCwgaGlkZSAnQ2xlYXIgQWxsJyBidXR0b25cclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5yZXNvdXJjZUxpc3RBcnIubGVuZ3RoID09IDApIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7ZGVsZXRlQnV0dG9uU2hvd246IGZhbHNlfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vSGFuZGxpbmcgJ0NsZWFyIEFsbCcgYnV0dG9uIGFjdGlvblxyXG4gICAgaGFuZGxlQ2xlYXIgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHJlc291cmNlTGlzdEFycjogW10gfSk7IC8vUmVtb3ZlIGFsbCBpdGVtcyBmcm9tIHByb2R1Y3RgcyBhcnJheVxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe2RlbGV0ZUJ1dHRvblNob3duOiBmYWxzZX0pOy8vSGlkZSAnQ2xlYXIgQWxsJyBidXR0b25cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VJbmRleDogMCB9KTsvL1NldCAnUmVzb3VyY2VJbmRleCcgY291bnRlciB0byAwXHJcbiAgICB9XHJcblxyXG4gICAgLy9IYW5kbGluZyBpdGVtcyBzZWxlY3Rpb24gZnJvbSByZXNvdXJjZSBwaWNrZXJcclxuICAgIGhhbmRsZVNlbGVjdGlvbiA9IChyZXNvdXJjZXMpID0+IHtcclxuICAgICAgICAvL21hcHBpbmcgbmV3IGFycmF5cyB3aXRoIG5lZWRlZCBwcm9kdWN0cyB2YWx1ZXNcclxuICAgICAgICBjb25zdCB0aXRsZUZyb21SZXNvdXJjZXMgPSByZXNvdXJjZXMuc2VsZWN0aW9uLm1hcCgocHJvZHVjdCkgPT4gcHJvZHVjdC50aXRsZSk7XHJcbiAgICAgICAgY29uc3QgaWRGcm9tUmVzb3VyY2VzID0gcmVzb3VyY2VzLnNlbGVjdGlvbi5tYXAoKHByb2R1Y3QpID0+IHByb2R1Y3QuaWQpO1xyXG4gICAgICAgIGNvbnN0IHByaWNlRnJvbVJlc291cmNlcyA9IHJlc291cmNlcy5zZWxlY3Rpb24ubWFwKChwcm9kdWN0KSA9PiBwcm9kdWN0LnZhcmlhbnRzLm1hcChmdW5jdGlvbihlKSB7IHJldHVybiBlLnByaWNlOyB9KSk7XHJcbiAgICAgICAgY29uc3QgdmFyaWFudEZyb21SZXNvdXJjZXMgPSByZXNvdXJjZXMuc2VsZWN0aW9uLm1hcCgocHJvZHVjdCkgPT4gcHJvZHVjdC52YXJpYW50cy5tYXAoZnVuY3Rpb24oZSkgeyByZXR1cm4gZS50aXRsZTsgfSkpO1xyXG4gICAgICAgIGNvbnN0IGltZ0Zyb21SZXNvdXJjZXMgPSByZXNvdXJjZXMuc2VsZWN0aW9uLm1hcCgocHJvZHVjdCkgPT4gcHJvZHVjdC5pbWFnZXNbMF0ub3JpZ2luYWxTcmMpO1xyXG4gICAgICAgIC8vU2hvdyAnQ2xlYXIgQWxsJyBidXR0b25cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtkZWxldGVCdXR0b25TaG93bjogdHJ1ZX0pO1xyXG4gICAgICAgIC8vQ2xvc2UgUmVzb3VyY2UgUGlja2VyXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7b3BlbjogZmFsc2V9KTtcclxuICAgICAgICAvL0FkZGluZyBwcm9kdWN0cyB0byBwcm9kdWN0YHMgYXJyYXlcclxuICAgICAgICBjb25zb2xlLmxvZyhyZXNvdXJjZXMpO1xyXG4gICAgICAgIGZvciAobGV0IHggPSAwOyB4IDwgcHJpY2VGcm9tUmVzb3VyY2VzLmxlbmd0aDsgeCsrKSB7XHJcbiAgICAgICAgICAgIGlmIChwcmljZUZyb21SZXNvdXJjZXNbeF0ubGVuZ3RoID09IDEpe1xyXG4gICAgICAgICAgICAgICAgbGV0IGpvaW5lZCA9IHRoaXMuc3RhdGUucmVzb3VyY2VMaXN0QXJyLmNvbmNhdCh7cmVzb3VyY2VJbmRleDogdGhpcy5zdGF0ZS5yZXNvdXJjZUluZGV4LCBpZDogaWRGcm9tUmVzb3VyY2VzW3hdLCAgbmFtZTp0aXRsZUZyb21SZXNvdXJjZXNbeF0sIGF2YXRhclNvdXJjZTppbWdGcm9tUmVzb3VyY2VzW3hdLCBwcmljZTpwcmljZUZyb21SZXNvdXJjZXNbeF0sIHZhcmlhbnQ6IHZhcmlhbnRGcm9tUmVzb3VyY2VzW3hdID09ICdUaXRsZScgPyAndHJ1ZScgOiAnZmFsc2UnfSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VMaXN0QXJyOiBqb2luZWQgfSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VJbmRleDogdGhpcy5zdGF0ZS5yZXNvdXJjZUluZGV4ICsgMSB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGZvciAobGV0IHkgPSAwOyB5IDwgcHJpY2VGcm9tUmVzb3VyY2VzW3hdLmxlbmd0aDsgeSsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IG5ld0pvaW5lZCA9IHRoaXMuc3RhdGUucmVzb3VyY2VMaXN0QXJyLmNvbmNhdCh7cmVzb3VyY2VJbmRleDogdGhpcy5zdGF0ZS5yZXNvdXJjZUluZGV4LCBpZDogaWRGcm9tUmVzb3VyY2VzW3hdLCAgbmFtZTp0aXRsZUZyb21SZXNvdXJjZXNbeF0sIGF2YXRhclNvdXJjZTppbWdGcm9tUmVzb3VyY2VzW3hdLCBwcmljZTpwcmljZUZyb21SZXNvdXJjZXNbeF1beV0sIHZhcmlhbnQ6IHZhcmlhbnRGcm9tUmVzb3VyY2VzW3hdW3ldfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHJlc291cmNlTGlzdEFycjogbmV3Sm9pbmVkIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyByZXNvdXJjZUluZGV4OiB0aGlzLnN0YXRlLnJlc291cmNlSW5kZXggKyAxIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufVxyXG4gIFxyXG4gIGV4cG9ydCBkZWZhdWx0IEluZGV4OyJdLCJzb3VyY2VSb290IjoiIn0=