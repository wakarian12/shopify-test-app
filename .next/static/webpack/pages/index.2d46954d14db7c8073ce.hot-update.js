webpackHotUpdate_N_E("pages/index",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/esm/createClass */ "./node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inherits */ "./node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _shopify_polaris__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @shopify/polaris */ "./node_modules/@shopify/polaris/dist/esm/index.js");
/* harmony import */ var _shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @shopify/app-bridge-react */ "./node_modules/@shopify/app-bridge-react/index.js");
/* harmony import */ var _shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_10__);










function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return Object(_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

//Import Polaris elements, Resource Picker and React




var Index = /*#__PURE__*/function (_Component) {
  Object(_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__["default"])(Index, _Component);

  var _super = _createSuper(Index);

  function Index() {
    var _this;

    Object(_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, Index);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "state", {
      open: false,
      resourceListArr: [],
      deleteButtonShown: false,
      resourceIndex: 0
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleDelete", function (ind) {
      var pos = _this.state.resourceListArr.map(function (e) {
        return e.resourceIndex;
      }).indexOf(ind); //Detecting position of item by it`s 'resourceIndex' value


      _this.state.resourceListArr.splice(pos, 1); //Delete item from products array


      _this.setState({
        resourceListArr: _this.state.resourceListArr
      }); //Updating state
      //In case if all objects removed, hide 'Clear All' button


      if (_this.state.resourceListArr.length == 0) {
        _this.setState({
          deleteButtonShown: false
        });
      }
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleClear", function () {
      _this.setState({
        resourceListArr: []
      }); //Remove all items from product`s array


      _this.setState({
        deleteButtonShown: false
      }); //Hide 'Clear All' button


      _this.setState({
        resourceIndex: 0
      }); //Set 'ResourceIndex' counter to 0

    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleSelection", function (resources) {
      //mapping new arrays with needed products values
      var titleFromResources = resources.selection.map(function (product) {
        return product.title;
      });
      var idFromResources = resources.selection.map(function (product) {
        return product.id;
      });
      var priceFromResources = resources.selection.map(function (product) {
        return product.variants.map(function (e) {
          return e.price;
        });
      });
      var variantFromResources = resources.selection.map(function (product) {
        return product.variants.map(function (e) {
          return e.title;
        });
      });
      var imgFromResources = resources.selection.map(function (product) {
        return product.images[0].originalSrc;
      }); //Show 'Clear All' button

      _this.setState({
        deleteButtonShown: true
      }); //Close Resource Picker


      _this.setState({
        open: false
      }); //Adding products to product`s array


      console.log(resources);
      console.log(variantFromResources);

      for (var x = 0; x < priceFromResources.length; x++) {
        if (priceFromResources[x].length == 1) {
          var joined = _this.state.resourceListArr.concat({
            resourceIndex: _this.state.resourceIndex,
            id: idFromResources[x],
            name: titleFromResources[x],
            avatarSource: imgFromResources[x],
            price: priceFromResources[x]
          });

          _this.setState({
            resourceListArr: joined
          });

          _this.setState({
            resourceIndex: _this.state.resourceIndex + 1
          });
        } else {
          for (var y = 0; y < priceFromResources[x].length; y++) {
            var newJoined = _this.state.resourceListArr.concat({
              resourceIndex: _this.state.resourceIndex,
              id: idFromResources[x],
              name: titleFromResources[x],
              avatarSource: imgFromResources[x],
              price: priceFromResources[x][y]
            });

            _this.setState({
              resourceListArr: newJoined
            });

            _this.setState({
              resourceIndex: _this.state.resourceIndex + 1
            });
          }
        }
      }
    });

    return _this;
  }

  Object(_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(Index, [{
    key: "render",
    //set state
    value: function render() {
      var _this2 = this;

      return (
        /*#__PURE__*/
        //Page styling using Polaris Page element, Text an Button
        Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxs"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Page"], {
          fullWidth: true,
          title: "Product Selection",
          primaryAction: {
            content: 'Select product',
            onAction: function onAction() {
              return _this2.setState({
                open: true
              });
            }
          },
          children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9__["ResourcePicker"]
          /*Resource Picker, change state on clothong and handling if items selected*/
          , {
            resourceType: "Product",
            open: this.state.open,
            onCancel: function onCancel() {
              return _this2.setState({
                open: false
              });
            },
            onSelection: function onSelection(resources) {
              return _this2.handleSelection(resources);
            }
          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
            children: // Display Clearing Button if any item is selected
            this.state.deleteButtonShown ? /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
              style: {
                marginBottom: '15px'
              },
              children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Button"], {
                destructive: true,
                onClick: function onClick() {
                  return _this2.handleClear();
                },
                children: "Clear all"
              })
            }) : null
          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Card"], {
            children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["ResourceList"]
            /*Adding Resource List wich consists of ResourceItems */
            , {
              resourceName: {
                singular: 'product',
                plural: 'product'
              },
              items: this.state.resourceListArr,
              renderItem: function renderItem(item) {
                var resourceIndex = item.resourceIndex,
                    id = item.id,
                    name = item.name,
                    avatarSource = item.avatarSource,
                    price = item.price,
                    variant = item.variant; //Setting Item

                return /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxs"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["ResourceItem"], {
                  id: id,
                  media: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Avatar"], {
                    customer: true,
                    size: "medium",
                    name: name,
                    source: avatarSource
                  }),
                  accessibilityLabel: "View details for ".concat(name),
                  children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("h2", {
                    children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["TextStyle"], {
                      variation: "strong",
                      children: name
                    })
                  }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
                    children: price
                  }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
                    style: {
                      position: 'absolute',
                      right: '20px',
                      marginTop: '-40px'
                    },
                    children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Button"], {
                      destructive: true,
                      resourceIndex: resourceIndex,
                      onClick: function onClick(ind) {
                        return _this2.handleDelete(item.resourceIndex);
                      },
                      children: "Delete"
                    })
                  })]
                });
              }
            })
          })]
        })
      );
    } //Handling Deletion of element from Resource List

  }]);

  return Index;
}(react__WEBPACK_IMPORTED_MODULE_10__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Index);

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvaW5kZXguanMiXSwibmFtZXMiOlsiSW5kZXgiLCJvcGVuIiwicmVzb3VyY2VMaXN0QXJyIiwiZGVsZXRlQnV0dG9uU2hvd24iLCJyZXNvdXJjZUluZGV4IiwiaW5kIiwicG9zIiwic3RhdGUiLCJtYXAiLCJlIiwiaW5kZXhPZiIsInNwbGljZSIsInNldFN0YXRlIiwibGVuZ3RoIiwicmVzb3VyY2VzIiwidGl0bGVGcm9tUmVzb3VyY2VzIiwic2VsZWN0aW9uIiwicHJvZHVjdCIsInRpdGxlIiwiaWRGcm9tUmVzb3VyY2VzIiwiaWQiLCJwcmljZUZyb21SZXNvdXJjZXMiLCJ2YXJpYW50cyIsInByaWNlIiwidmFyaWFudEZyb21SZXNvdXJjZXMiLCJpbWdGcm9tUmVzb3VyY2VzIiwiaW1hZ2VzIiwib3JpZ2luYWxTcmMiLCJjb25zb2xlIiwibG9nIiwieCIsImpvaW5lZCIsImNvbmNhdCIsIm5hbWUiLCJhdmF0YXJTb3VyY2UiLCJ5IiwibmV3Sm9pbmVkIiwiY29udGVudCIsIm9uQWN0aW9uIiwiaGFuZGxlU2VsZWN0aW9uIiwibWFyZ2luQm90dG9tIiwiaGFuZGxlQ2xlYXIiLCJzaW5ndWxhciIsInBsdXJhbCIsIml0ZW0iLCJ2YXJpYW50IiwicG9zaXRpb24iLCJyaWdodCIsIm1hcmdpblRvcCIsImhhbmRsZURlbGV0ZSIsIkNvbXBvbmVudCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7O0lBRU1BLEs7Ozs7Ozs7Ozs7Ozs7Ozs7Z05BQ007QUFBRUMsVUFBSSxFQUFFLEtBQVI7QUFBZUMscUJBQWUsRUFBRSxFQUFoQztBQUFvQ0MsdUJBQWlCLEVBQUUsS0FBdkQ7QUFBOERDLG1CQUFhLEVBQUU7QUFBN0UsSzs7dU5BMERPLFVBQUNDLEdBQUQsRUFBUztBQUNwQixVQUFJQyxHQUFHLEdBQUcsTUFBS0MsS0FBTCxDQUFXTCxlQUFYLENBQTJCTSxHQUEzQixDQUErQixVQUFTQyxDQUFULEVBQVk7QUFBRSxlQUFPQSxDQUFDLENBQUNMLGFBQVQ7QUFBeUIsT0FBdEUsRUFBd0VNLE9BQXhFLENBQWdGTCxHQUFoRixDQUFWLENBRG9CLENBQzRFOzs7QUFDaEcsWUFBS0UsS0FBTCxDQUFXTCxlQUFYLENBQTJCUyxNQUEzQixDQUFrQ0wsR0FBbEMsRUFBdUMsQ0FBdkMsRUFGb0IsQ0FFdUI7OztBQUMzQyxZQUFLTSxRQUFMLENBQWM7QUFBRVYsdUJBQWUsRUFBRyxNQUFLSyxLQUFMLENBQVdMO0FBQS9CLE9BQWQsRUFIb0IsQ0FHNkM7QUFDakU7OztBQUNBLFVBQUksTUFBS0ssS0FBTCxDQUFXTCxlQUFYLENBQTJCVyxNQUEzQixJQUFxQyxDQUF6QyxFQUE0QztBQUN4QyxjQUFLRCxRQUFMLENBQWM7QUFBQ1QsMkJBQWlCLEVBQUU7QUFBcEIsU0FBZDtBQUNIO0FBQ0osSzs7c05BR2EsWUFBTTtBQUNoQixZQUFLUyxRQUFMLENBQWM7QUFBRVYsdUJBQWUsRUFBRTtBQUFuQixPQUFkLEVBRGdCLENBQ3dCOzs7QUFDeEMsWUFBS1UsUUFBTCxDQUFjO0FBQUNULHlCQUFpQixFQUFFO0FBQXBCLE9BQWQsRUFGZ0IsQ0FFMEI7OztBQUMxQyxZQUFLUyxRQUFMLENBQWM7QUFBRVIscUJBQWEsRUFBRTtBQUFqQixPQUFkLEVBSGdCLENBR29COztBQUN2QyxLOzswTkFHaUIsVUFBQ1UsU0FBRCxFQUFlO0FBQzdCO0FBQ0EsVUFBTUMsa0JBQWtCLEdBQUdELFNBQVMsQ0FBQ0UsU0FBVixDQUFvQlIsR0FBcEIsQ0FBd0IsVUFBQ1MsT0FBRDtBQUFBLGVBQWFBLE9BQU8sQ0FBQ0MsS0FBckI7QUFBQSxPQUF4QixDQUEzQjtBQUNBLFVBQU1DLGVBQWUsR0FBR0wsU0FBUyxDQUFDRSxTQUFWLENBQW9CUixHQUFwQixDQUF3QixVQUFDUyxPQUFEO0FBQUEsZUFBYUEsT0FBTyxDQUFDRyxFQUFyQjtBQUFBLE9BQXhCLENBQXhCO0FBQ0EsVUFBTUMsa0JBQWtCLEdBQUdQLFNBQVMsQ0FBQ0UsU0FBVixDQUFvQlIsR0FBcEIsQ0FBd0IsVUFBQ1MsT0FBRDtBQUFBLGVBQWFBLE9BQU8sQ0FBQ0ssUUFBUixDQUFpQmQsR0FBakIsQ0FBcUIsVUFBU0MsQ0FBVCxFQUFZO0FBQUUsaUJBQU9BLENBQUMsQ0FBQ2MsS0FBVDtBQUFpQixTQUFwRCxDQUFiO0FBQUEsT0FBeEIsQ0FBM0I7QUFDQSxVQUFNQyxvQkFBb0IsR0FBR1YsU0FBUyxDQUFDRSxTQUFWLENBQW9CUixHQUFwQixDQUF3QixVQUFDUyxPQUFEO0FBQUEsZUFBYUEsT0FBTyxDQUFDSyxRQUFSLENBQWlCZCxHQUFqQixDQUFxQixVQUFTQyxDQUFULEVBQVk7QUFBRSxpQkFBT0EsQ0FBQyxDQUFDUyxLQUFUO0FBQWlCLFNBQXBELENBQWI7QUFBQSxPQUF4QixDQUE3QjtBQUNBLFVBQU1PLGdCQUFnQixHQUFHWCxTQUFTLENBQUNFLFNBQVYsQ0FBb0JSLEdBQXBCLENBQXdCLFVBQUNTLE9BQUQ7QUFBQSxlQUFhQSxPQUFPLENBQUNTLE1BQVIsQ0FBZSxDQUFmLEVBQWtCQyxXQUEvQjtBQUFBLE9BQXhCLENBQXpCLENBTjZCLENBTzdCOztBQUNBLFlBQUtmLFFBQUwsQ0FBYztBQUFDVCx5QkFBaUIsRUFBRTtBQUFwQixPQUFkLEVBUjZCLENBUzdCOzs7QUFDQSxZQUFLUyxRQUFMLENBQWM7QUFBQ1gsWUFBSSxFQUFFO0FBQVAsT0FBZCxFQVY2QixDQVc3Qjs7O0FBQ0EyQixhQUFPLENBQUNDLEdBQVIsQ0FBWWYsU0FBWjtBQUNBYyxhQUFPLENBQUNDLEdBQVIsQ0FBWUwsb0JBQVo7O0FBQ0EsV0FBSyxJQUFJTSxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHVCxrQkFBa0IsQ0FBQ1IsTUFBdkMsRUFBK0NpQixDQUFDLEVBQWhELEVBQW9EO0FBQ2hELFlBQUlULGtCQUFrQixDQUFDUyxDQUFELENBQWxCLENBQXNCakIsTUFBdEIsSUFBZ0MsQ0FBcEMsRUFBc0M7QUFDbEMsY0FBSWtCLE1BQU0sR0FBRyxNQUFLeEIsS0FBTCxDQUFXTCxlQUFYLENBQTJCOEIsTUFBM0IsQ0FBa0M7QUFBQzVCLHlCQUFhLEVBQUUsTUFBS0csS0FBTCxDQUFXSCxhQUEzQjtBQUEwQ2dCLGNBQUUsRUFBRUQsZUFBZSxDQUFDVyxDQUFELENBQTdEO0FBQW1FRyxnQkFBSSxFQUFDbEIsa0JBQWtCLENBQUNlLENBQUQsQ0FBMUY7QUFBK0ZJLHdCQUFZLEVBQUNULGdCQUFnQixDQUFDSyxDQUFELENBQTVIO0FBQWlJUCxpQkFBSyxFQUFDRixrQkFBa0IsQ0FBQ1MsQ0FBRDtBQUF6SixXQUFsQyxDQUFiOztBQUNBLGdCQUFLbEIsUUFBTCxDQUFjO0FBQUVWLDJCQUFlLEVBQUU2QjtBQUFuQixXQUFkOztBQUNBLGdCQUFLbkIsUUFBTCxDQUFjO0FBQUVSLHlCQUFhLEVBQUUsTUFBS0csS0FBTCxDQUFXSCxhQUFYLEdBQTJCO0FBQTVDLFdBQWQ7QUFDSCxTQUpELE1BS0s7QUFDRCxlQUFLLElBQUkrQixDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHZCxrQkFBa0IsQ0FBQ1MsQ0FBRCxDQUFsQixDQUFzQmpCLE1BQTFDLEVBQWtEc0IsQ0FBQyxFQUFuRCxFQUF1RDtBQUNuRCxnQkFBSUMsU0FBUyxHQUFHLE1BQUs3QixLQUFMLENBQVdMLGVBQVgsQ0FBMkI4QixNQUEzQixDQUFrQztBQUFDNUIsMkJBQWEsRUFBRSxNQUFLRyxLQUFMLENBQVdILGFBQTNCO0FBQTBDZ0IsZ0JBQUUsRUFBRUQsZUFBZSxDQUFDVyxDQUFELENBQTdEO0FBQW1FRyxrQkFBSSxFQUFDbEIsa0JBQWtCLENBQUNlLENBQUQsQ0FBMUY7QUFBK0ZJLDBCQUFZLEVBQUNULGdCQUFnQixDQUFDSyxDQUFELENBQTVIO0FBQWlJUCxtQkFBSyxFQUFDRixrQkFBa0IsQ0FBQ1MsQ0FBRCxDQUFsQixDQUFzQkssQ0FBdEI7QUFBdkksYUFBbEMsQ0FBaEI7O0FBQ0Esa0JBQUt2QixRQUFMLENBQWM7QUFBRVYsNkJBQWUsRUFBRWtDO0FBQW5CLGFBQWQ7O0FBQ0Esa0JBQUt4QixRQUFMLENBQWM7QUFBRVIsMkJBQWEsRUFBRSxNQUFLRyxLQUFMLENBQVdILGFBQVgsR0FBMkI7QUFBNUMsYUFBZDtBQUNIO0FBQ0o7QUFDSjtBQUNKLEs7Ozs7Ozs7QUF4R3VGOzZCQUUvRTtBQUFBOztBQUNMO0FBQUE7QUFDQTtBQUNBLHVFQUFDLHFEQUFEO0FBQ0ksbUJBQVMsTUFEYjtBQUVJLGVBQUssRUFBQyxtQkFGVjtBQUdJLHVCQUFhLEVBQUU7QUFDWGlDLG1CQUFPLEVBQUUsZ0JBREU7QUFFWEMsb0JBQVEsRUFBRTtBQUFBLHFCQUFNLE1BQUksQ0FBQzFCLFFBQUwsQ0FBYztBQUFDWCxvQkFBSSxFQUFFO0FBQVAsZUFBZCxDQUFOO0FBQUE7QUFGQyxXQUhuQjtBQUFBLGtDQVFRLDhEQUFDLHdFQUFjO0FBQUU7QUFBakI7QUFDSSx3QkFBWSxFQUFDLFNBRGpCO0FBRUksZ0JBQUksRUFBRSxLQUFLTSxLQUFMLENBQVdOLElBRnJCO0FBR0ksb0JBQVEsRUFBRztBQUFBLHFCQUFNLE1BQUksQ0FBQ1csUUFBTCxDQUFjO0FBQUNYLG9CQUFJLEVBQUU7QUFBUCxlQUFkLENBQU47QUFBQSxhQUhmO0FBSUksdUJBQVcsRUFBRSxxQkFBQ2EsU0FBRDtBQUFBLHFCQUFlLE1BQUksQ0FBQ3lCLGVBQUwsQ0FBcUJ6QixTQUFyQixDQUFmO0FBQUE7QUFKakIsWUFSUixlQWNRO0FBQUEsc0JBQ0s7QUFDRCxpQkFBS1AsS0FBTCxDQUFXSixpQkFBWCxnQkFDSTtBQUFLLG1CQUFLLEVBQUU7QUFBQ3FDLDRCQUFZLEVBQUU7QUFBZixlQUFaO0FBQUEscUNBQ0ksOERBQUMsdURBQUQ7QUFBUSwyQkFBVyxNQUFuQjtBQUNBLHVCQUFPLEVBQUU7QUFBQSx5QkFBTSxNQUFJLENBQUNDLFdBQUwsRUFBTjtBQUFBLGlCQURUO0FBQUE7QUFBQTtBQURKLGNBREosR0FLRTtBQVBOLFlBZFIsZUF1QlEsOERBQUMscURBQUQ7QUFBQSxtQ0FDQSw4REFBQyw2REFBWTtBQUFDO0FBQWQ7QUFDSSwwQkFBWSxFQUFFO0FBQUNDLHdCQUFRLEVBQUUsU0FBWDtBQUFzQkMsc0JBQU0sRUFBRTtBQUE5QixlQURsQjtBQUVJLG1CQUFLLEVBQUcsS0FBS3BDLEtBQUwsQ0FBV0wsZUFGdkI7QUFHSSx3QkFBVSxFQUFFLG9CQUFDMEMsSUFBRCxFQUFVO0FBQUEsb0JBQ2Z4QyxhQURlLEdBQzJDd0MsSUFEM0MsQ0FDZnhDLGFBRGU7QUFBQSxvQkFDQWdCLEVBREEsR0FDMkN3QixJQUQzQyxDQUNBeEIsRUFEQTtBQUFBLG9CQUNLYSxJQURMLEdBQzJDVyxJQUQzQyxDQUNLWCxJQURMO0FBQUEsb0JBQ1dDLFlBRFgsR0FDMkNVLElBRDNDLENBQ1dWLFlBRFg7QUFBQSxvQkFDeUJYLEtBRHpCLEdBQzJDcUIsSUFEM0MsQ0FDeUJyQixLQUR6QjtBQUFBLG9CQUNnQ3NCLE9BRGhDLEdBQzJDRCxJQUQzQyxDQUNnQ0MsT0FEaEMsRUFDaUQ7O0FBQ3ZFLG9DQUNJLCtEQUFDLDZEQUFEO0FBQ0Esb0JBQUUsRUFBRXpCLEVBREo7QUFFQSx1QkFBSyxlQUFHLDhEQUFDLHVEQUFEO0FBQVEsNEJBQVEsTUFBaEI7QUFBaUIsd0JBQUksRUFBQyxRQUF0QjtBQUErQix3QkFBSSxFQUFFYSxJQUFyQztBQUEyQywwQkFBTSxFQUFFQztBQUFuRCxvQkFGUjtBQUdBLG9DQUFrQiw2QkFBc0JELElBQXRCLENBSGxCO0FBQUEsMENBS0E7QUFBQSwyQ0FDSSw4REFBQywwREFBRDtBQUFXLCtCQUFTLEVBQUMsUUFBckI7QUFBQSxnQ0FBK0JBO0FBQS9CO0FBREosb0JBTEEsZUFRQTtBQUFBLDhCQUFNVjtBQUFOLG9CQVJBLGVBU0E7QUFBSyx5QkFBSyxFQUFFO0FBQUN1Qiw4QkFBUSxFQUFDLFVBQVY7QUFBc0JDLDJCQUFLLEVBQUMsTUFBNUI7QUFBb0NDLCtCQUFTLEVBQUU7QUFBL0MscUJBQVo7QUFBQSwyQ0FDSSw4REFBQyx1REFBRDtBQUFRLGlDQUFXLE1BQW5CO0FBQW9CLG1DQUFhLEVBQUU1QyxhQUFuQztBQUNBLDZCQUFPLEVBQUUsaUJBQUNDLEdBQUQ7QUFBQSwrQkFBUyxNQUFJLENBQUM0QyxZQUFMLENBQWtCTCxJQUFJLENBQUN4QyxhQUF2QixDQUFUO0FBQUEsdUJBRFQ7QUFBQTtBQUFBO0FBREosb0JBVEE7QUFBQSxrQkFESjtBQWdCQztBQXJCTDtBQURBLFlBdkJSO0FBQUE7QUFGQTtBQW9ESCxLLENBRUQ7Ozs7O0VBMURnQjhDLGdEOztBQTZHSGxELG9FQUFmIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3BhZ2VzL2luZGV4LjJkNDY5NTRkMTRkYjdjODA3M2NlLmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvL0ltcG9ydCBQb2xhcmlzIGVsZW1lbnRzLCBSZXNvdXJjZSBQaWNrZXIgYW5kIFJlYWN0XHJcbmltcG9ydCB7IFBhZ2UsIENhcmQsIFJlc291cmNlTGlzdCwgUmVzb3VyY2VJdGVtLCBBdmF0YXIsIFRleHRTdHlsZSwgQnV0dG9uIH0gZnJvbSBcIkBzaG9waWZ5L3BvbGFyaXNcIjtcclxuaW1wb3J0IHsgUmVzb3VyY2VQaWNrZXIgfSBmcm9tIFwiQHNob3BpZnkvYXBwLWJyaWRnZS1yZWFjdFwiO1xyXG5pbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xyXG5cclxuY2xhc3MgSW5kZXggZXh0ZW5kcyBDb21wb25lbnQge1xyXG4gICAgc3RhdGUgPSB7IG9wZW46IGZhbHNlLCByZXNvdXJjZUxpc3RBcnI6IFtdLCBkZWxldGVCdXR0b25TaG93bjogZmFsc2UsIHJlc291cmNlSW5kZXg6IDAgfS8vc2V0IHN0YXRlXHJcblxyXG4gICAgcmVuZGVyKCkgeyBcclxuICAgICAgICByZXR1cm4gKCBcclxuICAgICAgICAvL1BhZ2Ugc3R5bGluZyB1c2luZyBQb2xhcmlzIFBhZ2UgZWxlbWVudCwgVGV4dCBhbiBCdXR0b25cclxuICAgICAgICA8UGFnZSAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBmdWxsV2lkdGhcclxuICAgICAgICAgICAgdGl0bGU9XCJQcm9kdWN0IFNlbGVjdGlvblwiXHJcbiAgICAgICAgICAgIHByaW1hcnlBY3Rpb249e3tcclxuICAgICAgICAgICAgICAgIGNvbnRlbnQ6ICdTZWxlY3QgcHJvZHVjdCcsXHJcbiAgICAgICAgICAgICAgICBvbkFjdGlvbjogKCkgPT4gdGhpcy5zZXRTdGF0ZSh7b3BlbjogdHJ1ZX0pXHJcbiAgICAgICAgICAgIH19XHJcbiAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIDxSZXNvdXJjZVBpY2tlciAgLypSZXNvdXJjZSBQaWNrZXIsIGNoYW5nZSBzdGF0ZSBvbiBjbG90aG9uZyBhbmQgaGFuZGxpbmcgaWYgaXRlbXMgc2VsZWN0ZWQqL1xyXG4gICAgICAgICAgICAgICAgICAgIHJlc291cmNlVHlwZT1cIlByb2R1Y3RcIlxyXG4gICAgICAgICAgICAgICAgICAgIG9wZW49e3RoaXMuc3RhdGUub3Blbn1cclxuICAgICAgICAgICAgICAgICAgICBvbkNhbmNlbD17ICgpID0+IHRoaXMuc2V0U3RhdGUoe29wZW46IGZhbHNlfSl9XHJcbiAgICAgICAgICAgICAgICAgICAgb25TZWxlY3Rpb249eyhyZXNvdXJjZXMpID0+IHRoaXMuaGFuZGxlU2VsZWN0aW9uKHJlc291cmNlcyl9IFxyXG4gICAgICAgICAgICAgICAgLz4gXHJcbiAgICAgICAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgIHsvLyBEaXNwbGF5IENsZWFyaW5nIEJ1dHRvbiBpZiBhbnkgaXRlbSBpcyBzZWxlY3RlZFxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc3RhdGUuZGVsZXRlQnV0dG9uU2hvd24gPyBcclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBzdHlsZT17e21hcmdpbkJvdHRvbTogJzE1cHgnfX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QnV0dG9uIGRlc3RydWN0aXZlIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gdGhpcy5oYW5kbGVDbGVhcigpfT5DbGVhciBhbGw8L0J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+IFxyXG4gICAgICAgICAgICAgICAgICAgIDogbnVsbH0gXHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxDYXJkPlxyXG4gICAgICAgICAgICAgICAgPFJlc291cmNlTGlzdCAvKkFkZGluZyBSZXNvdXJjZSBMaXN0IHdpY2ggY29uc2lzdHMgb2YgUmVzb3VyY2VJdGVtcyAqL1xyXG4gICAgICAgICAgICAgICAgICAgIHJlc291cmNlTmFtZT17e3Npbmd1bGFyOiAncHJvZHVjdCcsIHBsdXJhbDogJ3Byb2R1Y3QnfX1cclxuICAgICAgICAgICAgICAgICAgICBpdGVtcz17IHRoaXMuc3RhdGUucmVzb3VyY2VMaXN0QXJyIH1cclxuICAgICAgICAgICAgICAgICAgICByZW5kZXJJdGVtPXsoaXRlbSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHtyZXNvdXJjZUluZGV4LCBpZCwgIG5hbWUsIGF2YXRhclNvdXJjZSwgcHJpY2UsIHZhcmlhbnR9ID0gaXRlbTsgLy9TZXR0aW5nIEl0ZW1cclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8UmVzb3VyY2VJdGVtXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkPXtpZH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgbWVkaWE9IHs8QXZhdGFyIGN1c3RvbWVyIHNpemU9XCJtZWRpdW1cIiBuYW1lPXtuYW1lfSBzb3VyY2U9e2F2YXRhclNvdXJjZX0gLz59XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFjY2Vzc2liaWxpdHlMYWJlbD17YFZpZXcgZGV0YWlscyBmb3IgJHtuYW1lfWB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGgyPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPFRleHRTdHlsZSB2YXJpYXRpb249XCJzdHJvbmdcIj57bmFtZX08L1RleHRTdHlsZT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9oMj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdj57cHJpY2V9PC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgc3R5bGU9e3twb3NpdGlvbjonYWJzb2x1dGUnLCByaWdodDonMjBweCcsIG1hcmdpblRvcDogJy00MHB4J319PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEJ1dHRvbiBkZXN0cnVjdGl2ZSByZXNvdXJjZUluZGV4PXtyZXNvdXJjZUluZGV4fVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KGluZCkgPT4gdGhpcy5oYW5kbGVEZWxldGUoaXRlbS5yZXNvdXJjZUluZGV4KX0+RGVsZXRlPC9CdXR0b24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L1Jlc291cmNlSXRlbT5cclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgICAgIH19XHJcbiAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgICAgPC9DYXJkPlxyXG4gICAgICAgIDwvUGFnZT4gXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvL0hhbmRsaW5nIERlbGV0aW9uIG9mIGVsZW1lbnQgZnJvbSBSZXNvdXJjZSBMaXN0XHJcbiAgICBoYW5kbGVEZWxldGUgPSAoaW5kKSA9PiB7XHJcbiAgICAgICAgdmFyIHBvcyA9IHRoaXMuc3RhdGUucmVzb3VyY2VMaXN0QXJyLm1hcChmdW5jdGlvbihlKSB7IHJldHVybiBlLnJlc291cmNlSW5kZXg7IH0pLmluZGV4T2YoaW5kKTsgLy9EZXRlY3RpbmcgcG9zaXRpb24gb2YgaXRlbSBieSBpdGBzICdyZXNvdXJjZUluZGV4JyB2YWx1ZVxyXG4gICAgICAgIHRoaXMuc3RhdGUucmVzb3VyY2VMaXN0QXJyLnNwbGljZShwb3MsIDEpOyAvL0RlbGV0ZSBpdGVtIGZyb20gcHJvZHVjdHMgYXJyYXlcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VMaXN0QXJyOiAgdGhpcy5zdGF0ZS5yZXNvdXJjZUxpc3RBcnIgfSk7IC8vVXBkYXRpbmcgc3RhdGVcclxuICAgICAgICAvL0luIGNhc2UgaWYgYWxsIG9iamVjdHMgcmVtb3ZlZCwgaGlkZSAnQ2xlYXIgQWxsJyBidXR0b25cclxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5yZXNvdXJjZUxpc3RBcnIubGVuZ3RoID09IDApIHtcclxuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7ZGVsZXRlQnV0dG9uU2hvd246IGZhbHNlfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vSGFuZGxpbmcgJ0NsZWFyIEFsbCcgYnV0dG9uIGFjdGlvblxyXG4gICAgaGFuZGxlQ2xlYXIgPSAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHJlc291cmNlTGlzdEFycjogW10gfSk7IC8vUmVtb3ZlIGFsbCBpdGVtcyBmcm9tIHByb2R1Y3RgcyBhcnJheVxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe2RlbGV0ZUJ1dHRvblNob3duOiBmYWxzZX0pOy8vSGlkZSAnQ2xlYXIgQWxsJyBidXR0b25cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VJbmRleDogMCB9KTsvL1NldCAnUmVzb3VyY2VJbmRleCcgY291bnRlciB0byAwXHJcbiAgICB9XHJcblxyXG4gICAgLy9IYW5kbGluZyBpdGVtcyBzZWxlY3Rpb24gZnJvbSByZXNvdXJjZSBwaWNrZXJcclxuICAgIGhhbmRsZVNlbGVjdGlvbiA9IChyZXNvdXJjZXMpID0+IHtcclxuICAgICAgICAvL21hcHBpbmcgbmV3IGFycmF5cyB3aXRoIG5lZWRlZCBwcm9kdWN0cyB2YWx1ZXNcclxuICAgICAgICBjb25zdCB0aXRsZUZyb21SZXNvdXJjZXMgPSByZXNvdXJjZXMuc2VsZWN0aW9uLm1hcCgocHJvZHVjdCkgPT4gcHJvZHVjdC50aXRsZSk7XHJcbiAgICAgICAgY29uc3QgaWRGcm9tUmVzb3VyY2VzID0gcmVzb3VyY2VzLnNlbGVjdGlvbi5tYXAoKHByb2R1Y3QpID0+IHByb2R1Y3QuaWQpO1xyXG4gICAgICAgIGNvbnN0IHByaWNlRnJvbVJlc291cmNlcyA9IHJlc291cmNlcy5zZWxlY3Rpb24ubWFwKChwcm9kdWN0KSA9PiBwcm9kdWN0LnZhcmlhbnRzLm1hcChmdW5jdGlvbihlKSB7IHJldHVybiBlLnByaWNlOyB9KSk7XHJcbiAgICAgICAgY29uc3QgdmFyaWFudEZyb21SZXNvdXJjZXMgPSByZXNvdXJjZXMuc2VsZWN0aW9uLm1hcCgocHJvZHVjdCkgPT4gcHJvZHVjdC52YXJpYW50cy5tYXAoZnVuY3Rpb24oZSkgeyByZXR1cm4gZS50aXRsZTsgfSkpO1xyXG4gICAgICAgIGNvbnN0IGltZ0Zyb21SZXNvdXJjZXMgPSByZXNvdXJjZXMuc2VsZWN0aW9uLm1hcCgocHJvZHVjdCkgPT4gcHJvZHVjdC5pbWFnZXNbMF0ub3JpZ2luYWxTcmMpO1xyXG4gICAgICAgIC8vU2hvdyAnQ2xlYXIgQWxsJyBidXR0b25cclxuICAgICAgICB0aGlzLnNldFN0YXRlKHtkZWxldGVCdXR0b25TaG93bjogdHJ1ZX0pO1xyXG4gICAgICAgIC8vQ2xvc2UgUmVzb3VyY2UgUGlja2VyXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7b3BlbjogZmFsc2V9KTtcclxuICAgICAgICAvL0FkZGluZyBwcm9kdWN0cyB0byBwcm9kdWN0YHMgYXJyYXlcclxuICAgICAgICBjb25zb2xlLmxvZyhyZXNvdXJjZXMpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHZhcmlhbnRGcm9tUmVzb3VyY2VzKTtcclxuICAgICAgICBmb3IgKGxldCB4ID0gMDsgeCA8IHByaWNlRnJvbVJlc291cmNlcy5sZW5ndGg7IHgrKykge1xyXG4gICAgICAgICAgICBpZiAocHJpY2VGcm9tUmVzb3VyY2VzW3hdLmxlbmd0aCA9PSAxKXtcclxuICAgICAgICAgICAgICAgIGxldCBqb2luZWQgPSB0aGlzLnN0YXRlLnJlc291cmNlTGlzdEFyci5jb25jYXQoe3Jlc291cmNlSW5kZXg6IHRoaXMuc3RhdGUucmVzb3VyY2VJbmRleCwgaWQ6IGlkRnJvbVJlc291cmNlc1t4XSwgIG5hbWU6dGl0bGVGcm9tUmVzb3VyY2VzW3hdLCBhdmF0YXJTb3VyY2U6aW1nRnJvbVJlc291cmNlc1t4XSwgcHJpY2U6cHJpY2VGcm9tUmVzb3VyY2VzW3hdfSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VMaXN0QXJyOiBqb2luZWQgfSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VJbmRleDogdGhpcy5zdGF0ZS5yZXNvdXJjZUluZGV4ICsgMSB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGZvciAobGV0IHkgPSAwOyB5IDwgcHJpY2VGcm9tUmVzb3VyY2VzW3hdLmxlbmd0aDsgeSsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IG5ld0pvaW5lZCA9IHRoaXMuc3RhdGUucmVzb3VyY2VMaXN0QXJyLmNvbmNhdCh7cmVzb3VyY2VJbmRleDogdGhpcy5zdGF0ZS5yZXNvdXJjZUluZGV4LCBpZDogaWRGcm9tUmVzb3VyY2VzW3hdLCAgbmFtZTp0aXRsZUZyb21SZXNvdXJjZXNbeF0sIGF2YXRhclNvdXJjZTppbWdGcm9tUmVzb3VyY2VzW3hdLCBwcmljZTpwcmljZUZyb21SZXNvdXJjZXNbeF1beV19KTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VMaXN0QXJyOiBuZXdKb2luZWQgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHJlc291cmNlSW5kZXg6IHRoaXMuc3RhdGUucmVzb3VyY2VJbmRleCArIDEgfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59XHJcbiAgXHJcbiAgZXhwb3J0IGRlZmF1bHQgSW5kZXg7Il0sInNvdXJjZVJvb3QiOiIifQ==