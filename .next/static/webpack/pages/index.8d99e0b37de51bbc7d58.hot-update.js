webpackHotUpdate_N_E("pages/index",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var _babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/esm/createClass */ "./node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/esm/assertThisInitialized */ "./node_modules/@babel/runtime/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inherits */ "./node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var _babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/helpers/esm/defineProperty */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _shopify_polaris__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @shopify/polaris */ "./node_modules/@shopify/polaris/dist/esm/index.js");
/* harmony import */ var _shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @shopify/app-bridge-react */ "./node_modules/@shopify/app-bridge-react/index.js");
/* harmony import */ var _shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_10__);










function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return Object(_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

//Import Polaris elements, Resource Picker and React




var Index = /*#__PURE__*/function (_Component) {
  Object(_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_3__["default"])(Index, _Component);

  var _super = _createSuper(Index);

  function Index() {
    var _this;

    Object(_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_0__["default"])(this, Index);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "state", {
      open: false,
      resourceListArr: [],
      deleteButtonShown: false,
      resourceIndex: 0
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleDelete", function (ind) {
      var pos = _this.state.resourceListArr.map(function (e) {
        return e.resourceIndex;
      }).indexOf(ind); //Detecting position of item by it`s 'resourceIndex' value


      _this.state.resourceListArr.splice(pos, 1); //Delete item from products array


      _this.setState({
        resourceListArr: _this.state.resourceListArr
      }); //Updating state
      //In case if all objects removed, hide 'Clear All' button


      if (_this.state.resourceListArr.length == 0) {
        _this.setState({
          deleteButtonShown: false
        });
      }
    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleClear", function () {
      _this.setState({
        resourceListArr: []
      }); //Remove all items from product`s array


      _this.setState({
        deleteButtonShown: false
      }); //Hide 'Clear All' button


      _this.setState({
        resourceIndex: 0
      }); //Set 'ResourceIndex' counter to 0

    });

    Object(_babel_runtime_helpers_esm_defineProperty__WEBPACK_IMPORTED_MODULE_6__["default"])(Object(_babel_runtime_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_2__["default"])(_this), "handleSelection", function (resources) {
      //mapping new arrays with needed products values
      var titleFromResources = resources.selection.map(function (product) {
        return product.title;
      });
      var idFromResources = resources.selection.map(function (product) {
        return product.id;
      }); //const priceFromResources = resources.selection.map((product) => product.variants[0].price);

      var priceFromResources = resources.selection.map(function (product) {
        return product.variants.map(function (e) {
          return e.price;
        });
      });
      var imgFromResources = resources.selection.map(function (product) {
        return product.images[0].originalSrc;
      }); //Show 'Clear All' button

      _this.setState({
        deleteButtonShown: true
      }); //Close Resource Picker


      _this.setState({
        open: false
      }); //Adding products to product`s array


      for (var x = 0; x < priceFromResources.length; x++) {
        if (priceFromResources[x].length == 1) {
          /*let joined = this.state.resourceListArr.concat({resourceIndex: this.state.resourceIndex, id: idFromResources[x],  name:titleFromResources[x], avatarSource:imgFromResources[x], price:priceFromResources[x]});
          this.setState({ resourceListArr: joined });
          this.setState({ resourceIndex: this.state.resourceIndex + 1 });*/
        } else {
          for (var y = 0; y < priceFromResources[x].length; y++) {
            var newJoined = _this.state.resourceListArr.concat({
              resourceIndex: _this.state.resourceIndex,
              id: idFromResources[x],
              name: titleFromResources[x],
              avatarSource: imgFromResources[x],
              price: priceFromResources[x]
            });

            _this.setState({
              resourceListArr: newJoined
            });

            _this.setState({
              resourceIndex: _this.state.resourceIndex + 1
            });

            console.log(priceFromResources);
          }
        }
      }
    });

    return _this;
  }

  Object(_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_1__["default"])(Index, [{
    key: "render",
    //set state
    value: function render() {
      var _this2 = this;

      return (
        /*#__PURE__*/
        //Page styling using Polaris Page element, Text an Button
        Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxs"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Page"], {
          fullWidth: true,
          title: "Product Selection",
          primaryAction: {
            content: 'Select product',
            onAction: function onAction() {
              return _this2.setState({
                open: true
              });
            }
          },
          children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_app_bridge_react__WEBPACK_IMPORTED_MODULE_9__["ResourcePicker"]
          /*Resource Picker, change state on clothong and handling if items selected*/
          , {
            resourceType: "Product",
            open: this.state.open,
            onCancel: function onCancel() {
              return _this2.setState({
                open: false
              });
            },
            onSelection: function onSelection(resources) {
              return _this2.handleSelection(resources);
            }
          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
            children: // Display Clearing Button if any item is selected
            this.state.deleteButtonShown ? /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
              style: {
                marginBottom: '15px'
              },
              children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Button"], {
                destructive: true,
                onClick: function onClick() {
                  return _this2.handleClear();
                },
                children: "Clear all"
              })
            }) : null
          }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Card"], {
            children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["ResourceList"]
            /*Adding Resource List wich consists of ResourceItems */
            , {
              resourceName: {
                singular: 'product',
                plural: 'product'
              },
              items: this.state.resourceListArr,
              renderItem: function renderItem(item) {
                var resourceIndex = item.resourceIndex,
                    id = item.id,
                    name = item.name,
                    avatarSource = item.avatarSource,
                    price = item.price; //Setting Item

                return /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsxs"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["ResourceItem"], {
                  id: id,
                  media: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Avatar"], {
                    customer: true,
                    size: "medium",
                    name: name,
                    source: avatarSource
                  }),
                  accessibilityLabel: "View details for ".concat(name),
                  children: [/*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("h2", {
                    children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["TextStyle"], {
                      variation: "strong",
                      children: name
                    })
                  }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
                    children: price
                  }), /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])("div", {
                    style: {
                      position: 'absolute',
                      right: '20px',
                      marginTop: '-40px'
                    },
                    children: /*#__PURE__*/Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__["jsx"])(_shopify_polaris__WEBPACK_IMPORTED_MODULE_8__["Button"], {
                      destructive: true,
                      resourceIndex: resourceIndex,
                      onClick: function onClick(ind) {
                        return _this2.handleDelete(item.resourceIndex);
                      },
                      children: "Delete"
                    })
                  })]
                });
              }
            })
          })]
        })
      );
    } //Handling Deletion of element from Resource List

  }]);

  return Index;
}(react__WEBPACK_IMPORTED_MODULE_10__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Index);

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvaW5kZXguanMiXSwibmFtZXMiOlsiSW5kZXgiLCJvcGVuIiwicmVzb3VyY2VMaXN0QXJyIiwiZGVsZXRlQnV0dG9uU2hvd24iLCJyZXNvdXJjZUluZGV4IiwiaW5kIiwicG9zIiwic3RhdGUiLCJtYXAiLCJlIiwiaW5kZXhPZiIsInNwbGljZSIsInNldFN0YXRlIiwibGVuZ3RoIiwicmVzb3VyY2VzIiwidGl0bGVGcm9tUmVzb3VyY2VzIiwic2VsZWN0aW9uIiwicHJvZHVjdCIsInRpdGxlIiwiaWRGcm9tUmVzb3VyY2VzIiwiaWQiLCJwcmljZUZyb21SZXNvdXJjZXMiLCJ2YXJpYW50cyIsInByaWNlIiwiaW1nRnJvbVJlc291cmNlcyIsImltYWdlcyIsIm9yaWdpbmFsU3JjIiwieCIsInkiLCJuZXdKb2luZWQiLCJjb25jYXQiLCJuYW1lIiwiYXZhdGFyU291cmNlIiwiY29uc29sZSIsImxvZyIsImNvbnRlbnQiLCJvbkFjdGlvbiIsImhhbmRsZVNlbGVjdGlvbiIsIm1hcmdpbkJvdHRvbSIsImhhbmRsZUNsZWFyIiwic2luZ3VsYXIiLCJwbHVyYWwiLCJpdGVtIiwicG9zaXRpb24iLCJyaWdodCIsIm1hcmdpblRvcCIsImhhbmRsZURlbGV0ZSIsIkNvbXBvbmVudCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7O0lBRU1BLEs7Ozs7Ozs7Ozs7Ozs7Ozs7Z05BQ007QUFBRUMsVUFBSSxFQUFFLEtBQVI7QUFBZUMscUJBQWUsRUFBRSxFQUFoQztBQUFvQ0MsdUJBQWlCLEVBQUUsS0FBdkQ7QUFBOERDLG1CQUFhLEVBQUU7QUFBN0UsSzs7dU5BMERPLFVBQUNDLEdBQUQsRUFBUztBQUNwQixVQUFJQyxHQUFHLEdBQUcsTUFBS0MsS0FBTCxDQUFXTCxlQUFYLENBQTJCTSxHQUEzQixDQUErQixVQUFTQyxDQUFULEVBQVk7QUFBRSxlQUFPQSxDQUFDLENBQUNMLGFBQVQ7QUFBeUIsT0FBdEUsRUFBd0VNLE9BQXhFLENBQWdGTCxHQUFoRixDQUFWLENBRG9CLENBQzRFOzs7QUFDaEcsWUFBS0UsS0FBTCxDQUFXTCxlQUFYLENBQTJCUyxNQUEzQixDQUFrQ0wsR0FBbEMsRUFBdUMsQ0FBdkMsRUFGb0IsQ0FFdUI7OztBQUMzQyxZQUFLTSxRQUFMLENBQWM7QUFBRVYsdUJBQWUsRUFBRyxNQUFLSyxLQUFMLENBQVdMO0FBQS9CLE9BQWQsRUFIb0IsQ0FHNkM7QUFDakU7OztBQUNBLFVBQUksTUFBS0ssS0FBTCxDQUFXTCxlQUFYLENBQTJCVyxNQUEzQixJQUFxQyxDQUF6QyxFQUE0QztBQUN4QyxjQUFLRCxRQUFMLENBQWM7QUFBQ1QsMkJBQWlCLEVBQUU7QUFBcEIsU0FBZDtBQUNIO0FBQ0osSzs7c05BR2EsWUFBTTtBQUNoQixZQUFLUyxRQUFMLENBQWM7QUFBRVYsdUJBQWUsRUFBRTtBQUFuQixPQUFkLEVBRGdCLENBQ3dCOzs7QUFDeEMsWUFBS1UsUUFBTCxDQUFjO0FBQUNULHlCQUFpQixFQUFFO0FBQXBCLE9BQWQsRUFGZ0IsQ0FFMEI7OztBQUMxQyxZQUFLUyxRQUFMLENBQWM7QUFBRVIscUJBQWEsRUFBRTtBQUFqQixPQUFkLEVBSGdCLENBR29COztBQUN2QyxLOzswTkFHaUIsVUFBQ1UsU0FBRCxFQUFlO0FBQzdCO0FBQ0EsVUFBTUMsa0JBQWtCLEdBQUdELFNBQVMsQ0FBQ0UsU0FBVixDQUFvQlIsR0FBcEIsQ0FBd0IsVUFBQ1MsT0FBRDtBQUFBLGVBQWFBLE9BQU8sQ0FBQ0MsS0FBckI7QUFBQSxPQUF4QixDQUEzQjtBQUNBLFVBQU1DLGVBQWUsR0FBR0wsU0FBUyxDQUFDRSxTQUFWLENBQW9CUixHQUFwQixDQUF3QixVQUFDUyxPQUFEO0FBQUEsZUFBYUEsT0FBTyxDQUFDRyxFQUFyQjtBQUFBLE9BQXhCLENBQXhCLENBSDZCLENBSTdCOztBQUNBLFVBQU1DLGtCQUFrQixHQUFHUCxTQUFTLENBQUNFLFNBQVYsQ0FBb0JSLEdBQXBCLENBQXdCLFVBQUNTLE9BQUQ7QUFBQSxlQUFhQSxPQUFPLENBQUNLLFFBQVIsQ0FBaUJkLEdBQWpCLENBQXFCLFVBQVNDLENBQVQsRUFBWTtBQUFFLGlCQUFPQSxDQUFDLENBQUNjLEtBQVQ7QUFBaUIsU0FBcEQsQ0FBYjtBQUFBLE9BQXhCLENBQTNCO0FBQ0EsVUFBTUMsZ0JBQWdCLEdBQUdWLFNBQVMsQ0FBQ0UsU0FBVixDQUFvQlIsR0FBcEIsQ0FBd0IsVUFBQ1MsT0FBRDtBQUFBLGVBQWFBLE9BQU8sQ0FBQ1EsTUFBUixDQUFlLENBQWYsRUFBa0JDLFdBQS9CO0FBQUEsT0FBeEIsQ0FBekIsQ0FONkIsQ0FPN0I7O0FBQ0EsWUFBS2QsUUFBTCxDQUFjO0FBQUNULHlCQUFpQixFQUFFO0FBQXBCLE9BQWQsRUFSNkIsQ0FTN0I7OztBQUNBLFlBQUtTLFFBQUwsQ0FBYztBQUFDWCxZQUFJLEVBQUU7QUFBUCxPQUFkLEVBVjZCLENBVzdCOzs7QUFDQSxXQUFLLElBQUkwQixDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHTixrQkFBa0IsQ0FBQ1IsTUFBdkMsRUFBK0NjLENBQUMsRUFBaEQsRUFBb0Q7QUFDaEQsWUFBSU4sa0JBQWtCLENBQUNNLENBQUQsQ0FBbEIsQ0FBc0JkLE1BQXRCLElBQWdDLENBQXBDLEVBQXNDO0FBQ2xDO0FBQ2hCO0FBQ0E7QUFDYSxTQUpELE1BS0s7QUFDRCxlQUFLLElBQUllLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdQLGtCQUFrQixDQUFDTSxDQUFELENBQWxCLENBQXNCZCxNQUExQyxFQUFrRGUsQ0FBQyxFQUFuRCxFQUF1RDtBQUNuRCxnQkFBSUMsU0FBUyxHQUFHLE1BQUt0QixLQUFMLENBQVdMLGVBQVgsQ0FBMkI0QixNQUEzQixDQUFrQztBQUFDMUIsMkJBQWEsRUFBRSxNQUFLRyxLQUFMLENBQVdILGFBQTNCO0FBQTBDZ0IsZ0JBQUUsRUFBRUQsZUFBZSxDQUFDUSxDQUFELENBQTdEO0FBQW1FSSxrQkFBSSxFQUFDaEIsa0JBQWtCLENBQUNZLENBQUQsQ0FBMUY7QUFBK0ZLLDBCQUFZLEVBQUNSLGdCQUFnQixDQUFDRyxDQUFELENBQTVIO0FBQWlJSixtQkFBSyxFQUFDRixrQkFBa0IsQ0FBQ00sQ0FBRDtBQUF6SixhQUFsQyxDQUFoQjs7QUFDQSxrQkFBS2YsUUFBTCxDQUFjO0FBQUVWLDZCQUFlLEVBQUUyQjtBQUFuQixhQUFkOztBQUNBLGtCQUFLakIsUUFBTCxDQUFjO0FBQUVSLDJCQUFhLEVBQUUsTUFBS0csS0FBTCxDQUFXSCxhQUFYLEdBQTJCO0FBQTVDLGFBQWQ7O0FBQ0E2QixtQkFBTyxDQUFDQyxHQUFSLENBQVliLGtCQUFaO0FBQ0g7QUFDSjtBQUNKO0FBQ0osSzs7Ozs7OztBQXZHdUY7NkJBRS9FO0FBQUE7O0FBQ0w7QUFBQTtBQUNBO0FBQ0EsdUVBQUMscURBQUQ7QUFDSSxtQkFBUyxNQURiO0FBRUksZUFBSyxFQUFDLG1CQUZWO0FBR0ksdUJBQWEsRUFBRTtBQUNYYyxtQkFBTyxFQUFFLGdCQURFO0FBRVhDLG9CQUFRLEVBQUU7QUFBQSxxQkFBTSxNQUFJLENBQUN4QixRQUFMLENBQWM7QUFBQ1gsb0JBQUksRUFBRTtBQUFQLGVBQWQsQ0FBTjtBQUFBO0FBRkMsV0FIbkI7QUFBQSxrQ0FRUSw4REFBQyx3RUFBYztBQUFFO0FBQWpCO0FBQ0ksd0JBQVksRUFBQyxTQURqQjtBQUVJLGdCQUFJLEVBQUUsS0FBS00sS0FBTCxDQUFXTixJQUZyQjtBQUdJLG9CQUFRLEVBQUc7QUFBQSxxQkFBTSxNQUFJLENBQUNXLFFBQUwsQ0FBYztBQUFDWCxvQkFBSSxFQUFFO0FBQVAsZUFBZCxDQUFOO0FBQUEsYUFIZjtBQUlJLHVCQUFXLEVBQUUscUJBQUNhLFNBQUQ7QUFBQSxxQkFBZSxNQUFJLENBQUN1QixlQUFMLENBQXFCdkIsU0FBckIsQ0FBZjtBQUFBO0FBSmpCLFlBUlIsZUFjUTtBQUFBLHNCQUNLO0FBQ0QsaUJBQUtQLEtBQUwsQ0FBV0osaUJBQVgsZ0JBQ0k7QUFBSyxtQkFBSyxFQUFFO0FBQUNtQyw0QkFBWSxFQUFFO0FBQWYsZUFBWjtBQUFBLHFDQUNJLDhEQUFDLHVEQUFEO0FBQVEsMkJBQVcsTUFBbkI7QUFDQSx1QkFBTyxFQUFFO0FBQUEseUJBQU0sTUFBSSxDQUFDQyxXQUFMLEVBQU47QUFBQSxpQkFEVDtBQUFBO0FBQUE7QUFESixjQURKLEdBS0U7QUFQTixZQWRSLGVBdUJRLDhEQUFDLHFEQUFEO0FBQUEsbUNBQ0EsOERBQUMsNkRBQVk7QUFBQztBQUFkO0FBQ0ksMEJBQVksRUFBRTtBQUFDQyx3QkFBUSxFQUFFLFNBQVg7QUFBc0JDLHNCQUFNLEVBQUU7QUFBOUIsZUFEbEI7QUFFSSxtQkFBSyxFQUFHLEtBQUtsQyxLQUFMLENBQVdMLGVBRnZCO0FBR0ksd0JBQVUsRUFBRSxvQkFBQ3dDLElBQUQsRUFBVTtBQUFBLG9CQUNmdEMsYUFEZSxHQUNrQ3NDLElBRGxDLENBQ2Z0QyxhQURlO0FBQUEsb0JBQ0FnQixFQURBLEdBQ2tDc0IsSUFEbEMsQ0FDQXRCLEVBREE7QUFBQSxvQkFDS1csSUFETCxHQUNrQ1csSUFEbEMsQ0FDS1gsSUFETDtBQUFBLG9CQUNXQyxZQURYLEdBQ2tDVSxJQURsQyxDQUNXVixZQURYO0FBQUEsb0JBQ3lCVCxLQUR6QixHQUNrQ21CLElBRGxDLENBQ3lCbkIsS0FEekIsRUFDd0M7O0FBQzlELG9DQUNJLCtEQUFDLDZEQUFEO0FBQ0Esb0JBQUUsRUFBRUgsRUFESjtBQUVBLHVCQUFLLGVBQUcsOERBQUMsdURBQUQ7QUFBUSw0QkFBUSxNQUFoQjtBQUFpQix3QkFBSSxFQUFDLFFBQXRCO0FBQStCLHdCQUFJLEVBQUVXLElBQXJDO0FBQTJDLDBCQUFNLEVBQUVDO0FBQW5ELG9CQUZSO0FBR0Esb0NBQWtCLDZCQUFzQkQsSUFBdEIsQ0FIbEI7QUFBQSwwQ0FLQTtBQUFBLDJDQUNJLDhEQUFDLDBEQUFEO0FBQVcsK0JBQVMsRUFBQyxRQUFyQjtBQUFBLGdDQUErQkE7QUFBL0I7QUFESixvQkFMQSxlQVFBO0FBQUEsOEJBQU1SO0FBQU4sb0JBUkEsZUFTQTtBQUFLLHlCQUFLLEVBQUU7QUFBQ29CLDhCQUFRLEVBQUMsVUFBVjtBQUFzQkMsMkJBQUssRUFBQyxNQUE1QjtBQUFvQ0MsK0JBQVMsRUFBRTtBQUEvQyxxQkFBWjtBQUFBLDJDQUNJLDhEQUFDLHVEQUFEO0FBQVEsaUNBQVcsTUFBbkI7QUFBb0IsbUNBQWEsRUFBRXpDLGFBQW5DO0FBQ0EsNkJBQU8sRUFBRSxpQkFBQ0MsR0FBRDtBQUFBLCtCQUFTLE1BQUksQ0FBQ3lDLFlBQUwsQ0FBa0JKLElBQUksQ0FBQ3RDLGFBQXZCLENBQVQ7QUFBQSx1QkFEVDtBQUFBO0FBQUE7QUFESixvQkFUQTtBQUFBLGtCQURKO0FBZ0JDO0FBckJMO0FBREEsWUF2QlI7QUFBQTtBQUZBO0FBb0RILEssQ0FFRDs7Ozs7RUExRGdCMkMsZ0Q7O0FBNEdIL0Msb0VBQWYiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvaW5kZXguOGQ5OWUwYjM3ZGU1MWJiYzdkNTguaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8vSW1wb3J0IFBvbGFyaXMgZWxlbWVudHMsIFJlc291cmNlIFBpY2tlciBhbmQgUmVhY3RcclxuaW1wb3J0IHsgUGFnZSwgQ2FyZCwgUmVzb3VyY2VMaXN0LCBSZXNvdXJjZUl0ZW0sIEF2YXRhciwgVGV4dFN0eWxlLCBCdXR0b24gfSBmcm9tIFwiQHNob3BpZnkvcG9sYXJpc1wiO1xyXG5pbXBvcnQgeyBSZXNvdXJjZVBpY2tlciB9IGZyb20gXCJAc2hvcGlmeS9hcHAtYnJpZGdlLXJlYWN0XCI7XHJcbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XHJcblxyXG5jbGFzcyBJbmRleCBleHRlbmRzIENvbXBvbmVudCB7XHJcbiAgICBzdGF0ZSA9IHsgb3BlbjogZmFsc2UsIHJlc291cmNlTGlzdEFycjogW10sIGRlbGV0ZUJ1dHRvblNob3duOiBmYWxzZSwgcmVzb3VyY2VJbmRleDogMCB9Ly9zZXQgc3RhdGVcclxuXHJcbiAgICByZW5kZXIoKSB7IFxyXG4gICAgICAgIHJldHVybiAoIFxyXG4gICAgICAgIC8vUGFnZSBzdHlsaW5nIHVzaW5nIFBvbGFyaXMgUGFnZSBlbGVtZW50LCBUZXh0IGFuIEJ1dHRvblxyXG4gICAgICAgIDxQYWdlICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGZ1bGxXaWR0aFxyXG4gICAgICAgICAgICB0aXRsZT1cIlByb2R1Y3QgU2VsZWN0aW9uXCJcclxuICAgICAgICAgICAgcHJpbWFyeUFjdGlvbj17e1xyXG4gICAgICAgICAgICAgICAgY29udGVudDogJ1NlbGVjdCBwcm9kdWN0JyxcclxuICAgICAgICAgICAgICAgIG9uQWN0aW9uOiAoKSA9PiB0aGlzLnNldFN0YXRlKHtvcGVuOiB0cnVlfSlcclxuICAgICAgICAgICAgfX1cclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgPFJlc291cmNlUGlja2VyICAvKlJlc291cmNlIFBpY2tlciwgY2hhbmdlIHN0YXRlIG9uIGNsb3Rob25nIGFuZCBoYW5kbGluZyBpZiBpdGVtcyBzZWxlY3RlZCovXHJcbiAgICAgICAgICAgICAgICAgICAgcmVzb3VyY2VUeXBlPVwiUHJvZHVjdFwiXHJcbiAgICAgICAgICAgICAgICAgICAgb3Blbj17dGhpcy5zdGF0ZS5vcGVufVxyXG4gICAgICAgICAgICAgICAgICAgIG9uQ2FuY2VsPXsgKCkgPT4gdGhpcy5zZXRTdGF0ZSh7b3BlbjogZmFsc2V9KX1cclxuICAgICAgICAgICAgICAgICAgICBvblNlbGVjdGlvbj17KHJlc291cmNlcykgPT4gdGhpcy5oYW5kbGVTZWxlY3Rpb24ocmVzb3VyY2VzKX0gXHJcbiAgICAgICAgICAgICAgICAvPiBcclxuICAgICAgICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgey8vIERpc3BsYXkgQ2xlYXJpbmcgQnV0dG9uIGlmIGFueSBpdGVtIGlzIHNlbGVjdGVkXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZS5kZWxldGVCdXR0b25TaG93biA/IFxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IHN0eWxlPXt7bWFyZ2luQm90dG9tOiAnMTVweCd9fT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxCdXR0b24gZGVzdHJ1Y3RpdmUgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB0aGlzLmhhbmRsZUNsZWFyKCl9PkNsZWFyIGFsbDwvQnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2Rpdj4gXHJcbiAgICAgICAgICAgICAgICAgICAgOiBudWxsfSBcclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPENhcmQ+XHJcbiAgICAgICAgICAgICAgICA8UmVzb3VyY2VMaXN0IC8qQWRkaW5nIFJlc291cmNlIExpc3Qgd2ljaCBjb25zaXN0cyBvZiBSZXNvdXJjZUl0ZW1zICovXHJcbiAgICAgICAgICAgICAgICAgICAgcmVzb3VyY2VOYW1lPXt7c2luZ3VsYXI6ICdwcm9kdWN0JywgcGx1cmFsOiAncHJvZHVjdCd9fVxyXG4gICAgICAgICAgICAgICAgICAgIGl0ZW1zPXsgdGhpcy5zdGF0ZS5yZXNvdXJjZUxpc3RBcnIgfVxyXG4gICAgICAgICAgICAgICAgICAgIHJlbmRlckl0ZW09eyhpdGVtKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qge3Jlc291cmNlSW5kZXgsIGlkLCAgbmFtZSwgYXZhdGFyU291cmNlLCBwcmljZX0gPSBpdGVtOyAvL1NldHRpbmcgSXRlbVxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxSZXNvdXJjZUl0ZW1cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ9e2lkfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtZWRpYT0gezxBdmF0YXIgY3VzdG9tZXIgc2l6ZT1cIm1lZGl1bVwiIG5hbWU9e25hbWV9IHNvdXJjZT17YXZhdGFyU291cmNlfSAvPn1cclxuICAgICAgICAgICAgICAgICAgICAgICAgYWNjZXNzaWJpbGl0eUxhYmVsPXtgVmlldyBkZXRhaWxzIGZvciAke25hbWV9YH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8aDI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8VGV4dFN0eWxlIHZhcmlhdGlvbj1cInN0cm9uZ1wiPntuYW1lfTwvVGV4dFN0eWxlPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2gyPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2PntwcmljZX08L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBzdHlsZT17e3Bvc2l0aW9uOidhYnNvbHV0ZScsIHJpZ2h0OicyMHB4JywgbWFyZ2luVG9wOiAnLTQwcHgnfX0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QnV0dG9uIGRlc3RydWN0aXZlIHJlc291cmNlSW5kZXg9e3Jlc291cmNlSW5kZXh9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoaW5kKSA9PiB0aGlzLmhhbmRsZURlbGV0ZShpdGVtLnJlc291cmNlSW5kZXgpfT5EZWxldGU8L0J1dHRvbj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvUmVzb3VyY2VJdGVtPlxyXG4gICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICAgICAgfX1cclxuICAgICAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICAgICAgICA8L0NhcmQ+XHJcbiAgICAgICAgPC9QYWdlPiBcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8vSGFuZGxpbmcgRGVsZXRpb24gb2YgZWxlbWVudCBmcm9tIFJlc291cmNlIExpc3RcclxuICAgIGhhbmRsZURlbGV0ZSA9IChpbmQpID0+IHtcclxuICAgICAgICB2YXIgcG9zID0gdGhpcy5zdGF0ZS5yZXNvdXJjZUxpc3RBcnIubWFwKGZ1bmN0aW9uKGUpIHsgcmV0dXJuIGUucmVzb3VyY2VJbmRleDsgfSkuaW5kZXhPZihpbmQpOyAvL0RldGVjdGluZyBwb3NpdGlvbiBvZiBpdGVtIGJ5IGl0YHMgJ3Jlc291cmNlSW5kZXgnIHZhbHVlXHJcbiAgICAgICAgdGhpcy5zdGF0ZS5yZXNvdXJjZUxpc3RBcnIuc3BsaWNlKHBvcywgMSk7IC8vRGVsZXRlIGl0ZW0gZnJvbSBwcm9kdWN0cyBhcnJheVxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyByZXNvdXJjZUxpc3RBcnI6ICB0aGlzLnN0YXRlLnJlc291cmNlTGlzdEFyciB9KTsgLy9VcGRhdGluZyBzdGF0ZVxyXG4gICAgICAgIC8vSW4gY2FzZSBpZiBhbGwgb2JqZWN0cyByZW1vdmVkLCBoaWRlICdDbGVhciBBbGwnIGJ1dHRvblxyXG4gICAgICAgIGlmICh0aGlzLnN0YXRlLnJlc291cmNlTGlzdEFyci5sZW5ndGggPT0gMCkge1xyXG4gICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtkZWxldGVCdXR0b25TaG93bjogZmFsc2V9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLy9IYW5kbGluZyAnQ2xlYXIgQWxsJyBidXR0b24gYWN0aW9uXHJcbiAgICBoYW5kbGVDbGVhciA9ICgpID0+IHtcclxuICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VMaXN0QXJyOiBbXSB9KTsgLy9SZW1vdmUgYWxsIGl0ZW1zIGZyb20gcHJvZHVjdGBzIGFycmF5XHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7ZGVsZXRlQnV0dG9uU2hvd246IGZhbHNlfSk7Ly9IaWRlICdDbGVhciBBbGwnIGJ1dHRvblxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyByZXNvdXJjZUluZGV4OiAwIH0pOy8vU2V0ICdSZXNvdXJjZUluZGV4JyBjb3VudGVyIHRvIDBcclxuICAgIH1cclxuXHJcbiAgICAvL0hhbmRsaW5nIGl0ZW1zIHNlbGVjdGlvbiBmcm9tIHJlc291cmNlIHBpY2tlclxyXG4gICAgaGFuZGxlU2VsZWN0aW9uID0gKHJlc291cmNlcykgPT4ge1xyXG4gICAgICAgIC8vbWFwcGluZyBuZXcgYXJyYXlzIHdpdGggbmVlZGVkIHByb2R1Y3RzIHZhbHVlc1xyXG4gICAgICAgIGNvbnN0IHRpdGxlRnJvbVJlc291cmNlcyA9IHJlc291cmNlcy5zZWxlY3Rpb24ubWFwKChwcm9kdWN0KSA9PiBwcm9kdWN0LnRpdGxlKTtcclxuICAgICAgICBjb25zdCBpZEZyb21SZXNvdXJjZXMgPSByZXNvdXJjZXMuc2VsZWN0aW9uLm1hcCgocHJvZHVjdCkgPT4gcHJvZHVjdC5pZCk7XHJcbiAgICAgICAgLy9jb25zdCBwcmljZUZyb21SZXNvdXJjZXMgPSByZXNvdXJjZXMuc2VsZWN0aW9uLm1hcCgocHJvZHVjdCkgPT4gcHJvZHVjdC52YXJpYW50c1swXS5wcmljZSk7XHJcbiAgICAgICAgY29uc3QgcHJpY2VGcm9tUmVzb3VyY2VzID0gcmVzb3VyY2VzLnNlbGVjdGlvbi5tYXAoKHByb2R1Y3QpID0+IHByb2R1Y3QudmFyaWFudHMubWFwKGZ1bmN0aW9uKGUpIHsgcmV0dXJuIGUucHJpY2U7IH0pKTtcclxuICAgICAgICBjb25zdCBpbWdGcm9tUmVzb3VyY2VzID0gcmVzb3VyY2VzLnNlbGVjdGlvbi5tYXAoKHByb2R1Y3QpID0+IHByb2R1Y3QuaW1hZ2VzWzBdLm9yaWdpbmFsU3JjKTtcclxuICAgICAgICAvL1Nob3cgJ0NsZWFyIEFsbCcgYnV0dG9uXHJcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7ZGVsZXRlQnV0dG9uU2hvd246IHRydWV9KTtcclxuICAgICAgICAvL0Nsb3NlIFJlc291cmNlIFBpY2tlclxyXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe29wZW46IGZhbHNlfSk7XHJcbiAgICAgICAgLy9BZGRpbmcgcHJvZHVjdHMgdG8gcHJvZHVjdGBzIGFycmF5XHJcbiAgICAgICAgZm9yIChsZXQgeCA9IDA7IHggPCBwcmljZUZyb21SZXNvdXJjZXMubGVuZ3RoOyB4KyspIHtcclxuICAgICAgICAgICAgaWYgKHByaWNlRnJvbVJlc291cmNlc1t4XS5sZW5ndGggPT0gMSl7XHJcbiAgICAgICAgICAgICAgICAvKmxldCBqb2luZWQgPSB0aGlzLnN0YXRlLnJlc291cmNlTGlzdEFyci5jb25jYXQoe3Jlc291cmNlSW5kZXg6IHRoaXMuc3RhdGUucmVzb3VyY2VJbmRleCwgaWQ6IGlkRnJvbVJlc291cmNlc1t4XSwgIG5hbWU6dGl0bGVGcm9tUmVzb3VyY2VzW3hdLCBhdmF0YXJTb3VyY2U6aW1nRnJvbVJlc291cmNlc1t4XSwgcHJpY2U6cHJpY2VGcm9tUmVzb3VyY2VzW3hdfSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VMaXN0QXJyOiBqb2luZWQgfSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VJbmRleDogdGhpcy5zdGF0ZS5yZXNvdXJjZUluZGV4ICsgMSB9KTsqL1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgZm9yIChsZXQgeSA9IDA7IHkgPCBwcmljZUZyb21SZXNvdXJjZXNbeF0ubGVuZ3RoOyB5KyspIHtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgbmV3Sm9pbmVkID0gdGhpcy5zdGF0ZS5yZXNvdXJjZUxpc3RBcnIuY29uY2F0KHtyZXNvdXJjZUluZGV4OiB0aGlzLnN0YXRlLnJlc291cmNlSW5kZXgsIGlkOiBpZEZyb21SZXNvdXJjZXNbeF0sICBuYW1lOnRpdGxlRnJvbVJlc291cmNlc1t4XSwgYXZhdGFyU291cmNlOmltZ0Zyb21SZXNvdXJjZXNbeF0sIHByaWNlOnByaWNlRnJvbVJlc291cmNlc1t4XX0pO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoeyByZXNvdXJjZUxpc3RBcnI6IG5ld0pvaW5lZCB9KTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHsgcmVzb3VyY2VJbmRleDogdGhpcy5zdGF0ZS5yZXNvdXJjZUluZGV4ICsgMSB9KTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhwcmljZUZyb21SZXNvdXJjZXMpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufVxyXG4gIFxyXG4gIGV4cG9ydCBkZWZhdWx0IEluZGV4OyJdLCJzb3VyY2VSb290IjoiIn0=